package module;

import languageModel.STIRLanguageModel;
import util.DisfluencyHypothesisStack;
import util.WordHypothesisGraph;

public class EditTerm extends Module {
	
	public void init(){
		this.features = new String[]{"wml","wml_ed"};
	}
	
	
	public EditTerm() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EditTerm(STIRLanguageModel model) {
		super(model);
		// TODO Auto-generated constructor stub
	}

	public EditTerm(String modelFileLocation) throws Exception {
		super(modelFileLocation);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void generateNewFeaturesAndClassify(DisfluencyHypothesisStack stack,
			WordHypothesisGraph graph) {
		//TODO How the update model works will heavily influence how this works
		//GroundedIn is a useful notion here
		//this.languageModel.logProbNormalisedBySequenceLength(wordSequence)
		//add each feature in a WEKA feature structure
		for (String f : features){
			//this.languageModel.getFeatureValueByName(f);
		}
	}

}
