package module;


import javax.swing.SwingUtilities;

import demo.inpro.synthesis.SimpleEnglishPatternDemonstrator;
import languageModel.POSLanguageModel;
import languageModel.STIRLanguageModel;
import util.DisfluencyHypothesisStack;
import util.WordHypothesisGraph;
import edu.cmu.sphinx.result.Edge;
import edu.cmu.sphinx.result.Node;

public class STIRsystem extends Module {
	/**
	 * Central control/pipeline
	 */
	
	STIRLanguageModel lm = null;
	POSLanguageModel poslm = null;
	
	EditTerm ed = null;
	RPS rps = null;
	RMS rms = null;
	RPN rpn = null;
	
	WordHypothesisGraph wordgraph = null;
	DisfluencyHypothesisStack stack = null;
	
	@Override
	public void generateNewFeaturesAndClassify(DisfluencyHypothesisStack stack,
			WordHypothesisGraph graph) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Initializes the STIR system by initializes the subcomponents and the language model
	 */
	public void init(){
		lm = new STIRLanguageModel();
		ed = new EditTerm(lm);
		rms = new RMS(lm);
		rpn = new RPN(lm);
		utteranceInit();
	}
	
	private void run() {
		System.out.println("System running.");
		init();
		//BasicConfigurator.configure();
		//cm = new ConfigurationManager("src/config/config.xml");
    	//ps = cm.getPropertySheet(PROP_CURRENT_HYPOTHESIS);
    	//WebSpeech webSpeech = (WebSpeech) cm.lookup(PROP_WEB_SPEECH);
    	
    	//VeniceListenerModule vlm = (VeniceListenerModule) cm.lookup(PROP_VENICE_LISTENER);
    	//textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
    	//final List<PushBuffer> hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		//SimpleText.createAndShowGUI(hypListeners, textBasedFloorTracker);
	}
	
	public void utteranceInit(){
		wordgraph = new WordHypothesisGraph(); //will be the same for each init
		stack = new DisfluencyHypothesisStack();
	}
	
	//update types- no "commit" at the moment
	/**A main event driven function of this class (will be its primary input from ASR or text)
	 * TODO Implement this
	 * If a word is revoked, all its edges must be too in the word Lattice
	 * Additionally any RPS (repair onsets) or EditTerm classifications contingent on this must be removed.
	 * If RMS, RPS removed, these must be re-computed.
	 * Could frame all these as IU units with a pointer on the graph,
	 * @param nodeID
	 */
	public void revokeNode(String nodeID){
		wordgraph.removeNode(nodeID);
		//update();
	}
		
	/**
	 * A main event driven function of this class (will be its primary input from ASR or text)
	 * Lattice building step- adds an edge as an IU-like thing.		 
	 * Could be called several times- depth-first as goes through whole of STIR classifiers..
	 * Needs to deal with revokes, i.e. clear all paths revoked
	 * Also this may happen after a revoke (i.e.) say in path w1 w2 w3, w2 is revoked and re-added 
	*/
	public void addNode(String word, String nodeID, int beginTime, int endTime, Node fromNode, double acousticScore, double lmScore){
		Node toNode = wordgraph.addNode(nodeID, word, beginTime, endTime);
		Edge newEdge = wordgraph.addEdge(fromNode, toNode, acousticScore, lmScore);//we could add the more complex LM calculations in there
		update(newEdge);
	}
	
	public void update(Edge newEdge){
		/**
		 * Gets new Language Model scores for the new word edge/node
		 */
		//wordgraph.newEdge.getToNode()
		//addNode();
	
	}
	
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				//STIRsystem s = new STIRsystem();
				//run();
				init();
			}
		});
		
		//updates should come via RSB word by word or via InproTK
		//the end result is an update from a lattice, need to fake this somehow..
		
	}
	

}
