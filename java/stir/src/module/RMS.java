package module;

import languageModel.STIRLanguageModel;
import util.DisfluencyHypothesisStack;
import util.WordHypothesisGraph;

public class RMS extends Module {

	public void init(){
		this.features = new String[]{"wml","wml_ed"};
	}
	
	public RMS() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RMS(STIRLanguageModel model) {
		super(model);
		// TODO Auto-generated constructor stub
	}

	public RMS(String modelFileLocation) throws Exception {
		super(modelFileLocation);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void generateNewFeaturesAndClassify(DisfluencyHypothesisStack stack,
			WordHypothesisGraph graph) {
		// TODO Auto-generated method stub

	}

}
