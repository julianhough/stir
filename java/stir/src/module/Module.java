package module;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import languageModel.STIRLanguageModel;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
//import edu.cmu.sphinx.util.props.S4Boolean;
import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import util.DisfluencyHypothesisStack;
import util.WordHypothesisGraph;
import weka.classifiers.Classifier;

public abstract class Module extends IUModule {
	/**
	 * Superclass for every module- they will all have random forests or other classifier (from WEKA most likely)
	 * They all consume word graphs
	 * All but the edit term classifier consume a stack of repair hypotheses
	 * They can all be in train or test (or load from model) mode
	 */
	
	//IU stuff for inproTK capabilities
	public final static String PROP_CHANGE = "change";
	
	public static STIRLanguageModel languageModel = null;
		
	ArrayList<RepairIU> iuList;
	private boolean change;
	
	public static String mode;//train, test, load
	public static Classifier classifier = null;//WEKA one
	public static String costMatrixFileName = null;
	
	public static String[] features; //the feature names to be called
	
	public Module(){
		init();
		//dummy constructor for now
	}
	
	public Module(STIRLanguageModel model){
		//dummy constructor for now
		this.languageModel = model;
		init();
	}
	
	public Module(String modelFileLocation) throws Exception{
		/**constructor that loads old models
		 *simply serializing the model TODO not sure if it needs the error function files too or if these are saved..
		 **/
		classifier = (Classifier) weka.core.SerializationHelper.read(modelFileLocation);
		init();
	}
	
	public abstract void init();//each module initialisizes differently, always part of the constructor
	
	
	public void setLanguageModel(STIRLanguageModel model){
		this.languageModel = model;
	}
	
	
	public static String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public static Classifier getClassifier() {
		return classifier;
	}

	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}

	public static String getCostMatrixFileName() {
		return costMatrixFileName;
	}

	public void setCostMatrixFileName(String costMatrixFileName) {
		this.costMatrixFileName = costMatrixFileName;
	}
	
	public void save(String modelFileLocation) throws Exception{
		//saves (serializes) the classifier, %TODO not sure if it needs error functions, possibly reference to which language model by ID..
		weka.core.SerializationHelper.write(modelFileLocation,classifier);
	}
	
	public abstract void generateNewFeaturesAndClassify(DisfluencyHypothesisStack stack, WordHypothesisGraph graph);
	/**
	 * Most important method in STIR. Given a new word (or set of words) has been consumed with new probabilities calculated on the ngram lattice/trellis and on the repair hypothesis (on the stack)
	 * which link to contiguous sequences on the trellis.
	 * This creates (possibly a number) of feature vectors for the classifier to consume, which then outputs its classification.
	 * May only be triggered once in the word-by-word modules, but several times in the backtracking ones..
	 */
	
	//IU stuff for InproTK
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		
		iuList = new ArrayList<>();
		iuList.add(new RepairIU());
		this.change = ps.getBoolean(PROP_CHANGE);
		
	}

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius, List<? extends EditMessage<? extends IU>> edits) {
		
		ArrayList<EditMessage<RepairIU>> newEdits = new ArrayList<EditMessage<RepairIU>>();
		
		for (EditMessage<? extends IU> edit : edits) {

			if (edit.getIU() instanceof WordIU) {
				
			}
			
			
			switch (edit.getType()) {
			case ADD:
				String payload = edit.getIU().toPayLoad();
				if (change)
					payload = payload.replaceAll("a", "E");
				RepairIU miu = new RepairIU();
				miu.groundIn(edit.getIU());
				miu.setSameLevelLink(iuList.get(iuList.size()-1));
				newEdits.add(new EditMessage(edit.getType(), miu));
				iuList.add(miu);
				break;
			case COMMIT:
				break;
			case REVOKE:
				// remove the corresponding IU from iuList
				break;
			default:
				break;
			
			}
			
	
			
			
		}
		
		
		rightBuffer.setBuffer(newEdits);
	}
	
	
	

}
