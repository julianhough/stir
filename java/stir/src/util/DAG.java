package util;

import java.util.HashMap;
import java.util.List;

public class DAG {
	
   Node root; // assuming only one root exists

   public static class Node{
      List<Node> successors;
      int value; 
   }
   
   class PathMap {
	   HashMap<DAG.Node, List<DAG.Node> > pathMap;

	   public List<DAG.Node> getPathFromRoot(DAG.Node n) {
	      List<DAG.Node> pathFromRoot = pathMap.get(n);
	      return pathFromRoot;
	   }
	 }
	

}
