package util;

import java.util.Map;

import edu.cmu.sphinx.result.Lattice;
import edu.cmu.sphinx.result.Node;

public class WordHypothesisGraph extends Lattice {
	/**
	 * A normal word trellis/lattice but designed for integrating repair probs (eventually).
	 * This should be isomorphic to the word hypothesis graph.
	 * In fact, this could BE a word hyp graph with an embedded wordgraph
	 * Nodes have the word and the time (hypotheses), edges connect nodes
	 */
	
	
	
	public void removeNode(String nodeID){
		this.removeNode(this.getNode(nodeID));
	}
	
	public void updateScores(Node newnode){ //gets all the relevant measures for this score
		newnode.getEnteringEdges();
	}
	
	
	
	//public void init(){
	//	this.setInitialNode(initialNode); //not sure what this does?
	//}

}
