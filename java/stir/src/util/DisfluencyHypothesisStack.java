package util;

import java.util.Stack;

public class DisfluencyHypothesisStack extends Stack<DisfluencyHypothesis>{
	
	public static int currentID = 0;
	
	//these mirror the constructors of the disfHyps
	public void newHypothesis(){
		this.add(new DisfluencyHypothesis(currentID));
	}
	
	public void newHypothesis(int rpsPosition){
		this.add(new DisfluencyHypothesis(currentID,rpsPosition));
		currentID+=1;
	}
	
	public void newHypothesis(int rpsPosition, int rmsPosition){
		this.add(new DisfluencyHypothesis(currentID,rpsPosition,rmsPosition));
		currentID+=1;
	}
	
	public void newHypothesis(int rpsPosition, float rpsConfidence){
		this.add(new DisfluencyHypothesis(currentID,rpsPosition,rpsConfidence));
		currentID+=1;
	}
	
	public void newHypothesis(int rpsPosition, int rmsPosition, float rpsConfidence, float rmsConfidence){
		this.add(new DisfluencyHypothesis(currentID,rpsPosition,rmsPosition,rpsConfidence,rmsConfidence));
		currentID+=1;
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
