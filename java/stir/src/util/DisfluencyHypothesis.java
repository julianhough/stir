package util;

public class DisfluencyHypothesis {
	
	public static int repairID;
	public static int RPSposition;
	public static int RMSposition;
	public static int RPNposition;
	public static float RPSconfidence;
	public static float RMSconfidence;
	public static float RPNconfidence;
	public static String currentCategory;
	
	//simple constructor
	public DisfluencyHypothesis(int ID){
		repairID =ID;
	}
	
	//some other common constructors we will need
	public DisfluencyHypothesis(int ID, int rpsPosition){
		repairID =ID;
		RPSposition = rpsPosition;
	}
	
	public DisfluencyHypothesis(int ID, int rpsPosition, int rmsPosition){
		repairID =ID;
		RPSposition = rpsPosition;
		RMSposition = rmsPosition;
	}
	
	public DisfluencyHypothesis(int ID, int rpsPosition, float rpsConfidence){
		repairID =ID;
		RPSposition = rpsPosition;
		RPSconfidence = rpsConfidence;	
	}
	
	public DisfluencyHypothesis(int ID, int rpsPosition, int rmsPosition, float rpsConfidence, float rmsConfidence){
		repairID =ID;
		RPSposition = rpsPosition;
		RMSposition = rmsPosition;
		RPSconfidence = rpsConfidence;
		RMSconfidence = rmsConfidence;
		
	}
	
	public static int getRPSposition() {
		return RPSposition;
	}
	public static void setRPSposition(int rPSposition) {
		RPSposition = rPSposition;
	}
	public static int getRMSposition() {
		return RMSposition;
	}
	public static void setRMSposition(int rMSposition) {
		RMSposition = rMSposition;
	}
	public static int getRPNposition() {
		return RPNposition;
	}
	public static void setRPNposition(int rPNposition) {
		RPNposition = rPNposition;
	}
	public static float getRPSconfidence() {
		return RPSconfidence;
	}
	public static void setRPSconfidence(float rPSconfidence) {
		RPSconfidence = rPSconfidence;
	}
	public static float getRMSconfidence() {
		return RMSconfidence;
	}
	public static void setRMSconfidence(float rMSconfidence) {
		RMSconfidence = rMSconfidence;
	}
	public static float getRPNconfidence() {
		return RPNconfidence;
	}
	public static void setRPNconfidence(float rPNconfidence) {
		RPNconfidence = rPNconfidence;
	}
	public static String getCurrentCategory() {
		return currentCategory;
	}
	public static void setCurrentCategory(String currentCategory) {
		DisfluencyHypothesis.currentCategory = currentCategory;
	}
	public static int getRepairID() {
		return repairID;
	}

}
