package languageModel;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.cmu.sphinx.linguist.WordSequence;
import edu.cmu.sphinx.linguist.language.ngram.SimpleNGramModel;
import edu.cmu.sphinx.linguist.acoustic.UnitManager;
import edu.cmu.sphinx.linguist.dictionary.FullDictionary;
import edu.cmu.sphinx.linguist.dictionary.Pronunciation;
import edu.cmu.sphinx.linguist.dictionary.Word;
import edu.cmu.sphinx.util.LogMath;
import edu.cmu.sphinx.util.props.ConfigurationManagerUtils;

public class STIRLanguageModel{
	
	/**
	 * Possibly multi-sourced language model for the 'fluent' model, using the N-gram model of inproTK or other sources
	 * Possibly using a grammar too- DS-TTR or RMRS
	 */
	
	public static int ORDER = 3; //tri-gram
	public static int DELTA = ORDER-1;//note, not an ASR delta! Just for convenience.
	public static int ANTILOG_FACTOR = 2;//TODO a bit of a hack transforming back and forth, ideally get it in raw prob format
	public static int LOG_FACTOR = 2; 
	public static SimpleNGramModel ngram_LM = null;
	public static String LMhomedir = "resources" + File.separatorChar + "Cocolab_DE_8gau_13dCep_16k_40mel_130Hz_6800Hz";
	public static String FullDictionaryLocation = LMhomedir + File.separatorChar + "dict" + File.separatorChar + "Cocolab_DE.lex";
	public static String FillerDictionaryLocation = LMhomedir + File.separatorChar + "dict" + File.separatorChar + "Cocolab_DE.filler";
	public static String LanagugeModelLocation = LMhomedir + File.separatorChar + "LM" + File.separatorChar + "inproCorpora_all.lm";
	
	//constructors
	public STIRLanguageModel(){
		init();
	}
	
	public STIRLanguageModel(String fulldictloc, String fillerdictloc, String lmlocation){
		FullDictionaryLocation = fulldictloc;
		FillerDictionaryLocation = fillerdictloc;
		LanagugeModelLocation = lmlocation;
		init();
	}
	
	//math methods
	public static float antilog(float logprob){
		return (float) Math.pow(logprob, (double) ANTILOG_FACTOR); 
	}
	
	private float sum(List<Float> floats) {
		float total = 0;
		for (float f : floats){
			total+=f;
		}
		return total;
	}
	
	
	public float getFeatureValueByName(String measure, List<Word> wordSequence){
		//TODO implement- do we want all things like the alignment features here,
		//or just the stuff contingent on the lm? Probably the latter
		switch (measure) {
		case "wml":
			return this.logProbNormalisedByUnigramWeight(wordSequence);
		}
	   return 0;
	}
	
	
	//sequence prob methods
	public List<Float> wordSequenceLogProb(List<Word> wordSequence,int order){
		/**
		 * Returns the list of logProbs for a wordSequence a given order.
		 * Skips first order-1 (delta) tokens as context TODO should it do this when order < ORDER
		 */
		List<Float> logprobs = new ArrayList<Float>();
		int delta = DELTA;
		if (order!=ORDER){
			delta = order-1; //this simulates what's happening with the current implementation- i.e. just takes the last unigram if order=1
		}
		for (int i=DELTA;i<wordSequence.size();i++){
			logprobs.add(ngram_LM.getProbability(new WordSequence(wordSequence.subList(i-delta, i+1))));
		}
		return logprobs;
		
	}
	
	/**
	 * Returns the sum of the log prob n-grams (where n = order of this language model) of the sequence
	 * divided by the length of the sequence
	 */
	public float logProbNormalisedBySequenceLength(List<Word> wordSequence){
		return sum(wordSequenceLogProb(wordSequence,ORDER))/(float) wordSequence.size(); 
	}
	
	/**
	 * Returns the value for the current word ngram (i.e. a local value)
	 */
	public float logProbNormalisedByUnigramWeight(){
		//TODO implement for last word
		return 0;
	}
	
	
	/**
	 * Returns the sum of the log prob n-grams (where n = order of this language model) of the sequence
	 * weighted by the sum of the unigrams after the first (n-1) context tokens (See Hough and Purver, 2014 EMNLP)
	 */
	public float logProbNormalisedByUnigramWeight(List<Word> wordSequence){
		return sum(wordSequenceLogProb(wordSequence,ORDER))/-sum(wordSequenceLogProb(wordSequence,1));
	}

	
	/**
	 * Initialises the dictionary, filler dictionary and language model
	 */
	public void init(){
		FullDictionary d = new FullDictionary();
		
		try {
			d = new FullDictionary(ConfigurationManagerUtils.resourceToURL(FullDictionaryLocation),
					ConfigurationManagerUtils.resourceToURL(FillerDictionaryLocation),
					 null, false, null, false, false, new UnitManager(),
			          null, 0);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			d.allocate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		LogMath l = new LogMath((float) LOG_FACTOR, false);
		
		try {
			ngram_LM = new SimpleNGramModel(LanagugeModelLocation,d,(float) 0.7,l, 3);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ngram_LM.allocate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		//LanguageModel lm = new LanguageModel();
		STIRLanguageModel l = new STIRLanguageModel();
		Pronunciation[] p = new Pronunciation[0];
		Word w1 = new Word("nimm",p,false);
		//w1.isSentenceStartWord();
		Word w2 = new Word("das",p,false);
		Word w3 = new Word("kreuz",p,false);
		Word uh = new Word("am",p,true);
		Word s = new Word("<s>",p,false);
		Word f = new Word("</s>",p,false);
		Word[] words = {s,s,w1,w2,w3,f};
		List<Word> mywords = Arrays.asList(s,s,w1,w2,w2,w3,f);
		WordSequence wordSequence = new WordSequence(words); 

		//System.out.print(ngram.getNGrams());
		
		//System.out.println(l.ngram.getProbability(wordSequence));
		
		for(WordSequence w : l.ngram_LM.getNGrams()){
			//System.out.println(w);
			for (Word word : w.getWords()){
				if (word.isFiller()&!word.toString().contains("s>")){
					System.out.println(word.toString());
				}
			}
		}
		//System.out.println(l.logProbNormalisedBySequenceLength(mywords));
		//System.out.println(l.logProbNormalisedByUnigramWeight(mywords));

		List<Word> wordt1 = Arrays.asList(s,s,w1);
		List<Word>  wordt2 = Arrays.asList(s,w1,w2);
		List<Word>  wordt3 = Arrays.asList(w1,w2,w3);
		List<Word> wordt3a = Arrays.asList(w1,w2,uh);
		List<Word> wordt3b = Arrays.asList(w2,uh,w2);
		List<Word> wordt3c = Arrays.asList(w1,w2,w2);
		List<Word> wordt3d = Arrays.asList(w1,w2,uh,w2);
		
		List<Word>  wordt4 = Arrays.asList(w2,w3,f);
		//List<Word>  wordt5 = Arrays.asList(w3,f,f);
		List<List<Word>> sequences = Arrays.asList(wordt1,wordt2,wordt3c,wordt3,wordt4);

		for (List<Word> seq : sequences){
			WordSequence wordTri1 = new WordSequence(seq); 
			//System.out.print(l.ngram.getProbability(wordTri1));
			
			System.out.print(l.logProbNormalisedByUnigramWeight(seq));
			System.out.print("\t" + wordTri1.toString()+"\n");
		
		}
		
	}
	
	
}
