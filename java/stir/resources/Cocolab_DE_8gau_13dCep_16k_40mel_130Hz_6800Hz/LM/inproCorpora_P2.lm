#############################################################################
## Copyright (c) 1996, Carnegie Mellon University, Cambridge University,
## Ronald Rosenfeld and Philip Clarkson
## Version 3, Copyright (c) 2006, Carnegie Mellon University 
## Contributors includes Wen Xu, Ananlada Chotimongkol, 
## David Huggins-Daines, Arthur Chan and Alan Black 
#############################################################################
=============================================================================
===============  This file was produced by the CMU-Cambridge  ===============
===============     Statistical Language Modeling Toolkit     ===============
=============================================================================
This is a 3-gram language model, based on a vocabulary of 92 words,
  which begins "$F", "$L", "$T"...
This is an OPEN-vocabulary model (type 1)
  (OOVs were mapped to UNK, which is treated as any other vocabulary word)
Good-Turing discounting was applied.
1-gram frequency of frequency : 31 
2-gram frequency of frequency : 110 33 11 13 5 7 11 
3-gram frequency of frequency : 208 70 23 22 10 16 4 
1-gram discounting ratios : 0.94 
2-gram discounting ratios : 0.43 0.29 
3-gram discounting ratios : 0.51 0.24 
This file is in the ARPA-standard format introduced by Doug Paul.

p(wd3|wd1,wd2)= if(trigram exists)           p_3(wd1,wd2,wd3)
                else if(bigram w1,w2 exists) bo_wt_2(w1,w2)*p(wd3|wd2)
                else                         p(wd3|w2)

p(wd2|wd1)= if(bigram exists) p_2(wd1,wd2)
            else              bo_wt_1(wd1)*p_1(wd2)

All probs and back-off weights (bo_wt) are given in log10 form.

Data formats:

Beginning of data mark: \data\
ngram 1=nr            # number of 1-grams
ngram 2=nr            # number of 2-grams
ngram 3=nr            # number of 3-grams

\1-grams:
p_1     wd_1 bo_wt_1
\2-grams:
p_2     wd_1 wd_2 bo_wt_2
\3-grams:
p_3     wd_1 wd_2 wd_3 

end of data mark: \end\

\data\
ngram 1=92
ngram 2=256
ngram 3=438

\1-grams:
-2.6511 $F	-0.5187
-2.6511 $L	-1.0877
-2.6511 $T	-0.3728
-2.5932 $U	-0.7133
-2.8942 $W	0.0254
-1.7109 $Y	-1.6232
-1.5668 $Z	-1.2057
-3.5234 'n	-0.2429
-0.5905 </s>	-2.9063
-98.9999 <s>	-1.3431
-3.5234 Bal	-0.1142
-1.7887 Balken	-1.0913
-2.6511 Bayonett	-0.5757
-2.6511 Brücke	-0.9296
-2.6511 Dreieck	-0.2091
-2.8942 Eins	0.0374
-2.2658 Gewehr	-0.9680
-3.5234 Gott	-0.2428
-2.5932 Hand	-0.5828
-1.8241 Handy	-1.4296
-1.9777 Kreuz	-1.1897
-2.3501 Krücke	-0.7919
-1.9399 Mast	-1.0145
-3.5234 Mikro	-0.1142
-1.5878 Pistole	-1.9667
-2.6511 Plus	-1.1178
-2.6511 Schlange	-0.2634
-1.9052 Sieben	-0.8768
-3.5234 T	-0.1142
-2.5420 Teil	-0.9125
-1.9052 Teile	-1.4839
-1.7639 Treppe	-1.8119
-3.5234 Win	-0.2349
-1.7559 Winkel	-0.9488
-3.5234 a	-0.2405
-3.5234 ah	-0.2428
-3.5234 aha	-0.2354
-1.9052 alle	-1.6073
-3.5234 alles	-0.2366
-2.8942 bin	-0.7775
-3.5234 d	-0.1142
-3.5234 da	-0.2117
-0.8668 das	-1.9074
-3.5234 de	-0.1796
-3.5234 den	-0.2380
-1.3409 der	-1.8341
-1.1578 die	-1.9521
-2.0649 dieses	-1.3327
-3.5234 dis	-0.2429
-3.1952 doch	-0.2114
-3.0191 dum	-0.1122
-1.7802 einmal	-1.6028
-1.5469 falsch	-1.8302
-2.7973 fertig	-0.7162
-3.5234 gekippte	-0.2311
-3.5234 ha	-0.1142
-3.5234 haben	-0.2429
-3.5234 hallo	-0.1142
-2.2410 hm	-1.2875
-3.5234 huch	-0.2428
-2.8942 ich	-0.7776
-3.5234 is	-0.2429
-2.8942 ist	-0.8641
-2.0049 ja	-1.1368
-3.5234 jetz	-0.1796
-3.0191 jetzt	-0.1559
-3.5234 links	-0.2425
-3.5234 m	-0.2405
-3.5234 mal	-0.2429
-3.1952 n	-0.1424
-3.1952 nee	-0.1003
-1.7328 nein	-1.2126
-3.1952 nich	-0.0172
-1.6329 nicht	-1.2479
-3.5234 nimm	-0.2425
-1.5095 noch	-1.5372
-3.5234 nochma	-0.2366
-3.1952 nochmal	-0.1397
-3.1952 nun	-0.2428
-3.1952 oh	-0.2428
-3.5234 schon	-0.2423
-1.7559 und	-1.8577
-3.5234 ups	-0.1142
-1.8335 von	-1.6838
-2.7181 vorn	-0.7742
-1.8942 vorne	-1.4944
-1.5617 war	-1.9320
-1.9052 waren	-1.4279
-3.5234 wir	-0.2117
-3.5234 wo	-0.2429
-2.8942 äh	-0.2203
-2.3202 ähm	-0.1968

\2-grams:
-1.0881 $F </s> 2.7865
-0.1461 $F die -0.9891
-1.3222 $L das -0.2591
-0.3522 $L die -0.5732
-0.3522 $L und -0.3668
-1.2711 $T </s> 2.5958
-0.2041 $T das -0.6815
-1.1461 $T und 0.1292
-0.1091 $U </s> 2.0612
-1.3222 $U die -0.2817
-1.3222 $U und -0.0753
-0.8451 $W </s> 2.7865
-0.8451 $W die -0.1118
-0.0658 $Y </s> 1.1660
-1.2041 $Y die -0.5879
-1.2041 $Y und -0.6715
-0.0944 $Z </s> 1.0612
-2.3075 $Z das -0.3101
-2.1826 $Z der 0.0861
-0.8256 $Z die -0.9122
-2.3075 $Z und -0.2514
-0.3680 'n dis -0.0675
-0.0005 </s> <s> -0.0144
-3.1504 <s> $Y 0.7321
-3.2754 <s> $Z 0.3986
-3.2754 <s> Balken 0.3641
-3.2754 <s> Treppe 1.4299
-3.2754 <s> Winkel 0.7387
-3.2754 <s> a -0.0675
-3.2754 <s> d -0.0675
-0.3969 <s> das -0.4159
-0.8392 <s> der 0.0197
-0.7675 <s> die -1.2818
-3.1504 <s> dieses 0.0351
-3.2754 <s> dum -0.2187
-3.2754 <s> ha -0.0675
-3.2754 <s> hallo -0.0675
-1.7935 <s> hm 0.7104
-2.2084 <s> ich 0.0792
-1.4160 <s> ja 0.0664
-3.2754 <s> jetz -0.0675
-3.1504 <s> jetzt -0.1644
-3.2754 <s> m -0.0675
-3.1504 <s> n 0.0263
-1.1831 <s> nein -0.1503
-3.2754 <s> nich -0.1644
-1.6521 <s> nicht -0.7984
-3.2754 <s> nimm -0.0675
-1.2739 <s> noch 0.4540
-3.2754 <s> nochma -0.0675
-3.1504 <s> nun -0.0675
-3.1504 <s> oh -0.0675
-1.5852 <s> und 0.4181
-3.2754 <s> ups -0.0675
-3.2754 <s> wo -0.0675
-3.1504 <s> äh -0.2058
-1.7313 <s> ähm -0.0339
-0.3680 Bal </s> 2.5958
-0.1032 Balken </s> 1.2936
-1.1139 Balken das -0.8189
-1.1139 Balken der -0.3370
-1.9590 Balken die 0.0819
-2.0840 Balken und -0.0615
-0.1249 Bayonett </s> 2.1282
-1.2711 Bayonett das -0.3034
-1.2711 Bayonett die -0.2943
-0.0580 Brücke </s> 2.0612
-1.2711 Brücke das -0.2452
-0.3010 Dreieck </s> 2.3043
-1.2711 Dreieck das -0.3034
-1.1461 Dreieck die -0.1056
-1.2711 Dreieck und -0.0753
-0.8451 Eins </s> 2.7865
-0.9700 Eins das -0.3064
-0.9700 Eins und -0.0615
-0.0512 Gewehr </s> 1.7022
-1.6232 Gewehr das -0.2591
-1.6232 Gewehr und -0.0615
-0.3680 Gott nee -0.2058
-1.1973 Hand </s> 2.7865
-0.1091 Hand das -0.7798
-0.1549 Handy </s> 1.3623
-0.8539 Handy das -1.1402
-0.8539 Handy die -0.8163
-0.1513 Kreuz </s> 1.5261
-0.6284 Kreuz das -0.8623
-1.8995 Kreuz die -0.3045
-1.8995 Kreuz und -0.0615
-0.2218 Krücke </s> 1.9521
-0.5740 Krücke das -0.8520
-1.4191 Krücke und 0.1292
-0.1027 Mast </s> 1.4292
-0.8808 Mast das -0.6918
-1.9478 Mast der -0.1046
-1.8228 Mast und 0.1292
-0.3680 Mikro </s> 2.5958
-0.1129 Pistole </s> 1.1002
-1.0160 Pistole die -0.7821
-2.2871 Pistole nein 0.1889
-0.9191 Pistole und 1.2226
-0.0580 Plus der -0.7232
-1.2711 Plus und -0.0615
-1.1461 Schlange </s> 2.7865
-1.2711 Schlange aha -0.0675
-0.3010 Schlange das -0.5949
-1.2711 Schlange und -0.0615
-0.0458 Sieben </s> 1.3500
-1.8451 Sieben das -0.1158
-1.8451 Sieben die -0.1118
-0.3680 T </s> 2.5958
-0.0414 Teil </s> 1.9063
-0.0107 Teile </s> 1.3043
-0.0080 Treppe </s> 1.1739
-2.1083 Treppe die -0.1088
-0.3680 Win nein 0.1889
-0.0406 Winkel </s> 1.1988
-2.1162 Winkel das -0.2174
-1.9912 Winkel der 0.0717
-1.9912 Winkel und 0.5268
-0.3680 a hm 0.1231
-0.3680 ah nee -0.2058
-0.3680 aha und -0.0615
-0.0107 alle Teile 0.0107
-0.3680 alles von 0.4766
-0.0792 bin fertig 0.1461
-0.3680 d </s> 2.5958
-0.3680 da die -0.2943
-1.7884 das $F 0.0263
-1.7884 das $L 0.0417
-1.7884 das $T 0.0595
-1.7304 das $U -0.0163
-2.0314 das $W 0.0263
-0.8553 das $Y 0.0280
-0.7144 das $Z -0.1541
-3.0014 das </s> 2.5958
-1.7884 das Bayonett -0.0095
-1.7884 das Dreieck 0.0451
-1.4030 das Gewehr -0.0426
-0.9522 das Handy 0.0177
-1.1150 das Kreuz -0.0545
-1.7884 das Plus -0.0095
-3.0014 das T -0.0675
-3.0014 das den -0.0675
-3.0014 das die -0.1088
-3.0014 das gekippte -0.0675
-2.0314 das ist 0.0969
-0.6939 das war 0.0050
-1.0424 das waren 0.0373
-0.3680 de das -0.2591
-0.3680 den Mast 0.3662
-2.5233 der Bal -0.0675
-0.4478 der Balken 0.0099
-0.6113 der Mast 0.0372
-2.5233 der Win -0.0675
-0.4229 der Winkel 0.0278
-2.5233 der das -0.2174
-2.7064 die </s> 2.5958
-1.4934 die Brücke -0.0095
-1.7364 die Eins -0.0129
-1.4354 die Hand 0.0775
-1.1923 die Krücke 0.0563
-0.4300 die Pistole -0.0569
-1.4934 die Schlange 0.0451
-0.7474 die Sieben 0.0373
-0.6142 die Treppe -0.0514
-1.8613 die das -0.1862
-2.7064 die die -0.1088
-0.4771 dieses </s> 1.9063
-0.4771 dieses Teil 0.0414
-0.5229 dieses nicht -0.7974
-0.3680 dis Mikro -0.0675
-0.6690 doch die -0.2943
-0.6690 doch nich -0.1644
-0.8451 dum das -0.2174
-0.7202 dum dum -0.1344
-0.6990 einmal </s> 1.8649
-0.1069 einmal von 0.0567
-0.0048 falsch </s> 0.9521
-0.0669 fertig </s> 2.1282
-0.3680 gekippte $Z -0.3084
-0.3680 ha </s> 2.5958
-0.3680 haben wir -0.0675
-0.3680 hallo </s> 2.5958
-0.1996 hm </s> 1.8272
-0.8016 hm das 0.0439
-0.8016 hm hm 0.5021
-1.6467 hm nein -0.2647
-0.3680 huch doch -0.2058
-0.0792 ich bin 0.0792
-0.3680 is 'n -0.0675
-1.0669 ist das -0.2174
-0.0969 ist falsch 1.3570
-0.3274 ja </s> 1.7022
-0.8325 ja das -0.6577
-0.4901 ja noch -0.7034
-1.7745 ja nochmal 0.0263
-0.3680 jetz das -0.3034
-0.8451 jetzt das -0.3034
-0.8451 jetzt der -0.1046
-0.8451 jetzt haben -0.0675
-0.3680 links ist -0.2716
-0.3680 m hm 0.1231
-0.3680 mal alles -0.0675
-0.5441 n dieses 0.0351
-0.6690 nee </s> 2.5958
-0.6690 nee nicht -0.1836
-1.0000 nein </s> 2.1282
-1.1761 nein das 0.0308
-1.0000 nein dieses -0.6232
-2.1461 nein doch -0.2058
-2.0212 nein nicht 0.0071
-0.1654 nein noch 0.0825
-0.5441 nich </s> 2.7865
-0.1849 nicht </s> 1.2161
-2.2430 nicht ah -0.0675
-2.1181 nicht alle 1.4930
-2.2430 nicht der -0.1886
-1.3979 nicht die 0.0386
-0.5963 nicht dieses -0.8016
-0.3680 nimm äh -0.2613
-2.2343 noch </s> 2.7865
-0.2670 noch einmal 0.0161
-2.3592 noch mal -0.0675
-0.3680 noch nicht -0.7964
-0.3680 nochma von 0.4766
-0.5441 nochmal von 1.3797
-0.6690 nun da -0.0675
-0.6690 nun de -0.0675
-0.6690 oh Gott -0.0675
-0.6690 oh schon -0.0675
-0.3680 schon fertig 0.5346
-0.3602 und das -0.3529
-0.8953 und der 0.8236
-0.3786 und die -0.5642
-2.1083 und huch -0.0675
-0.3680 ups </s> 2.5958
-0.8451 von vorn 0.0580
-0.0774 von vorne 0.0105
-0.0580 vorn </s> 2.0612
-0.0105 vorne </s> 1.2936
-0.0050 war falsch 0.0195
-0.0223 waren alle 0.0330
-1.8451 waren noch 0.1232
-0.3680 wir die -0.2943
-0.3680 wo is -0.0675
-0.9700 äh links -0.0675
-0.9700 äh nein -0.3074
-0.9700 äh noch 0.0275
-0.9700 äh äh -0.2613
-1.5441 ähm </s> 2.5958
-0.2730 ähm das -0.1960
-1.4191 ähm der 0.0717
-1.5441 ähm die -0.3045
-1.5441 ähm jetzt -0.2436
-1.5441 ähm nein 0.1889
-1.5441 ähm nicht 0.1496

\3-grams:
-0.6178 $F </s> <s> 
-0.0969 $F die Schlange 
-0.9907 $F die das 
-0.2918 $L das Handy 
-0.1249 $L die Krücke 
-0.1249 $L und die 
-0.2918 $T </s> <s> 
-0.0969 $T das Gewehr 
-0.6178 $T und das 
-0.0669 $U </s> <s> 
-0.2918 $U die Krücke 
-0.2918 $U und die 
-0.6178 $W </s> <s> 
-0.6178 $W die Eins 
-0.0080 $Y </s> <s> 
-0.1249 $Y die Brücke 
-0.1249 $Y und die 
-0.8938 $Y und huch 
-0.0062 $Z </s> <s> 
-0.2918 $Z das die 
-0.6178 $Z der Winkel 
-0.0348 $Z die Pistole 
-0.2918 $Z und der 
-0.2918 'n dis Mikro 
-3.2226 </s> <s> $Y 
-3.1976 </s> <s> $Z 
-3.1976 </s> <s> Balken 
-3.1976 </s> <s> Treppe 
-3.1976 </s> <s> Winkel 
-3.1976 </s> <s> a 
-3.1976 </s> <s> d 
-0.3953 </s> <s> das 
-0.8376 </s> <s> der 
-0.7659 </s> <s> die 
-3.2226 </s> <s> dieses 
-3.1976 </s> <s> dum 
-3.1976 </s> <s> ha 
-3.1976 </s> <s> hallo 
-1.7919 </s> <s> hm 
-2.3037 </s> <s> ich 
-1.4144 </s> <s> ja 
-3.1976 </s> <s> jetz 
-3.2226 </s> <s> jetzt 
-3.1976 </s> <s> m 
-3.2226 </s> <s> n 
-1.1815 </s> <s> nein 
-3.1976 </s> <s> nich 
-1.6505 </s> <s> nicht 
-3.1976 </s> <s> nimm 
-1.2723 </s> <s> noch 
-3.1976 </s> <s> nochma 
-3.2226 </s> <s> nun 
-3.2226 </s> <s> oh 
-1.6048 </s> <s> und 
-3.1976 </s> <s> ups 
-3.1976 </s> <s> wo 
-3.2226 </s> <s> äh 
-1.7597 </s> <s> ähm 
-0.6178 <s> $Y </s> 
-0.2918 <s> $Z </s> 
-0.2918 <s> Balken </s> 
-0.2918 <s> Treppe </s> 
-0.2918 <s> Winkel </s> 
-0.2918 <s> a hm 
-0.2918 <s> d </s> 
-2.0334 <s> das $F 
-2.8023 <s> das $L 
-2.8023 <s> das $T 
-2.8273 <s> das $U 
-0.8478 <s> das $Y 
-0.6976 <s> das $Z 
-2.8023 <s> das </s> 
-2.8273 <s> das Dreieck 
-1.6075 <s> das Gewehr 
-0.8978 <s> das Handy 
-1.0192 <s> das Kreuz 
-1.9085 <s> das Plus 
-2.0334 <s> das ist 
-0.5915 <s> das war 
-0.9920 <s> das waren 
-2.3599 <s> der Bal 
-0.4150 <s> der Balken 
-0.6368 <s> der Mast 
-2.3599 <s> der Win 
-0.4449 <s> der Winkel 
-2.3599 <s> der das 
-2.4316 <s> die Brücke 
-1.5378 <s> die Hand 
-0.3617 <s> die Pistole 
-0.5958 <s> die Sieben 
-0.5717 <s> die Treppe 
-2.4316 <s> die die 
-0.6178 <s> dieses nicht 
-0.2918 <s> dum dum 
-0.2918 <s> ha </s> 
-0.2918 <s> hallo </s> 
-0.2688 <s> hm </s> 
-0.6368 <s> hm das 
-1.4307 <s> hm hm 
-1.4057 <s> hm nein 
-0.0969 <s> ich bin 
-0.3153 <s> ja </s> 
-0.8893 <s> ja das 
-0.4914 <s> ja noch 
-1.8081 <s> ja nochmal 
-0.2918 <s> jetz das 
-0.5928 <s> jetzt das 
-0.5928 <s> jetzt der 
-0.2918 <s> m hm 
-0.6178 <s> n dieses 
-1.1222 <s> nein </s> 
-1.1222 <s> nein das 
-1.0253 <s> nein dieses 
-2.0410 <s> nein nicht 
-0.1445 <s> nein noch 
-0.2918 <s> nich </s> 
-1.5470 <s> nicht der 
-1.5720 <s> nicht die 
-0.0792 <s> nicht dieses 
-0.2918 <s> nimm äh 
-1.9502 <s> noch </s> 
-0.0424 <s> noch einmal 
-1.9252 <s> noch mal 
-1.9252 <s> noch nicht 
-0.2918 <s> nochma von 
-0.5928 <s> nun da 
-0.5928 <s> nun de 
-0.5928 <s> oh Gott 
-0.5928 <s> oh schon 
-0.4191 <s> und das 
-0.7202 <s> und der 
-0.4191 <s> und die 
-0.2918 <s> ups </s> 
-0.2918 <s> wo is 
-0.5928 <s> äh nein 
-0.5928 <s> äh noch 
-1.4678 <s> ähm </s> 
-0.2730 <s> ähm das 
-1.4928 <s> ähm der 
-1.4678 <s> ähm die 
-1.4678 <s> ähm jetzt 
-1.4678 <s> ähm nein 
-1.4678 <s> ähm nicht 
-0.2918 Bal </s> <s> 
-0.0107 Balken </s> <s> 
-0.1249 Balken das $Z 
-0.8938 Balken das gekippte 
-0.8938 Balken der Balken 
-0.1249 Balken der Winkel 
-0.6178 Balken die Pistole 
-0.2918 Balken und das 
-0.0792 Bayonett </s> <s> 
-0.2918 Bayonett das Plus 
-0.2918 Bayonett die Hand 
-0.0669 Brücke </s> <s> 
-0.2918 Brücke das $Y 
-0.1249 Dreieck </s> <s> 
-0.2918 Dreieck das $F 
-0.6178 Dreieck die Schlange 
-0.2918 Dreieck und die 
-0.6178 Eins </s> <s> 
-0.2918 Eins das $W 
-0.2918 Eins und das 
-0.0280 Gewehr </s> <s> 
-0.2918 Gewehr das Handy 
-0.2918 Gewehr und das 
-0.2918 Gott nee </s> 
-0.6178 Hand </s> <s> 
-0.0669 Hand das $Y 
-0.0126 Handy </s> <s> 
-0.0669 Handy das $L 
-1.1369 Handy das $U 
-0.0669 Handy die Krücke 
-0.0185 Kreuz </s> <s> 
-0.1249 Kreuz das $T 
-1.1948 Kreuz das Kreuz 
-1.1948 Kreuz das Plus 
-0.2918 Kreuz die das 
-0.2918 Kreuz und das 
-0.0512 Krücke </s> <s> 
-0.1249 Krücke das $U 
-0.8938 Krücke das Handy 
-0.6178 Krücke und das 
-0.0147 Mast </s> <s> 
-0.0969 Mast das Bayonett 
-0.2918 Mast der Winkel 
-0.6178 Mast und das 
-0.2918 Mikro </s> <s> 
-0.0068 Pistole </s> <s> 
-0.0580 Pistole die Treppe 
-0.2918 Pistole nein noch 
-1.3168 Pistole und das 
-1.3168 Pistole und der 
-0.2218 Pistole und die 
-0.0669 Plus der Mast 
-0.2918 Plus und das 
-0.6178 Schlange </s> <s> 
-0.2918 Schlange aha und 
-0.1249 Schlange das Dreieck 
-0.2918 Schlange und das 
-0.0122 Sieben </s> <s> 
-0.6178 Sieben das $W 
-0.6178 Sieben die Eins 
-0.2918 T </s> <s> 
-0.0458 Teil </s> <s> 
-0.0110 Teile </s> <s> 
-0.0081 Treppe </s> <s> 
-0.2918 Treppe die Pistole 
-0.2918 Win nein noch 
-0.0086 Winkel </s> <s> 
-0.2918 Winkel das $Z 
-0.6178 Winkel der Balken 
-0.5928 Winkel und das 
-0.5928 Winkel und die 
-0.2918 a hm </s> 
-0.2918 ah nee nicht 
-0.2918 aha und das 
-0.0110 alle Teile </s> 
-0.2918 alles von vorne 
-0.0969 bin fertig </s> 
-0.2918 d </s> <s> 
-0.2918 da die Hand 
-1.1619 das $F </s> 
-0.1461 das $F die 
-1.1369 das $L das 
-0.3680 das $L die 
-0.3680 das $L und 
-1.1369 das $T </s> 
-0.2430 das $T das 
-1.1619 das $T und 
-0.1249 das $U </s> 
-1.1948 das $U die 
-1.1948 das $U und 
-0.9188 das $W </s> 
-0.9188 das $W die 
-0.0621 das $Y </s> 
-1.3010 das $Y die 
-1.1761 das $Y und 
-0.0866 das $Z </s> 
-2.2108 das $Z das 
-2.2358 das $Z der 
-0.8399 das $Z die 
-0.2918 das </s> <s> 
-0.1461 das Bayonett </s> 
-1.1369 das Bayonett das 
-1.1369 das Bayonett die 
-0.3680 das Dreieck </s> 
-1.1369 das Dreieck das 
-1.1619 das Dreieck die 
-1.1369 das Dreieck und 
-0.0544 das Gewehr </s> 
-1.5222 das Gewehr das 
-1.5222 das Gewehr und 
-0.1498 das Handy </s> 
-0.8361 das Handy das 
-0.9031 das Handy die 
-0.1568 das Kreuz </s> 
-0.6154 das Kreuz das 
-1.8103 das Kreuz die 
-1.8103 das Kreuz und 
-0.0669 das Plus der 
-1.1369 das Plus und 
-0.2918 das T </s> 
-0.2918 das den Mast 
-0.2918 das die Pistole 
-0.2918 das gekippte $Z 
-0.1249 das ist falsch 
-0.0050 das war falsch 
-0.0229 das waren alle 
-1.9078 das waren noch 
-0.2918 de das Handy 
-0.2918 den Mast </s> 
-0.2918 der Bal </s> 
-0.1055 der Balken </s> 
-1.1055 der Balken das 
-1.1055 der Balken der 
-2.0243 der Balken die 
-1.9993 der Balken und 
-0.0969 der Mast </s> 
-0.9420 der Mast das 
-1.8358 der Mast der 
-1.8608 der Mast und 
-0.2918 der Win nein 
-0.0422 der Winkel </s> 
-2.0242 der Winkel das 
-2.0491 der Winkel der 
-2.0491 der Winkel und 
-0.2918 der das $Z 
-0.2918 die </s> <s> 
-0.0669 die Brücke </s> 
-1.1369 die Brücke das 
-0.9188 die Eins </s> 
-0.8938 die Eins das 
-0.8938 die Eins und 
-1.2198 die Hand </s> 
-0.1249 die Hand das 
-0.2430 die Krücke </s> 
-0.5441 die Krücke das 
-1.4629 die Krücke und 
-0.1091 die Pistole </s> 
-1.0634 die Pistole die 
-2.2002 die Pistole nein 
-0.9085 die Pistole und 
-1.1619 die Schlange </s> 
-1.1369 die Schlange aha 
-0.3680 die Schlange das 
-1.1369 die Schlange und 
-0.0470 die Sieben </s> 
-1.9078 die Sieben das 
-1.9078 die Sieben die 
-0.0083 die Treppe </s> 
-2.0160 die Treppe die 
-0.7689 die das $Z 
-0.7689 die das Dreieck 
-0.7689 die das Gewehr 
-0.2918 die die Pistole 
-0.0458 dieses </s> <s> 
-0.0458 dieses Teil </s> 
-0.0512 dieses nicht </s> 
-1.2460 dieses nicht ah 
-0.2918 dis Mikro </s> 
-0.2918 doch die Hand 
-0.2918 doch nich </s> 
-0.2918 dum das $Z 
-0.5928 dum dum das 
-0.5928 dum dum dum 
-0.0414 einmal </s> <s> 
-0.9345 einmal von vorn 
-0.0653 einmal von vorne 
-0.0049 falsch </s> <s> 
-0.0792 fertig </s> <s> 
-0.2918 gekippte $Z und 
-0.2918 ha </s> <s> 
-0.2918 haben wir die 
-0.2918 hallo </s> <s> 
-0.0378 hm </s> <s> 
-0.7939 hm das $Z 
-0.7689 hm das war 
-0.7939 hm hm </s> 
-0.7689 hm hm hm 
-0.2918 hm nein </s> 
-0.2918 huch doch die 
-0.0969 ich bin fertig 
-0.2918 is 'n dis 
-0.2918 ist das $Z 
-0.1249 ist falsch </s> 
-0.0280 ja </s> <s> 
-0.0969 ja das waren 
-0.0414 ja noch einmal 
-0.6178 ja nochmal von 
-0.2918 jetz das Bayonett 
-0.2918 jetzt das $F 
-0.2918 jetzt der Winkel 
-0.2918 jetzt haben wir 
-0.2918 links ist das 
-0.2918 m hm </s> 
-0.2918 mal alles von 
-0.6178 n dieses nicht 
-0.2918 nee </s> <s> 
-0.2918 nee nicht dieses 
-0.0792 nein </s> <s> 
-0.9188 nein das war 
-0.9188 nein das waren 
-0.0792 nein dieses nicht 
-0.2918 nein doch nich 
-0.6178 nein nicht dieses 
-1.9295 nein noch einmal 
-0.0217 nein noch nicht 
-0.6178 nich </s> <s> 
-0.0090 nicht </s> <s> 
-0.2918 nicht ah nee 
-0.6178 nicht alle Teile 
-0.2918 nicht der Mast 
-0.7689 nicht die </s> 
-0.7689 nicht die Pistole 
-0.7689 nicht die Sieben 
-0.3245 nicht dieses </s> 
-0.3245 nicht dieses Teil 
-0.2918 nimm äh äh 
-0.6178 noch </s> <s> 
-0.7243 noch einmal </s> 
-0.1010 noch einmal von 
-0.2918 noch mal alles 
-0.0322 noch nicht </s> 
-1.9400 noch nicht alle 
-1.9150 noch nicht die 
-0.2918 nochma von vorne 
-0.5928 nochmal von vorn 
-0.5928 nochmal von vorne 
-0.2918 nun da die 
-0.2918 nun de das 
-0.2918 oh Gott nee 
-0.2918 oh schon fertig 
-0.2918 schon fertig </s> 
-1.6970 und das $F 
-1.6970 und das $U 
-1.6720 und das $W 
-0.6021 und das $Y 
-0.6812 und das $Z 
-1.6970 und das Bayonett 
-0.7782 und das Gewehr 
-1.6720 und das T 
-1.6720 und das den 
-1.1369 und der Balken 
-1.1369 und der Mast 
-0.1461 und der Winkel 
-0.8846 und die Brücke 
-0.7597 und die Krücke 
-0.8846 und die Pistole 
-1.6535 und die Schlange 
-0.8846 und die Sieben 
-0.4075 und die Treppe 
-0.2918 und huch doch 
-0.2918 ups </s> <s> 
-0.0669 von vorn </s> 
-0.0107 von vorne </s> 
-0.0669 vorn </s> <s> 
-0.0107 vorne </s> <s> 
-0.0050 war falsch </s> 
-0.0116 waren alle Teile 
-0.6178 waren noch nicht 
-0.2918 wir die Hand 
-0.2918 wo is 'n 
-0.2918 äh links ist 
-0.2918 äh nein doch 
-0.2918 äh noch einmal 
-0.2918 äh äh links 
-0.2918 ähm </s> <s> 
-0.4260 ähm das $Z 
-1.1948 ähm das Dreieck 
-1.2198 ähm das Handy 
-1.1948 ähm das Kreuz 
-1.1948 ähm das Plus 
-0.6178 ähm der Balken 
-0.2918 ähm die das 
-0.2918 ähm jetzt haben 
-0.2918 ähm nein noch 
-0.2918 ähm nicht </s> 

\end\
