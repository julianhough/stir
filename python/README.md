To run the code here you need to have `Python 2.7` installed, and also `pip` for installing the dependencies. (Also `IPython` should be installed, preferrably, if you want to run the notebooks).

You then need to run `sudo pip install -r requirements.txt` from the command line from this folder.

## Running TOPICs experiments

To run the experiments from the TOPICs special issue on miscommunication paper, please cite the below:

```
@article{PurverEtAl18Topics,
  author = "Matthew Purver and Julian Hough and Christine Howes",
  journal = "Topics in Cognitive Science",
  title = "{C}omputational models of miscommunication phenomena",
  volume = "submitted, under review",
  year = "2018"
}

```

To reproduce the results from our pre-trained STIR models, you must clone [https://bitbucket.org/julianhough/stir_data] as the sister directory to this repository's location, then run the below from here:

	`python experiments/TOPICS_Miscommunication.py`

Once complete, you can see most of the results of interest as `class, precision, recall, f1-score` comma-separated lines printed out at the console for each file- of particular interest is the top `per-utterance <rps` score, and for the switchboard files the line beginning `<rm,` for reparandum word detection accuracy. 

The full evaluation and error analyses in the paper can be obtained by running the notebook at `experiments/analysis/TOPICS_Miscommunication.ipynb`.

If you wish to run the entire training process from the Switchboard data from scratch, adjust the `TOPICS_Miscommunication.py` script so the variable `TRAIN` near the top of the file is set to `True` before running the script.
