package pipeline;

public class STIRcomponent extends Component{

	public STIRcomponent(String trainfilename, 
			String costfilename, String predictionDirectory, String range, String[] testfiles){
		super(trainfilename, costfilename, predictionDirectory, range, testfiles);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//for (String s: args) {
        //   System.out.println(s);
        //}
		if (args.length == 0){
			System.out.println("NO args!");
			//String[] myargs = new String[]{"C:\\users\\julian\\phd\\corpora\\swda\\acl\\SW1\\3_3SW1FullREFbacksearch\\Edit_Term_Data_SWJCHeldoutREF.csv"}; //just the one arg
			//STIRcomponent e = new STIRcomponent("C:\\users\\julian\\phd\\corpora\\swda\\acl\\SW1\\3_3SW1FullREFbacksearch\\Edit_Term_Data_SW1FullREF.csv",
			//"C:\\users\\julian\\phd\\corpora\\swda\\acl\\SW1\\3_3SW1FullREFbacksearch\\costs\\Edit3.csv", 
			//"C:\\users\\julian\\phd\\corpora\\swda\\acl\\SW1\\3_3SW1FullREFbacksearch\\Edit_Term_Predictions_1\\Edit_Term_Predictions_1","9,10,11",myargs);
		}
		else {
			System.out.println("Loading files");
			String[] testfiles = new String[args.length-5];
			int a = 0;
			int i = 0;
			//getting right training, test and cost files
			for (String s: args){
				System.out.println(a);
				System.out.println(s);
				if (a>3&a<(args.length-1)) //last one is always the model
				{testfiles[i] = s; i++;}
				a++;
			}
			System.out.print("TEST FILES");
			System.out.print(testfiles);
			//if no model is in the args, initialize
			String model = args[(args.length-1)];
			String training_data = args[1];
			STIRcomponent component = new STIRcomponent(args[0], args[1], 
				args[2], args[3], testfiles);
			if (model.equals("None")){
				try{
					component.train();
					component.test(); //testing on the test files
				
				} catch (Exception e){
					e.printStackTrace();
				}
			} else {
				try {
					component.loadModel(model);
					component.test(); //testing on the test files
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
