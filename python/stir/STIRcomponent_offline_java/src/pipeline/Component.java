package pipeline;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.classifiers.Classifier;
import weka.classifiers.CostMatrix;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.MetaCost;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.SerializationHelper;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.NominalToString;

public abstract class Component {
	
	public static ArrayList<String> classes = new ArrayList<String>();
	public static Classifier classifier = null;
	public static Instances train_data = null;
	public static List<Instances> test_data = new ArrayList<Instances>();
	public static String training_data_prediction_file = "";
	public static List<String> test_data_prediction_files = new ArrayList<String>();
	
	/*
	public static String edit_filename = null;
	public static String[] edit_test_files;
	public static String repair_onset_filename = null;
	public static String[] repair_onset_test_files;
	public static String reparandum_onset_filename = null;
	public static String[] reparandum_onset_test_files;
	public static String repair_extent_filename = null;
	public static String[] repair_extent_test_files;
	*/
	
	public static String cost_matrix_filename = null;

	/**
	 * 
	 * @param trainfilename
	 * @param costfilename
	 * @param predictionDirectory
	 * @param range The attributes wanted for classification eg. first-3,5,6-last the default should be first-last
	 * @param testfiles
	 */
	public Component(String trainfilename, String costfilename, String predictionDirectory, String range, String[] testfiles){
		try{
			init(trainfilename, costfilename, predictionDirectory, range, testfiles);
			//train();
			//test(); //testing on the test files
		
		} catch (Exception e){
			e.printStackTrace();
		}
	
	}
	
	
	/***
	 * Generates a WEKA compatible cost matrix file from a simple .csv file with the costs
	 */
	public void generateCostFile(String mycostfilename) throws Exception {
		BufferedReader inputStream = null;
		try {
			inputStream = new BufferedReader(new FileReader(mycostfilename));
			String line = inputStream.readLine(); //first line is the classified as line
			String[] classifiedAs = line.split(",");
			Map<String, Map<String, String>> costMap = 
				    new HashMap<String, Map<String, String>>();
			for (int s=0; s<classifiedAs.length-1; s++){
				line = inputStream.readLine(); //goes to the next line with the number of classes, as they should be the same
				String[] row = line.split(",");
				if (!row[row.length-1].equals(classifiedAs[s])){ //assuming a well-formed matrix
					System.out.println("NOT THE SAME CLASS IN MATRIX ROW AND COLUMN %%%" + classifiedAs[s] + "%%%");
					for (String r : row){
						System.out.println(r);
					}
					System.out.println("%%%"+row[row.length-1]+"%%%");
					System.exit(0);
				}
				Map<String,String> columnCosts = new HashMap<String,String>();
				//embedded loop
				for (int r=0; r<row.length-1; r++){
					columnCosts.put(classifiedAs[r],row[r]);
				}
				costMap.put(row[row.length-1],columnCosts);
			}
			//now we create a cost file in accordance with the order of classes found in training
			String costFileString = "%\tRows\tColumns\n" + String.valueOf(classes.size()) + "\t" + String.valueOf(classes.size()) + "\n";
			costFileString+="%\tMatrix elements\n";
			for (String myclass: classes){
				for (String myinnerclass: classes){
					costFileString+=costMap.get(myclass).get(myinnerclass) +"\t";
				}
				costFileString = costFileString.substring(0,costFileString.length()-1); //-1 or -2
				costFileString+="\n";
			}
			//writes to a new file
			System.out.println(costFileString);
			System.out.println("writing to file");
			cost_matrix_filename = mycostfilename.substring(0,mycostfilename.indexOf(".")) + ".cost";
			System.out.println(cost_matrix_filename);
			PrintWriter writer = new PrintWriter(cost_matrix_filename, "UTF-8");
			writer.print(costFileString);
			writer.close();
			
		}  finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}

	}
	
	
	/***
	 * Initialise the appropriate filenames for the prediction files, whether readIn or written to
	 * @throws Exception 
	 */
	public void init(String trainfilename, String costfilename, String predictionDirectory, String range, String[] testfiles) throws Exception{
		training_data_prediction_file = predictionDirectory + trainfilename.substring(trainfilename.lastIndexOf("_data_")+5,trainfilename.lastIndexOf("."))+".text";
		System.out.println(training_data_prediction_file);
		int o = 0;
		System.out.println("test files:");
		for (String testfilename : testfiles){
			System.out.println("&&&"+testfilename+"&&&");
			o++;
			test_data_prediction_files.add(predictionDirectory + testfilename.substring(testfilename.lastIndexOf("_data_")+5,testfilename.lastIndexOf("."))+".text");
			System.out.println("corresponding prediction File");
			System.out.println(predictionDirectory + testfilename.substring(testfilename.lastIndexOf("_data_")+5,testfilename.lastIndexOf("."))+".text");
		}
		//System.out.println(test_data_prediction_files);
		
		//cost_matrix_filename = costfilename; //now being done in generateCostFile
		setup_data(trainfilename, testfiles, range);
		generateCostFile(costfilename);
	}
	
	
	
	
	/***
	 * Method to convert the test data into same format as the training data
	 * @param trainfilename
	 * @param testfilenames 
	 * @param range A list of the attributes used in the classification as a string like: first-3,5,6-last
	 * @throws Exception
	 */
	public void setup_data(String trainfilename, String[] testfilenames, String range) throws Exception{
		
		System.out.println("train file name:");
		System.out.println(trainfilename);
		//System.out.println(testfilenames);
		CSVLoader loader = new CSVLoader();

		loader.setSource(new File(trainfilename));
		train_data = loader.getDataSet();
		
		//removing features specified in range
	    if (!range.equals("None")){
	    	Remove remove = new Remove();
	    	remove.setAttributeIndices(range);
	    	remove.setInvertSelection(false); //if true the selected columns kept, false, they are removed
	    	remove.setInputFormat(train_data);
	    	train_data = Filter.useFilter(train_data, remove);
	    	//System.out.println(instNew);
	    }
	    
		for (int x =0; x<train_data.numAttributes(); x++){
			System.out.println(train_data.attribute(x).name());

		}
		classes = new ArrayList<String>();	

		train_data.setClassIndex(train_data.numAttributes()-1);
		int ind = 0;
		//gets classes in order
		while (classes.size()< train_data.numClasses()) {
			Instance inst = train_data.instance(ind);
			String myclass = inst.stringValue(inst.numAttributes()-1);
			if (!classes.contains(myclass)){
				classes.add(myclass);
			}
			ind++;
		}
		System.out.println("train classes");
		System.out.println(classes);
		for (String testfilename: testfilenames){
			System.out.println(testfilename);
			test_data.add(testDataFromTrain(train_data, testfilename, range));
		}
		
		
	}
	
	/**
	 * Returns filtered string array, i.e. all those NOT in @param range
	 * @param atts
	 * @param range
	 * @return
	 */
	public String[] filterAttributes(String[] atts, String range){
		
		List<Integer> intranges = new ArrayList<Integer>();
		if(!range.equals("None")){
			String[] ranges = range.split(",");
			for (String myrange : ranges){
				intranges.add(Integer.parseInt(myrange)-1); //-1 as not 0-indexed in weka
			}
		}
		String[] newatts = new String[atts.length-intranges.size()];
		int count = 0;
		for (int i=0; i<atts.length; i++){
			if (intranges.contains(i))
			{continue;}
			newatts[count] = atts[i]; 
			count+=1;
		}
		
		return newatts;
	}
	
	public Instances testDataFromTrain(Instances train, String testfilename, String range) throws Exception{
		//declare the feature vector from test set
		FastVector fvWekaAttributes = new FastVector(train.numAttributes());
		for (int o = 0; o < train.numAttributes(); o++) {
			fvWekaAttributes.addElement(train.attribute(o));
		}
		Instances testdata = new Instances("rel", fvWekaAttributes, 1000000);
		testdata.setClassIndex(train.numAttributes() - 1);
		BufferedReader inputStream = null;
		try {
			inputStream = new BufferedReader(new FileReader(testfilename));
			String line = inputStream.readLine(); // skip first one
			while ((line = inputStream.readLine()) != null) {
				String[] attributeVals = filterAttributes(line.split(","),range);
				Instance iExample = new Instance(train.numAttributes());
				//System.out.println("Instance");
				for (int o = 0; o < train.numAttributes(); o++) {
					Attribute myAtt = train.attribute(o);
					//System.out.println(myAtt.name());
					//System.out.println("***"+attributeVals[o]+"***");
					if (myAtt.isNominal()) {
						//try{
							iExample.setValue(train.attribute(o), attributeVals[o]);
						//}
						//finally{
						//	iExample.setValue(train.attribute(o), "NULL");//default for missing nominals
						//}

					} else if (myAtt.isNumeric()) {
						iExample.setValue(train.attribute(o), Float.parseFloat(attributeVals[o]));
					}
				}
				iExample.setDataset(train); // do we need to do this?
				testdata.add(iExample);
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
		}

		int ind = 0;
		ArrayList<String> testclasses = new ArrayList<String>();
		while (testclasses.size() < train.numClasses()) {
			Instance inst = testdata.instance(ind);
			if (inst == null){
				System.out.println("WARNING HAVEN'T GOT ALL CLASSES IN TRAINING SET!!");
				break;
			}
			String myclass = null;
			try {myclass = inst.stringValue(inst.numAttributes() - 1);}
			catch (Exception e){
				System.out.println(inst.numAttributes());
				System.out.println(inst.toString());
				System.exit(0);
			}
			if (!testclasses.contains(myclass)) {
				testclasses.add(myclass);
			}
			ind++;
		}
		System.out.println("test classes:");
		System.out.println(testclasses);
				
		return testdata;
	
	}
	
	/***
	 * Method for training, may be different for each component, but default is to use MetaCost
	 * @throws Exception
	 */
	public void train() throws Exception{
		
		//classifier = new CostSensitiveClassifier();
		
		classifier = new MetaCost();
		RandomForest rf = new RandomForest();
		rf.setNumTrees(20);
		System.out.println(cost_matrix_filename);
		CostMatrix matrix = new CostMatrix(
                 new BufferedReader(new FileReader(cost_matrix_filename)));
		System.out.println(matrix);
		
		((MetaCost)classifier).setClassifier(rf);
		((MetaCost)classifier).setCostMatrix(matrix);
		((MetaCost)classifier).setNumIterations(10); //these good acc. Domingos 99, 10 or 20
		((MetaCost)classifier).setBagSizePercent(25); // 20 /10 20 much better, though this is what slows it down however..
		//classifier = new RandomForest();
		//((RandomForest) classifier).setNumTrees(20);
			
		System.out.println("Begin training");
		classifier.buildClassifier(train_data);
		
		//test this on the training data and output
		StringBuffer forPredictionsPrinting = new StringBuffer();
		weka.core.Range attsToOutput = null;
		Boolean outputDistribution = new Boolean(true);
		Evaluation eval = new Evaluation(train_data);
		eval.evaluateModel(classifier, train_data, forPredictionsPrinting, attsToOutput, outputDistribution);
		// write it to file
		System.out.println(training_data_prediction_file);
		BufferedWriter out = new BufferedWriter(new FileWriter(training_data_prediction_file));
		out.write("inst#\n");
		out.write(forPredictionsPrinting.toString());
		out.flush();
		out.close();
		//TODO possibly print these to a results file?
		System.out.println(eval.toClassDetailsString());
		System.out.println(eval.toMatrixString());
		//Save the model
		SerializationHelper.write(training_data_prediction_file.replace(".text", ".model"), classifier);
	
	}
	
	/***
	 * Method to write to the target predictionsFile
	 * Will test all the test data, writing to the corresponding predictionfiles
	 * @throws Exception
	 */
	public void test() throws Exception{
		// testing
		System.out.println("Begin testing and evaluation");
		for (int i=0; i<test_data.size(); i++){
			Instances data = test_data.get(i);
			String predictionsFile = test_data_prediction_files.get(i);
			Evaluation eval = new Evaluation(train_data);
			// eval.crossValidateModel(rf, train, 5, new Random(1));
			// System.out.println(eval.toSummaryString("\nResults\n=====\n",false));
			// System.out.println(eval.toClassDetailsString());
			// System.out.println(eval.toMatrixString());
			StringBuffer forPredictionsPrinting = new StringBuffer();
			weka.core.Range attsToOutput = null;
			Boolean outputDistribution = new Boolean(true);
			eval.evaluateModel(classifier, data, forPredictionsPrinting, attsToOutput, outputDistribution);
			// write it to file
			System.out.println("testing");
			System.out.println(predictionsFile);
			BufferedWriter out = new BufferedWriter(new FileWriter(predictionsFile));
			out.write("inst#\n");
			out.write(forPredictionsPrinting.toString());
			out.flush();
			out.close();
			// possibly print these to a results file?
			System.out.println(eval.toClassDetailsString());
			System.out.println(eval.toMatrixString());
		}
	}
	
	/***
	 * Method for loading WEKA model file
	 * @throws Exception
	 */
	public void loadModel(String modelPath) throws Exception{
		
		//classifier = new CostSensitiveClassifier();
		
		classifier = new MetaCost();
		RandomForest rf = new RandomForest();
		rf.setNumTrees(20);
		System.out.println(cost_matrix_filename);
		CostMatrix matrix = new CostMatrix(
                 new BufferedReader(new FileReader(cost_matrix_filename)));
		System.out.println(matrix);
		
		((MetaCost)classifier).setClassifier(rf);
		((MetaCost)classifier).setCostMatrix(matrix);
		((MetaCost)classifier).setNumIterations(10); //these good acc. Domingos 99, 10 or 20
		((MetaCost)classifier).setBagSizePercent(25); // 20 /10 20 much better, though this is what slows it down however..
		//classifier = new RandomForest();
		//((RandomForest) classifier).setNumTrees(20);
			
		System.out.println("Load model " + modelPath);
		classifier = (MetaCost) SerializationHelper.read(new FileInputStream(modelPath));
		
		boolean test_on_training_data = false;
		if (test_on_training_data){
			//test this on the training data and output- need to avoid this in future
			StringBuffer forPredictionsPrinting = new StringBuffer();
			weka.core.Range attsToOutput = null;
			Boolean outputDistribution = new Boolean(true);
			Evaluation eval = new Evaluation(train_data);
			eval.evaluateModel(classifier, train_data, forPredictionsPrinting, attsToOutput, outputDistribution);
			// write it to file
			System.out.println(training_data_prediction_file);
			BufferedWriter out = new BufferedWriter(new FileWriter(training_data_prediction_file));
			out.write("inst#\n");
			out.write(forPredictionsPrinting.toString());
			out.flush();
			out.close();
			//TODO possibly print these to a results file?
			System.out.println(eval.toClassDetailsString());
			System.out.println(eval.toMatrixString());
		}
	
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
