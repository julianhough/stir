import csv

import os
import sys
THIS_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(THIS_DIR + "/../")
from evaluation.eval_utils import sort_into_dialogue_speakers
from evaluation.eval_utils import get_tag_data_from_corpus_file


def load_data_from_stir_file(fp, representation="disf1", limit=8,
                             add_fake_timings=True):
    """Loads from file into five lists of lists of strings of equal length:
    one for utterance iDs (IDs))
    one for word timings of the targets (start,stop)
    one for words (seq),
    one for pos (pos_seq)
    one for tags (targets).

    NB this does not convert them into one-hot arrays, just outputs lists of
    string tags."""
    print "loading from", fp
    f = open(fp)
    print "loading data", f.name
    count_seq = 0
    IDs = []
    seq = []
    pos_seq = []
    targets = []
    timings = []
    currentTimings = []
    current_dialogue = ""

    reader = csv.reader(f, delimiter='\t')
    counter = 0
    utt_reference = ""
    currentWords = []
    currentPOS = []
    currentTags = []
    current_fake_time = 0

    for ref, timing, word, postag, disftag in reader:
        # mixture of POS and Words
        # print ref, timing, word, postag, disftag
        counter += 1
        if not ref == "":
            if count_seq > 0:  # do not reset the first time
                seq.append(tuple(currentWords))
                pos_seq.append(tuple(currentPOS))
                targets.append(tuple(currentTags))
                IDs.append(utt_reference)
                timings.append(tuple(currentTimings))
                # reset the words
                currentWords = []
                currentPOS = []
                currentTags = []
                currentTimings = []
            # set the utterance reference
            count_seq += 1
            utt_reference = ref
            if not utt_reference.split(":")[0] == current_dialogue:
                current_dialogue = utt_reference.split(":")[0]
                current_fake_time = 0
                # TODO fake for now- reset the current beginning of word time
        currentWords.append((word, float(current_fake_time),
                             float(current_fake_time) + 1))
        currentPOS.append(postag)
        currentTags.append(disftag)
        currentTimings.append(float(timing))
        current_fake_time += 1
    # flush
    if not currentWords == []:
        seq.append(tuple(currentWords))
        pos_seq.append(tuple(currentPOS))
        targets.append(tuple(currentTags))
        IDs.append(utt_reference)
        timings.append(tuple(currentTimings))

    assert len(seq) == len(targets) == len(pos_seq), ("{0} {1} {2}".format(
                                                            len(seq),
                                                            len(targets),
                                                            len(pos_seq)
                                                                    )
                                                      )
    print "loaded " + str(len(seq)) + " sequences"
    f.close()
    return (IDs, timings, seq, pos_seq, targets)


def create_final_evaluation_file_from_stir_output(stir_tag_file,
                                                  eval_file_path,
                                                  gold_file_path=None,
                                                  timings=False,
                                                  eval_word=True,
                                                  eval_interval=False):
    """Write an evaluation friendly file from STIR tag outputs.

    stir_tag_file :: the path to the results outputted by STIR in standard
    format
    eval_file_path :: the path to the file which will be passed into

    Converts from:

    4646:B:2    1    but    CC    <f/>
    2    i    PRP    <rms id="2"/>
    3    my    PRP$    <rps id="2"/>
    4    grandparents    NNS    <f/>
    5    were    VBD    <f/>
    6    looking    VBG    <f/>
    7    into    IN    <f/>
    8    it    PRP    <f/>
    9    before    RB    <f/>
    4646:B:3    1    so    RB    <f/>
    2    i    PRP    <f/>
    3    know    VBP    <f/>
    4    what    WP    <f/>
    5    theyve    PRPVBP    <f/>
    6    said    VBN    <f/>


    to the evaluation algorithm final format, inheriting timing from the gold
    if @gold_file_path is supplied:


    File: 4646-B
    0.0    6.26    no    <f/><tt>@<f/><tt>
    6.26    6.41    but    <f/><tc>@<f/><tc>
    6.41    6.65    i    <rms id="3"/><cc>@<rms id="3"/><cc>
    6.65    6.78    my    <rps id="3"/><cc>@<rps id="3"/><rpnsub id="3"/><cc>
    6.78    7.42    grandparents    <rpnsub id="3"/><cc>@<f/><cc>
    7.42    7.52    were    <f/><cc>@<f/><cc>
    7.52    8.01    looking    <f/><cc>@<f/><cc>
    8.01    8.56    into    <f/><cc>@<f/><cc>
    8.56    8.64    it    <f/><cc>@<f/><ct>
    8.64    9.35    before    <f/><ct>@<f/><tt>
    9.35    10.68    so    <f/><tc>@<f/><tc>
    10.68    10.88    i    <f/><cc>@<f/><cc>
    10.88    11.05    know    <f/><cc>@<f/><cc>
    11.05    11.25    what    <f/><cc>@<f/><cc>
    11.25    11.5    theyve    <f/><cc>@<f/><cc>
    """
    if gold_file_path:
        IDs, timings, words, pos_tags, labels = get_tag_data_from_corpus_file(
                                                                gold_file_path)
        gold_speakers_dict = {}  # map from the file name to the data
        for dialogue, a, b, c, d in zip(IDs, timings, words, pos_tags, labels):
            gold_speakers_dict[dialogue] = (a, b, c, d)

    dialogue_speakers = []
    IDs, mappings, utts, pos_tags, labels = load_data_from_stir_file(
        stir_tag_file, add_fake_timings=True)
    # create fake timings for utts
    dialogue_speakers.extend(sort_into_dialogue_speakers(IDs,
                                                         mappings,
                                                         utts,
                                                         pos_tags,
                                                         labels))
    prediction_speakers_dict = {}
    for tup in dialogue_speakers:
        prediction_speakers_dict[tup[0]] = (tup[1], tup[2], tup[3], tup[4])

    # write to file
    eval_file = open(eval_file_path, "w")
    for s in sorted(prediction_speakers_dict.keys()):
        # print s
        if gold_file_path and gold_speakers_dict.get(s) == None:
            print s, "not in gold"
            continue

        eval_file.write("Speaker: " + s + "\n")
        hyp = prediction_speakers_dict[s]
        if gold_file_path:
            gold = gold_speakers_dict[s]

        hypwords = hyp[1]
        if gold_file_path:
            goldwords = gold[1]
        else:
            goldwords = hypwords  # just a dummy
        if eval_word:  # assumes number of words == no of intervals
            final_words = []
            if gold_file_path:
                for g_word, h_word in zip(goldwords, hypwords):
                    word = g_word
                    if not h_word[0] == g_word:
                        word += "@" + h_word[0]
                    final_words.append((h_word[1], h_word[2], word))
            else:
                # print hypwords
                final_words = [(h_word[1], h_word[2], h_word[0])
                               for h_word in hypwords]
            if gold_file_path:
                for word, pos_tag, g_tag, h_tag in zip(final_words,
                                                       gold[3],
                                                       hyp[-2],
                                                       hyp[-1]):
                    eval_file.write("\t".join([str(word[0]),
                                               str(word[1]),
                                               str(word[2]),
                                               pos_tag,
                                               "{0}@{1}"
                                               .format(g_tag, h_tag)]) + "\n")
            else:
                for word, pos_tag, h_tag in zip(final_words,
                                                hyp[-2],
                                                hyp[-1]):
                    eval_file.write("\t".join([str(word[0]),
                                               str(word[1]),
                                               str(word[2]),
                                               pos_tag,
                                               "{0}@{1}"
                                               .format(h_tag, h_tag)]) + "\n")
                eval_file.write("\n")
        if eval_interval:
            raise NotImplementedError
    eval_file.close()

if __name__ == '__main__':
    tag_file = "/home/julian/git/stir/python/experiments/results/" + \
        "stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/" + \
        "swbd_disf_heldout_partial_data_TAGS.text"
    
    all_files = [
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/swbd_disf_heldout_partial_data_output_final.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/PCC_test_partial_data_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/PCC_test_partial_data_output_final.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/PCC_test_partial_data_easy_read_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/BNC-CH_partial_data_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/BNC-CH_partial_data_output_final.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/BNC-CH_partial_data_easy_read_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/swbd_disf_test_partial_data_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/swbd_disf_test_partial_data_output_final.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/swbd_disf_test_partial_data_easy_read_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/swbd_disf_heldout_partial_data_TAGS.text",
        "/home/julian/git/stir/python/experiments/results/stack_d3_lm3_partial_swbd_disf_train_1_partial_data_3_9_4_2/swbd_disf_heldout_partial_data_easy_read_TAGS.text"
        ]

    for tag_file in all_files:
        print tag_file
        if "_TAGS" not in tag_file: continue
        if "easy_read" in tag_file: continue
        eval_file_path = tag_file.replace("_TAGS.", "_output_final.")
        create_final_evaluation_file_from_stir_output(
                                                  tag_file,
                                                  eval_file_path,
                                                  gold_file_path=None,
                                                  timings=False,
                                                  eval_word=True,
                                                  eval_interval=False)