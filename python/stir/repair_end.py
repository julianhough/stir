from collections import defaultdict
import re
import sys
import os
sys.path.append(os.path.join(os.getcwd(),".."))

from stir_classifier import stir_classifier
from language_model.ngram_language_model import NgramGraph

class repair_end(stir_classifier):
    """Takes (n) number of rm hypotheses per rp hypothesis from reparandum_onset classifier and for each position in the string from
    rp0 to rp0+6 (clean words), then pops the repair hypothesis.
    It can adjust the reparandumStart positions based on what it sees next.
    This can also CLASSIFY the repair, we know all the syntax, now get the semantics.
    """
    def __init__(self,lm,pos_model=None,evaluation=True,corpus=None,corpus_name=None,editfilelines=None,repairfilelines=None,reparandumfilelines=None,
                 dataFile=None,referenceFile=None,
                 tagFile=None, #in case we want an intermediate results outputted, probably not needed
                 ranges=None,fileIndex=None,repairIndex=None,reparandumIndex=None,
                 partial_words=False,editGold=False,repairGold=False,reparandumGold=False,
                 previousExp=None,stackDepth=1):
        
        #evaluation measures for all edit terms, and we can break this down into <e> and <i> and inc
        if previousExp == None: self.eval_init(lm,["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"])
        else:
            print "inheriting values"
            self.inherit_eval_values(lm,previousExp)
        
        #relaxes rm and rms, only looking at rps, not rp or rpn
        
        editCount = fileIndex
        repairCount = repairIndex
        reparandumCount = reparandumIndex
        
        
        delta = lm.order-1
        posdelta = pos_model.order-1
        p = posdelta - delta
        #window = 3 #how far back we can look for repeats
        FPindex = 0 #creates index for FPs so only the correct ones are removed
        multipleRPS = []
        
        
        end = False
        #reparandumStack = [] #will be pushed and popped
        
        def repairEndData(lm,repair,words,predictedTags,nonEditPrefixWords,nonEditPrefixPOS):
            """returns the data needed for the repair end for a given lm and tag sequence and the
            rm and rp hypotheses so far"""
            delta = lm.order-1
            posdelta = pos_model.order-1
            p = posdelta - delta
            #repair object structure
            #(repairID,RPSpos,RMSpos,currentPOS,rp0confidence,rm0confidence,categoryAtcurrentpos)#7-tuple
            #print "%%%%%%%%%%%%%%%%%%%%%%"
            #print repair
            j = repair[1] #rms absolute position
            i = repair[2] #rps absolute position
            n = repair[3] #current position of word pointer
            #print words
            #print predictedTags
            #now need to discount edit words
            #print nonEditPrefixWords.word_graph
            #print nonEditPrefixPOS.word_graph
            embedded = None
            if len(predictedTags)-1 > i: #i.e. beyond the current RPS
                if "<rps" in predictedTags[-1]:
                    embedded = True
                else: embedded = False
            #adjust these for edits
            for tagCount in range(delta,len(predictedTags)):    
                if "<i><e>" in predictedTags[tagCount]:
                    if tagCount <= repair[1]:
                        j = j -1 #discount the edit words
                    if tagCount <= repair[2]:
                        i = i-1 #discount the edit words
                    n = n-1 # gets us up to the current nonEditPrefixWords marker, could be different in POS marker (n+p)
            #print "%%%"
            #print j
            #print i
            #print n
            
            
            reparandumLength = i - j
            repairLength = n - i + 1 #i.e. potential repair length; the number of tokens from the RPS to the current prefix position + 1
            if repairLength > 6: return None #do this here!
            
            ######################got this far, carry on
            RP0confidence = float(repair[4])
            RM0confidence = float(repair[5])
            
            #embeddings I [like + like ] + like
            #overall scores
            #local boost of the removal up to delta beyond...#note- to make it local, only go delta back from rm0 for original bits and delta onward from the new hyps
            originalSequence = nonEditPrefixWords.subgraph([(j-delta),min((i+lm.order),len(nonEditPrefixWords.word_graph))],lm) #can't really do an end cap?? or can we?
            #print "original = " + str(originalSequence.word_graph)
            #originalSequenceNgram = originalSequence.word_graph[-lm.order:] #the ngram at the edge of the observable original change, for 2-grams will be the same as before, 3-grams we see more
            originalLocalWordWML = originalSequence.logprob_weighted_by_inverse_unigram_logprob()
            originalLocalWordLogProb = originalSequence.logprob_weighted_by_sequence_length()
            #originalWordEntropy = lm.entropy_continuation_very_fast(repairOnsetNgram[-(delta):],lm.order)
    
        
            #overall boost for utterance
            originalPOSsequence = nonEditPrefixPOS.subgraph([((j+p)-posdelta),min(((i+p)+pos_model.order),len(nonEditPrefixPOS.word_graph))],pos_model)
            #print "original POS = " + str(originalPOSsequence.word_graph)
            #originalPOSsequenceNgram = originalPOSsequence.word_graph[-pos_model.order:]
            originalLocalPOSWML = originalPOSsequence.logprob_weighted_by_inverse_unigram_logprob()
            originalLocalPOSLogProb = originalPOSsequence.logprob_weighted_by_sequence_length()
            #originalPOSEntropy = pos_model.entropy_continuation_very_fast(repairOnsetPOSngram[-posdelta:],pos_model.order)
            #local measures
            repairOnsetNgram = nonEditPrefixWords.word_graph[i-delta:i+1] #the uncleaned repair trigram
            reparandumEndNgram = nonEditPrefixWords.word_graph[i-lm.order:i]
            rmwordngram = nonEditPrefixWords.word_graph[j-delta:j+1] #the reparandum onset trigram
            rpwordngram = nonEditPrefixWords.word_graph[j-delta:j] + [repairOnsetNgram[-1]] #cleaned repair onset (excised of reparandum)
            
            wordCleanTest = nonEditPrefixWords.subgraph([j-delta,j],lm) #only going from delta words before rm0, not all the way back
            #print wordCleanTest.word_graph
            wordCleanTestLocal = nonEditPrefixWords.subgraph([j-delta,j],lm)
            #print wordCleanTestLocal.word_graph
            wordCleanRepairEndTestLocal = nonEditPrefixWords.subgraph([j-delta,i],lm)
            #print wordCleanRepairEndTestLocal.word_graph
            #for the Local Test we are limiting to just the delta after the repair onset to avoid unneccessary re-comp
            wordCleanTest.extend(nonEditPrefixWords.subgraph([i,len(nonEditPrefixWords.word_graph)],lm),lm) #so it's consistent with the original test
            wordCleanTestLocal.extend(nonEditPrefixWords.subgraph([i,min((i+lm.order),len(nonEditPrefixWords.word_graph))],lm),lm)
            wordCleanRepairEndTestLocal.extend(nonEditPrefixWords.subgraph([len(nonEditPrefixWords.word_graph)-1,len(nonEditPrefixWords.word_graph)],lm),lm)
            
            currentNgram = wordCleanTest.word_graph[-lm.order:] #i.e. potential rpn; the latest ngram cleaned of reparandum e.g. I love, like john = "I like john" for a trigram 
            
            #print "current ngram " + str(currentNgram)
            #print "reparandum end ngram " + str(reparandumEndNgram)
            #similarity logical features, just looking at boundaries, could have another one which is whether ALL are the same
            RM0RP0 = str(rmwordngram[-1] == rpwordngram[-1]).upper() #rm and rp onsets the same
            RMNRPN = str(reparandumEndNgram[-1] == currentNgram[-1]).upper() #ends the same, these will cover a lot of cases
            wordIdent = str(nonEditPrefixWords.word_graph[j:i]==nonEditPrefixWords.word_graph[i:n+1]).upper()
            
            
            #the below should help detect c0's vs repairs and classify i.e. the potential similarity
            #i.e. if we consider this the c0 word, where non incrementality comes in, hopefully sufficient
            #will certainly give substitutability relations
            cont1TestWML = wordCleanRepairEndTestLocal.logprob_weighted_by_inverse_unigram_logprob() #see if "I [love + like] john" where "I love john" is grammatical; should help classification
            
            #scores for cleaned data
            wordWML = wordCleanTestLocal.logprob_weighted_by_inverse_unigram_logprob()
            wordlogprob = wordCleanTestLocal.logprob_weighted_by_sequence_length()
            reparandumRepairDiff = wordWML - cont1TestWML #should not be high for more substitutive rp ends; may also not be high for rp's
            
            #not clear how these help us, first one possibly with classification- DELs have high entropy generally?
            wordRM0Entropy = lm.entropy_continuation_very_fast(rmwordngram[-delta:],lm.order)#uncleaned bigram/unigram around the rm0 boundary- should be high
            #wordEntropy = lm.entropy_continuation_very_fast(rpwordngram[-delta:],lm.order) #i.e. for cleaned, will both be the same in bigrams..
            #wordLocalEntropyReduce = wordEntropy - originalWordEntropy
            
            #gives us further on than 1 word, up to delta more
            wordInfoGainRaw = lm.sum_information_gain_very_fast(nonEditPrefixWords.word_graph[i-delta:min(len(nonEditPrefixWords.word_graph),i+delta)])
            wordInfoGainCleaned = lm.sum_information_gain_very_fast(rmwordngram[:-1]+nonEditPrefixWords.word_graph[i:min(len(nonEditPrefixWords.word_graph),i+delta)])
            wordInfoGainReduce = wordInfoGainRaw - wordInfoGainCleaned
            
            wordLocalWMLBoost = wordWML - originalLocalWordWML
            wordLocalProbBoost = wordlogprob - originalLocalWordLogProb
            
        
            repairOnsetPOSNgram = nonEditPrefixPOS.word_graph[(i+p)-posdelta:(i+p)+1] #the uncleaned repair trigram
            reparandumEndPOSNgram = nonEditPrefixPOS.word_graph[(i+p)-pos_model.order:(i+p)]
            rm0POSngram = nonEditPrefixPOS.word_graph[(j+p)-posdelta:(j+p)+1] #the reparandum onset trigram
            rp0POSngram = nonEditPrefixPOS.word_graph[(j+p)-posdelta:(j+p)] + [repairOnsetPOSNgram[-1]] #cleaned repair onset (no reparandum)
            #the currentBoundary ngram cleaned of this reparandum?
            POSCleanTest = nonEditPrefixPOS.subgraph([(j+p)-posdelta,(j+p)],pos_model) #only going from delta words before rm0, not all the way back
            POSCleanTestLocal = nonEditPrefixPOS.subgraph([(j+p)-posdelta,(j+p)],pos_model)
            POSCleanRepairEndTestLocal = nonEditPrefixPOS.subgraph([(j+p)-posdelta,(i+p)],pos_model)
            #for the Local Test we are limiting to just the delta after the repair onset to avoid unneccessary re-comp
            POSCleanTest.extend(nonEditPrefixPOS.subgraph([(i+p),len(nonEditPrefixPOS.word_graph)],pos_model),pos_model) #so it's consistent with the original test
            POSCleanTestLocal.extend(nonEditPrefixPOS.subgraph([(i+p),min(((i+p)+pos_model.order),len(nonEditPrefixPOS.word_graph))],pos_model),pos_model)
            POSCleanRepairEndTestLocal.extend(nonEditPrefixPOS.subgraph([len(nonEditPrefixPOS.word_graph)-1,len(nonEditPrefixPOS.word_graph)],pos_model),pos_model)
            
            currentPOSNgram = POSCleanTest.word_graph[-pos_model.order:] #i.e. potential rpn; the latest ngram cleaned of reparandum e.g. I love, like john = I like john for a trigram 
            #PARRALLELISM
            KLposRM0RP0 = pos_model.KL_divergence_continuation_fast(rm0POSngram[-posdelta:],rp0POSngram[-posdelta:])
            KLposRMNRPN = pos_model.KL_divergence_continuation_fast(reparandumEndPOSNgram[-posdelta:],currentPOSNgram[-posdelta:])
            
            POSWML = POSCleanTestLocal.logprob_weighted_by_inverse_unigram_logprob()
            POSlogprob = POSCleanTestLocal.logprob_weighted_by_sequence_length()
            cont1TestPOSWML = POSCleanRepairEndTestLocal.logprob_weighted_by_inverse_unigram_logprob()
            POSreparandumRepairDiff = POSWML - cont1TestPOSWML #should not be high for more substitutive ones
            POSLocalWMLBoost = POSWML - originalLocalPOSWML
            POSLocalProbBoost = POSlogprob - originalLocalPOSLogProb
            #booleans
            RM0RP0POS = str(rm0POSngram[-1] == rp0POSngram[-1]).upper() #onsets the same #booleans (could in theory extend this to "are all words the same" honnibal johnson etc.)
            RMNRPNPOS = str(reparandumEndPOSNgram[-1] == currentPOSNgram[-1]).upper() #ends the same, these will cover a lot of cases
            POSident = str(nonEditPrefixPOS.word_graph[(j+p):(i+p)]==nonEditPrefixPOS.word_graph[(i+p):(n+p)+1]).upper()
            
            #not clear how these help us, first one possibly with classification- DELs have high entropy generally?
            POSRM0Entropy = pos_model.entropy_continuation_very_fast(rm0POSngram[-posdelta:],pos_model.order)#uncleaned bigram/unigram around the rm0 boundary- should be high
            #wordEntropy = lm.entropy_continuation_very_fast(rpwordngram[-delta:],lm.order) #i.e. for cleaned, will both be the same in bigrams..
            #wordLocalEntropyReduce = wordEntropy - originalWordEntropy
            
            POSInfoGainRaw = pos_model.sum_information_gain_very_fast(nonEditPrefixPOS.word_graph[(i+p)-posdelta:min(len(nonEditPrefixPOS.word_graph),((i+p)+posdelta))])
            POSInfoGainCleaned = pos_model.sum_information_gain_very_fast(rm0POSngram[:-1]+nonEditPrefixPOS.word_graph[i:min(len(nonEditPrefixPOS.word_graph),((i+p)+posdelta))])
            POSInfoGainReduce = POSInfoGainRaw - POSInfoGainCleaned
            
            
            data = [RP0confidence,RM0confidence,reparandumLength,repairLength,embedded] #basic features
            data.extend([KLposRM0RP0,KLposRMNRPN,RM0RP0,wordIdent,RMNRPN,RM0RP0POS,RMNRPNPOS,POSident]) #similarity features (last four binary)
            data.extend([reparandumRepairDiff,POSreparandumRepairDiff]) #subsitutability evaluation
            data.extend([wordLocalWMLBoost,wordLocalProbBoost,POSLocalWMLBoost,POSLocalProbBoost]) #boost from reparandum removal, entropy and prob
            data.extend([wordInfoGainReduce,wordRM0Entropy,POSInfoGainReduce,POSRM0Entropy])
            return data
            
        a,b,c,d = corpus
        for reference,words,pos,tags in zip(a,b,c,d):    
            if ranges and not reference.split(":")[0] in ranges:
                continue
        
            nonEditPrefixWords = NgramGraph(lm)
            nonEditPrefixPOS = NgramGraph(pos_model) ##MUST BE POS MODEL! 
            words = delta * ["<s>"] + list(words) #all the non-edit words so far these can be removed with "TWO" word hypothesis, though not added to
            tags = delta * [""] + self.convert_to_old_stir_format(list(tags))
            pos = posdelta * ["<s>"] + list(pos)
            repairStack = defaultdict(list) #a stack of all the hypotheses being classified, indexed by the rps position
            #evaluation sequences
            predictedTags = delta * [""] #get these true negs for free! #we can evaluate tags so far, and ammend this, provides a way of removing entire repair annotations when a hypothesis is revoked
            #goldTags = delta * [""]  #this does not change incrementally, can use this or words
            
            #word loop through utterance/fileline
            target = editCount + len(words) - delta
            i = delta #word index
            while editCount < target:
                editTag = ""
                goldTag = ""
                evalTag = "" #more fine grained, i..e <i> vs. <e>
                line = editfilelines[editCount] #data line from edit predictions
                data = line.split()
                #[1] is gold
                if ":ONE" in data[1] or ":TWO" in data[1]:
                    assert "<e" in tags[i] or "<i" in tags[i], tags #check this corresponds!
                    if "<i" in tags[i]:
                        evalTag+="<i>"
                    elif "<e" in tags[i]:
                        evalTag+="<e>"
                    goldTag = "<i><e>" #one for now, can only get recall for interregna
                    #goldTag = evalTag
                #goldTags.append(goldTag)
                #evalTags.append(evalTag)
                #[2] is predicted, we can change the [2]s below to [1]s to give perfect results in testing
                if editGold==True:
                    editTag = goldTag  
                elif  (":ONE" in data[2]) or (":TWO" in data[2]):
                    editTag+="<i><e>" #in prediction we don't discriminate
                    
                    if (":TWO" in data[2]):
                        if not "<i><e>" in predictedTags[-1]  and i>delta: #NOT detected incrementally, this is CORRECTING the hyp
                            
                            if "<rps" in predictedTags[-1] or not repairStack.get(i-1)==None:
                                #deletes all the RPS/RM(S)/RPs at this point
                                repairRevokelist = re.findall('<rps\:[0-9]+>', predictedTags[-1], re.S) + re.findall('<rpsdel\:[0-9]+>', predictedTags[-1], re.S)
                                repairRevokelist+= re.findall('<rps\:FP[0-9]+>', predictedTags[-1], re.S) + re.findall('<rpsdel\:FP[0-9]+>', predictedTags[-1], re.S)#nb. need an FP index too as only want to get rid of those
                                try:
                                    del repairStack[i-1]
                                except KeyError:
                                    print "failed to delete"
                                    pass
                                #repairStack = filter(lambda x: not "<rps" + x[0] in repairRevokelist and not "<rpsdel" + x[0] in repairRevokelist, repairStack) #clear the onsets
                                #clear the other elements efficiently
                                b = len(predictedTags)-1
                                while b >= delta and len(repairRevokelist)>0:
                                    remove = False
                                    for rr in range(0,len(repairRevokelist)):
                                        r = repairRevokelist[rr]
                                        ID = r[r.rfind(":"):]
                                        if ID == "": print "incomplete" + r; raw_input()
                                        if "<rms"+ID in predictedTags[b]: remove = True
                                        predictedTags[b] = predictedTags[b].replace("<rm"+ID,"").replace("<rms"+ID,"").replace("<rpn"+ID,"").replace("<rpndel"+ID,"")
                                        if remove == True: del repairRevokelist[rr]; break 
                                    if remove == False: b=b-1
                                
                                #mainly for next stage, but useful here as we can remove the relevant repairs so they're no longer worked on#
                                
                            if not "<i><e>" in predictedTags[-1]:
                                predictedTags[-1]="<i><e>"  #
                            #NB DON'T ALLOW FURTHER BACKTRACKING PAST THE FIRST WORD OF THE NONEDITWORDS
                            if len(nonEditPrefixWords.word_graph)>delta: #JUST POPS ONE OFF THE END
                                nonEditPrefixWords = nonEditPrefixWords.subgraph([0,len(nonEditPrefixWords.word_graph)-1],lm)
                                nonEditPrefixPOS = nonEditPrefixPOS.subgraph([0,len(nonEditPrefixPOS.word_graph)-1],pos_model)

                predictedTags.append(editTag) #add most recent
               
                #now get repair/reparandum/repairEnd tags
                goldRepairTag = ""
                predictedRepairTag = ""
                
                repairlist = re.findall('<rps\:[0-9]+>', tags[i], re.S) + re.findall('<rpsdel\:[0-9]+>', tags[i], re.S) #could be embedded deletes, CLEARING THESE FOR NOW!
                #note- removing the shortest embedded ones
                if len(repairlist)>1:
                    b = i
                    while b >= delta and len(repairlist)>1:
                        remove = False
                        for rr in range(0,len(repairlist)):
                            r = repairlist[rr]
                            ID = r[r.rfind(":"):]
                            if ID == "": print "incomplete" + r; raw_input()
                            if "<rms"+ID in tags[b]: remove = True    
                            if remove == True: del repairlist[rr]; multipleRPS.append(list(words)); break 
                        if remove == False: b=b-1
                        else: break
                    assert(len(repairlist)<=1)
                for r in repairlist: #could reduce problem here
                    #if not "<i><e>" in goldTags[i]: goldTags[i]+=r; goldRepairTag = "<rps" #we don't consider these so won't hinder final score
                    #goldTags[i]+=r; 
                    goldRepairTag = "<rps"
                #repairlist = []
                
                #reparandumlist = re.findall('<rms?\:[0-9]+>', tags[i], re.S)
                #for r in reparandumlist:
                #    if not "<i><e>" in goldTags[i]: goldTags[i]+=r #we don't consider these so won't hinder..
                reparandumlist = []
                
                if (not editTag == ""): #this should link to the repair file- this will not get embedded edit terms within reparanda yet
                    #these are not even considered (unless we incorporate the 'rejector' module in phase 2 :(
                    #self.delayedAccuracy(lm,predictedTags,goldTags,6,["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"])
                    editCount+=1; i+=1; continue #they will get incremented below otherwise
                
                #got this far we have repairable
                #also, gets here we've got a non-edit word, add to prefixes
                nonEditPrefixWords.append(words[i],lm) #auto calculates probs
                nonEditPrefixPOS.append(pos[(i+p)],pos_model)
                if len(nonEditPrefixWords.word_graph)<=lm.order or i < lm.order: #i.e. only up to <s> <s> first word, repairfile won't have incremented #i.e. only up to <s> <s> first word, repairfile won't have incremented
                    #self.delayedAccuracy(lm,predictedTags,goldTags,6,["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"])
                    i+=1; editCount+=1; continue #only allow repairables..#now check the repairStack and check up to length 8, i.e. eight words beyond this point
                
                #got this far we have a repairable word, same as above method, can increment repairCount
                repairline = repairfilelines[repairCount].split()
                #if we predict a repair onset:
                if "rps" in repairline[2] or (repairGold==True and "rps" in repairline[1]): #could be a del
                    rp0confs = repairline[-1].split(",")
                    RP0confidence = float(filter(lambda x: "*" in x,rp0confs)[0].replace("*",""))
                    #RP0confidence = repairline[-1].split(",")[1].replace("*","") #gives the confidence of the repair prediction
                    
                    #predictedRepairTag = "<rps"
                    for r in repairlist: #can add the (ONLY!) one in the repair list
                        if "rpsdel" in repairline[2]:
                            if "del" in r: predictedRepairTag+=r
                            else: predictedRepairTag+="<rpsdel"+r[4:] #false pos for dels
                        elif "rpsdel" in r: predictedRepairTag+="<rps"+r[7:]  #false neg for dels
                        else: predictedRepairTag+=r #just a normal non del
                    if predictedRepairTag == "":
                        if "rpsdel" in repairline[2]:
                            predictedRepairTag+="<rpsdel:FP" + str(FPindex) + ">" 
                        else: predictedRepairTag+="<rps:FP" + str(FPindex) + ">"
                        repairlist = [predictedRepairTag]
                        FPindex+=1
                        
                #checks and balances
                if "<rps" in goldRepairTag:
                    #print repairline
                    assert("rps" in repairline[1]),str(repairline) + str(words) + str(i) + str(reference) #check it's made it through, won't work on MRC
                else:
                    assert("non" in repairline[1]),str(repairline) + str(words) + str(i) + str(reference)
                   
                #goldTags[i] = goldRepairTag
                #predictedTags[i]+=predictedRepairTag
                #will do this below for best hypothesis
                
                #NOW FOR RPN detection
                #if there is no repair end found because these are longer than 8 word repairs, stipulate
                #an early rpn, not ideal but will save time
                repairOnsetNgram = []
                rmwordngram = []
                for key,stack in sorted(repairStack.items()): #iterates through in order
                    remove = False
                    for rr in range(0,len(stack)):
                        
                        rnew = list(stack[rr]) 
                        assert len(rnew) == 7, rnew
                        oldIndex = rnew[3]
                        rnew[3] = i #increment to where we are #we have 6 categories here- rp, cancel, rpn, rpndel, c0, c
                        if "cancel" in rnew[-1] or rnew[-1] == 'c':
                            pass #remains the same
                        elif "c0" in rnew[-1] or "rpndel" in rnew[-1]: #here we make the distinction between word after delete and one after other repair
                            rnew[-1] = "c" #increments to c
                        elif "rpn" in rnew[-1]: #nb stability due to ,<i><e> can this get ammended? yes
                            if "<i><e>" in predictedTags[oldIndex]: rnew[-1] = "rpn" #ensures an end
                            else: rnew[-1] = "c0" #gone past the rpn, this is the only one worth recording, else `c'
                        elif "rp" in rnew[-1]:
                            #print words
                            #print words[i]
                            #raw_input()
                            if re.search(r"<rpnsub%s" % rnew[0],tags[i]) or re.search(r"<rpnrep%s" % rnew[0],tags[i]):
                                rnew[-1] = "rpn"
                                #print "match rpn"
                            elif re.search(r"<rpndel%s" % rnew[0],tags[i]):
                                #print "rpndel"
                                rnew[-1] = "rpndel"
                            else: #check if it's in previous ones overwritten by ie's
                                test = i
                                for bcount in range(test,delta-1,-1):
                                    if "<i><e>" in predictedTags[bcount]:
                                        if re.search(r"<rpnsub%s" % r[0],tags[bcount])\
                                        or re.search(r"<rpnrep%s" % r[0],tags[bcount])\
                                        or re.search(r"<rpndel%s" % r[0],tags[bcount]):
                                            print "possible overwriting of ie types"
                                            print predictedTags
                                            print tags
                                            print words
                                            rnew[-1] = "c0" #late one
                                            #raw_input()
                                            break
                            #raw_input()
                        repairData = repairEndData(lm,tuple(rnew),words,predictedTags,nonEditPrefixWords,nonEditPrefixPOS)
                        if repairData == None: #beyond 8 words
                            remove = True; break #remove as beyond 8 at this point
                        elif repairData[3] == 6 and rnew[-1] == "rp": #still rp length at 8
                            rnew[-1] = "rpn" #just stipulate a substitution (non-delete), very rare
                        stack[rr] = tuple(rnew) #replace the old one    
                        #get this far start writing the data
                        for databit in repairData:
                            dataFile.write(str(databit)+",")
                        dataFile.write(rnew[-1]+"\n") #repair cat
                        referenceFile.write(reference+","+str(rnew[1]-delta)+","+str(rnew[2]-delta)+","+str(rnew[3]-delta) + ",")
                        referenceFile.write(str(repairOnsetNgram)[1:-1].replace("'","").replace(" ","")+",") #the words
                        referenceFile.write(str(rmwordngram)[1:-1].replace("'","").replace(" ","")+"\n")
                    repairStack[key] = stack #just to ensure
                    if remove == True: del repairStack[key] #removes all hyps
                
                if not "<rps" in predictedRepairTag: #only looking from predicted repair points
                    #self.delayedAccuracy(lm,predictedTags,goldTags,6,["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"]) #for <i> and <e> should be no different
                    i+=1; editCount+=1;
                    repairCount+=1  #therwise incremented below
                    continue
                
                #TODO improve these
                editString = ""
                if "<i><e>" in predictedTags[i-1]:
                    j = i-1
                    while "<i><e>" in predictedTags[j] and j>=(delta):
                        editString = words[j]+" "+editString
                        j=j-1
                    editString=editString[:-1]
                else: j = i-1
                if editString == "": editString = "NULL"
                else: editString = "EDIT" #try more coarse grained approach
                
                """
                #all candidates from j, skip over the non edits
                """
                reparandumLength = 1
                k = -1
                repairOnsetNgram = nonEditPrefixWords.word_graph[-lm.order:] #the uncleaned repair trigram
                #local boost of the trigram
                
                repairOnsetPOSngram = nonEditPrefixPOS.word_graph[-pos_model.order:]
                #local boost of the trigram
                    
                orig = False
                hypStack = [] #temporary stack, takes the one with the most confidence
                while j >= delta: #backwards search until we hit first word 
                    if not "<i><e>" in predictedTags[j]:#we don't allow reparanda with just edit term rms.. though do in fact include them as reparanda for evaluation when sandwiched
                        reparandumline = reparandumfilelines[reparandumCount].split()
                        reparandumCount+=1
                        reparandumTag = "NON" #original utt/or FP (not repair) for this one
                        #if orig == True: #non
                        #    #if ":NON" in reparandumline[2]: tag = "NON"
                        #    #else: orig = False; tag = "RM"
                        #    pass
                        #RM0confidence = reparandumline[-1].split(",")[1].replace("*","") #get the confidence of the reparandum start classifier
                        rm0confs = reparandumline[-1].split(",")
                        RM0confidence  = float(filter(lambda x: "*" in x,rm0confs)[0].replace("*",""))
                        
                        if ":RMS" in reparandumline[2]:
                            orig = True
                            if ":RMSDEL" in reparandumline[2]: extradel = "" # "del"
                            else: extradel = ""
                            reparandumTag = "rms" + extradel
                        elif ":RM" in reparandumline[2]:
                        #    reparandum = True
                            reparandumTag = "rm"
                        elif orig == False: #being generous here
                            reparandumTag = "rm"
                        
                        #elif ":NON" in reparandumline[2]:
                            #can get rid of false pos here
                            #reparandum = False
                            #if orig == False: #over esimating RMS's
                            #    reparandumTag = "rm"
                            #orig = True
                        
                        #if (not repairlist == []) and (not reparandumTag == "NON"): #just add all reparandum predicted tags
                            #removallist = []
                        #    for repair in repairlist:
                        #        predictedTags[j]+="<" + reparandumTag + repair[repair.rfind(":"):repair.rfind(">")]
                        #    if orig == True: repairlist = []
                            
                        """
                        if "<rm" in words[j]:
                            reparandumList = re.findall('<rms?\:[0-9]*>', words[j], re.S)
                            #print reparandumList
                            for reparandum in reparandumList:
                                reparandumID = "<rps:" + reparandum[reparandum.rfind(":")+1:reparandum.rfind(">")] + ">"
                                if reparandumID in repairlist: #only the ones found by our detector
                                    if re.match('<[rm\:[0-9]*>',reparandum):
                                        cat = "RM"
                                    elif re.match('<[rms\:[0-9]*>',reparandum): #if there is an rms, pop it off
                                        cat= "RMS"
                                        repairlist = filter(lambda x: not x == reparandumID, repairlist) #popping it off
                                        # TODO these are rare! i.e. 3/13000
                                        #if len(repairlist)>0: cat = "RMSEMBED" #could be embedded delete
                                    #elif
                                        #break #this one takes precedence, not ideal, may have to entertain idea of multiple classifications back
                        """
                        
                        #the hypothesised rm onset likelihood should be high
                        rmwordngram = nonEditPrefixWords.word_graph[k-lm.order:k] #is this in fact correct
                        #the cleaned rpwordngram (i.e. excised of reparnadum)
                        rpwordngram = nonEditPrefixWords.word_graph[k-lm.order:k-1] + [repairOnsetNgram[-1]]
                        if len(rpwordngram) < lm.order: #no more..
                            break
                        
                        if (not repairlist == []):
                            if "rms" in reparandumTag and len(repairlist)>1: #i.e. if we've got two or more
                                print("more than one RPS hypothesised for word " + str(i) + str(words[i]) + str(tags[i])) #we allow this here, but not above?
                                multipleRPS.append(list(words))
                                #cat = "RM" + extradel #i.e. we don't treat this as an RMS anymore, problem solved for below, slight hit in RMS terms
                                print words
                                print tags
                                print reference
                                raw_input("more than one! should have been filtered out")
                            #DOING TAGGING AFTER 8 words back (see below)
                                
                        
                        #todo, currently only taking the first one
                        if "rms" in reparandumTag and len(repairlist)>0: #popping it off (first one) adding to repair list
                            #will only add the rm0-rp0 hypotheses made, if rm0 not found, discarded
                            #repair Stack 'popped' in theory when rp0 hypothesised
                            #in reality you have to keep it there for the whole utterance (or 10 words on from rps0?) after initial rps hyp.
                            #otherwise risk of inconsistency
                            #add the hypothesis to the repair stack, presuming initially in progress, this will be processed first
                            #make a hypothesis, add it to the stack
                            #see if <rpn[sub|del|rep]:number> is in this word else
                            assert(len(repairlist)==1)
                            repairEndCat = "rp" #presume mid repair, for now not differentiating between rep and sub
                            #because of the large amount of rep-subs.. (rep-initial subs)
                            #will this big messy class be sufficiently different to
                            #rpndel as opposed to different to each other for first words of rp0
                            for r in repairlist: #needs to match to one in here, even if FP
                                #if "del" in r:
                                #    delextra = r[7:]; reparandumTag = reparandumTag.replace("del","")
                                #else: delextra = r[4:]
                                repairID = r[r.rfind(":"):]
                                if "FP" in r:
                                    repairEndCat = "cancel" #must consistently have this as a category during search
                                    break #TODO
                                    #break: if we assume we only have max one repair onset and reparandum hyp per word
                                    #(embedded deletes in practice v rare and don't affect the main fscores (rps and rm)
                                elif "<rpndel" + r[r.rfind(":"):] in tags[i]:
                                    repairEndCat = "rpndel"
                                    break 
                                elif "<rpnsub" + r[r.rfind(":"):] in tags[i]:#sub
                                    repairEndCat = "rpn" #change this or not to rpnsub
                                    break
                                elif "<rpnrep" + r[r.rfind(":"):] in tags[i]: # rep
                                    repairEndCat = "rpn" #change this or not to rpnrep?
                                    break
                            
                            #just adding to hyps, confirmed after 8
                            hypStack.append((repairID,j,i,i,RP0confidence,RM0confidence,repairEndCat))  #rm0,rp0,rpN(so far, rpN can be extended, though always =rps for deletes)4th in tuple is whether in progress or its classification 
                           
                            #repairlist = []
                    
                        rmPOSngram = nonEditPrefixPOS.word_graph[k-pos_model.order:k]
                        rpPOSngram = nonEditPrefixPOS.word_graph[k-pos_model.order:k-1] + [repairOnsetPOSngram[-1]]
                            #print "rpPOSngram"
                            #print rpPOSngram
                            #referenceFile.write(str(repairOnsetPOSngram)[1:-1].replace("'","").replace(" ","")+",")
                            #referenceFile.write(str(rmPOSngram)[1:-1].replace("'","").replace(" ","")+"\n") #end of reference file stuff
                        
                        reparandumLength+=1 #we work with a non-edit context
                        k=-reparandumLength
                        if reparandumLength > 6: break #only up to lenth n
                    j=j-1
            
                #wait till decision has been made after 8/n- would be nice to be more incremental than this of course
                if not hypStack == []:
                    #confidences = map(lambda x:x[5],hypStack)
                    #sort by confidence first, if the same then the last one in the stack (i.e the longer reparandum)
                    sorting = True
                    hypStack = hypStack[::-1] 
                    while sorting == True:
                        sorting = False
                        for h in range(0,len(hypStack)):
                            if sorting == True: break
                            #check to see if any below it have greater confidence, or the same but longer
                            for hplus in range(h+1,len(hypStack)):
                                if hypStack[hplus][5] > hypStack[h][5]: #only switch if the shorter one has greater conf.
                                    hypStack[hplus],hypStack[h] = hypStack[h],hypStack[hplus]
                                    sorting = True
                                    break
                    hypcount = 0
                    ID = hypStack[0][0] #always the same
                    while hypcount<len(hypStack) and hypcount < stackDepth:
                        rnew = hypStack[hypcount] # gets the longesthypcount+=1
                        hypcount+=1
                        reparandumTag = "rms" #TODO do dels get different treatment..?
                        rm0index = rnew[1]
                    
                        #here we take a slightly fat-fingered approach in that all reparanda in this range are marked
                        if hypcount == 1: ##only adding in most confident as actual tags
                            predictedTags[i]+=predictedRepairTag
                            predictedTags[rm0index]+="<" + reparandumTag + r[r.rfind(":"):]
                            if "rms" in reparandumTag:
                                for t in range(rm0index+1,i): #up to current word, which is its RP
                                    if "<i><e>" in predictedTags[t]: continue #CHOICE can add these if they're in the reparandum..
                                    if not "<rm" + ID in predictedTags[t]:
                                        predictedTags[t]+="<rm" + ID
                        repairStack[i].append(rnew)
                        repairEndD = repairEndData(lm,repairStack[i][-1],words,predictedTags,nonEditPrefixWords,nonEditPrefixPOS)
                        for databit in repairEndD:
                            dataFile.write(str(databit)+",")
                        
                        #Writing actual values
                        #dataFile.write(str(repairOnsetNgram)[1:-1].replace("'","").replace(" ","")+",") #the repair onset words
                        #dataFile.write(str(rmwordngram)[1:-1].replace("'","").replace(" ","")+",") #the rm words
                        #rpNngram = nonEditPrefixWords.word_graph[-lm.order:]
                        #dataFile.write(str(rpNngram)[1:-1].replace("'","").replace(" ","")+",") #the rpn words
                        #dataFile.write(str(rpPOSngram)[1:-1].replace("'","").replace(" ","")+",") #the repair onset POS
                        #dataFile.write(str(rmPOSngram)[1:-1].replace("'","").replace(" ","")+",") #the rm POS
                        #rpNPOSngram = nonEditPrefixPOS.word_graph[-pos_model.order:]
                        #dataFile.write(str(rpNPOSngram)[1:-1].replace("'","").replace(" ","")+",") #the rpn POS
                        
                        
                        
                        dataFile.write(repairEndCat+"\n")
                        #refFile only when a positive classification of an RM0
                        referenceFile.write(reference+","+str(rnew[1]-delta)+","+str(rnew[2]-delta)+","+str(rnew[3]-delta)+",")
                        referenceFile.write(str(repairOnsetNgram)[1:-1].replace("'","").replace(" ","")+",") #the words
                        referenceFile.write(str(rmwordngram)[1:-1].replace("'","").replace(" ","")+"\n")
                    #clear the repair list
                    #OR ALLOW MULTIPLE HYPS.. up to us really, likely to rule out longer repairs
                    repairlist = [] #clear the repair list, so the below doesn't get called
                
                #clearing missed ones
                if (not repairlist == []): #no start found, get rid of all hyps OR use the most confident `no'? Means we can again get redundancy in the reparandum predictor
                    b = len(predictedTags)-1
                    while b >= delta:
                        remove = False                        
                        for rr in range(0,len(repairlist)):
                            r = repairlist[rr]
                            ID = r[r.rfind(":"):]
                            if ID == "": print "incomplete line 1500" + r; raw_input()
                            if "<rms"+ID in predictedTags[b]: remove = True
                            predictedTags[b] = predictedTags[b].replace("<rps"+ID,"").replace("<rpsdel"+ID,"").replace("<rm"+ID,"").replace("<rms"+ID,"")
                            if remove == True: del repairlist[rr]; break 
                        if remove == False: b=b-1
                    #print "ptags1506"
                    #print predictedTags
                #do the same for the reparanda
                #TODO
                
                #increment
                #self.delayedAccuracy(lm, predictedTags, goldTags, 6, ["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"])
                i+=1; editCount+=1 #overall counter increments
                repairCount+=1 #overall repair increments as we've got one
            self.reset_utterance(lm,predictedTags,tags,["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"],evaluation=evaluation) #overall for sentence, just do false p's and false n's
            
            #overall tag string here of reparandum words and repair onset.
            #if error == True:
            #    tagstring = ""
            #    for x in range(delta,len(goldTags)):
            #        tagstring+=words[x] + predictedTags[x].upper() + " " #gold on left, predicted on right in caps
            #   tagFile.write(reference + "\t" + tagstring[:-1]+"\n")
            #maybe focus on the errors!
        #checks
        #assert repairCount == len(repairfilelines)
        #assert editCount == len(editfilelines)
        #assert reparandumCount+1 == len(reparandumfilelines) 
        #these will no longer work
        self.fileIndex = editCount
        self.repairIndex = repairCount
        self.reparandumIndex = reparandumCount
        if evaluation == True:
            print self.accuracy_evaluation()
        return