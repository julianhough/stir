from collections import defaultdict
import csv
import re
import sys
import os

sys.path.append(os.path.join(os.getcwd(),".."))
from language_model.ngram_language_model import NgramGraph
from corpus.util import easy_read_disf_format
from stir_classifier import stir_classifier


class final_classifier(stir_classifier):
    """Outputs the best tag for each word from the whole pipeline. I.E. The non-incremental output of STIR.
    """
    def __init__(self,lm,pos_model=None,evaluation=True,corpus=None,corpus_name=None,\
                 editFile=None,repairFile=None,reparandumFile=None,repairExtentFile=None,\
                 tagFile=None, incrementalTagFile=None, easyReadTagFile=None,\
                 partial_words=False,\
                 editGold=False,repairGold=False,reparandumGold=False,repairextentgold=False,\
                 stackDepth=1):

        if editGold==False: editFileName = editFile[editFile.rfind(os.sep)+1:editFile.rfind('.text')]
        else: editFileName = "editGOLD"
        if repairGold==False: repairFileName = repairFile[repairFile.rfind(os.sep)+1:repairFile.rfind('.text')]
        else: repairFileName = "repairGOLD"
        if reparandumGold==False: reparandumFileName = reparandumFile[reparandumFile.rfind(os.sep)+1:reparandumFile.rfind('.text')]
        else: reparandumFileName = "reparandumGOLD"
        if repairextentgold==False: repairExtentFileName = repairExtentFile[repairExtentFile.rfind(os.sep)+1:repairExtentFile.rfind('.text')]
        else: repairExtentFileName = "repairExtentGOLD"
        
        
        print "FINAL TAG OUTPUT experiment\ncorpus File : " + corpus_name + \
        "\neditFile :" +editFileName + "\nrepairFile:" + repairFileName + \
        "\nreparandumFile :" + reparandumFileName + \
        "\nrepairExtentFile :" + repairExtentFileName
        
        multipleRPS = [] #can check this if needs be
        
        #file = open(dataFile, "w")
        

        #ref file to be read from as a check!
        referenceFile = reparandumFile[:reparandumFile.rfind(os.sep)+1]+"rpn_data_"+corpus_name+"REFERENCE.csv"
        referenceFile = open(referenceFile,"r")
        
        tagFile = open(tagFile,"w")
        #incrementalTagFile = open(incrementalTagFile,"w")
        easyReadTagFile = open(easyReadTagFile,"w")
        
        editFile = open(editFile,"r")
        editfilelines = list(editFile.readlines())
        repairFile = open(repairFile,"r")
        repairfilelines = list(repairFile.readlines()) #the repair onset results after edit
        reparandumFile = open(reparandumFile,"r")
        reparandumfilelines = list(reparandumFile.readlines())
        repairExtentFile = open(repairExtentFile,"r")
        repairExtentfilelines = list(repairExtentFile.readlines())
        
        
        repairExtentReferencefilelines = list(referenceFile.readlines())
        
        #get to header of the results        
        while not editfilelines[0][0:5]== "inst#":
            del editfilelines[0]
        while not repairfilelines[0][0:5]== "inst#":
            del repairfilelines[0]
        while not reparandumfilelines[0][0:5] == "inst#":
            del reparandumfilelines[0]
        while not repairExtentfilelines[0][0:5] == "inst#":
            del repairExtentfilelines[0]
        
        #evaluation measures for all edit terms, and we can break this down into <e> and <i> and inc
        self.eval_init(lm,["<i><e>","<i:","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:","<rp","<rp:","<rpn","<rpn:","<rpndel:"]) #relaxes rm and rms, only looking at rps, not rp or rpn
        editCount = 1 #goes through all the examples, 1-indexed, skipping first line of file
        repairCount = 1
        reparandumCount = 0
        repairExtentCount = 0
        
        
        delta = lm.order-1
        posdelta = pos_model.order-1
        p = posdelta - delta
        FPindex = 0 #creates index for FPs so only the correct ones are removed
        
        end = False
        
        def repairEndData(lm,repair,words,predictedTags,nonEditPrefixWords,nonEditPrefixPOS):
            """returns the data needed for the repair end for a given lm and tag sequence and the
            rm and rp hypotheses so far"""
            delta = lm.order-1
            posdelta = pos_model.order-1
            #repair object structure
            #(repairID,RPSpos,RMSpos,currentPOS,rp0confidence,rm0confidence,categoryAtcurrentpos)#7-tuple
            j = repair[1] #rms absolute position
            i = repair[2] #rps absolute position
            #k = repair[3] #rpn absolute position
            n = repair[4] #current position of word pointer
            #now need to discount edit words
            embedded = None
            if len(predictedTags)-1 > i: #i.e. beyond the current RPS
                if "<rps" in predictedTags[-1]:
                    embedded = True
                else: embedded = False
            #adjust these for edits
            for tagCount in range(delta,len(predictedTags)):    
                if "<i><e>" in predictedTags[tagCount]:
                    if tagCount <= repair[1]:
                        j = j -1 #discount the edit words
                    if tagCount <= repair[2]:
                        i = i-1 #discount the edit words
                    n = n-1 # gets us up to the current nonEditPrefixWords marker, could be different in POS marker (n+p)
        
            reparandumLength = i - j
            repairLength = n - i + 1 #i.e. potential repair length; the number of tokens from the RPS to the current prefix position + 1
            if repairLength > 6: return None #do this here!
            
            ######################got this far, carry on
            RP0confidence = float(repair[5])
            RM0confidence = float(repair[6])
            RPNconfidence = float(repair[7])
            
            #data = [] #mainly here just to distinguish between exhausted 8-word ones   
            data = [RP0confidence,RM0confidence,RPNconfidence,reparandumLength,repairLength,embedded] #basic features
            return data
        
        a,b,c,d = corpus
        for reference,words,pos,tags in zip(a,b,c,d):    
            #if ranges and not reference.split(":")[0] in ranges:
            #   continue
        
            nonEditPrefixWords = NgramGraph(lm)
            nonEditPrefixPOS = NgramGraph(pos_model) ##MUST BE POS MODEL! 
            words = delta * ["<s>"] + list(words) #all the non-edit words so far these can be removed with "TWO" word hypothesis, though not added to
            tags = delta * [""] + self.convert_to_old_stir_format(list(tags))
            pos = posdelta * ["<s>"] + list(pos)
            repairStack = defaultdict(list) #a stack of all the hypotheses being classified, indexed by the rps position
            #evaluation sequences
            predictedTags = delta * [""] #get these true negs for free! #we can evaluate tags so far, and ammend this, provides a way of removing entire repair annotations when a hypothesis is revoked
            #goldTags = delta * [""]  #this does not change incrementally, can use this or words
            
            #word loop through utterance/fileline
            target = editCount + len(words) - delta
            i = delta #word index
            
            while editCount < target:
                #print editCount
                editTag = ""
                goldTag = ""
                evalTag = "" #more fine grained, i..e <i> vs. <e>
                line = editfilelines[editCount] #data line from edit predictions
                data = line.split()
                #[1] is gold
                if ":ONE" in data[1] or ":TWO" in data[1]:
                    assert "<e" in tags[i] or "<i" in tags[i], tags #check this corresponds!
                    if "<i:" in tags[i]:
                        evalTag+="<i:"
                    #elif "<e" in tags[i]:
                    #    evalTag+="<e>"
                    goldTag = "<i><e>"+evalTag #one for now, can only get recall for interregna
                    #goldTag = evalTag
                #goldTags.append(goldTag)
                #evalTags.append(evalTag)
                #[2] is predicted, we can change the [2]s below to [1]s to give perfect results in testing
                if editGold==True:
                    editTag = goldTag  
                elif (":ONE" in data[2]) or (":TWO" in data[2]):
                    editTag+="<i><e>" #in prediction we don't discriminate
                    if (":TWO" in data[2]):
                        if not "<i><e>" in predictedTags[-1]  and i>delta: #NOT detected incrementally, this is CORRECTING the hyp
                     
                            if "<rps" in predictedTags[-1] or not repairStack.get(len(predictedTags)-1)==None:
                                #deletes all the RPS/RM(S)/RPs at this point
                                repairRevokelist = re.findall('<rps\:[0-9]+>', predictedTags[-1], re.S) + re.findall('<rpsdel\:[0-9]+>', predictedTags[-1], re.S)
                                repairRevokelist+= re.findall('<rps\:FP[0-9]+>', predictedTags[-1], re.S) + re.findall('<rpsdel\:FP[0-9]+>', predictedTags[-1], re.S)#nb. need an FP index too as only want to get rid of those
                                #print "clearing out revokes"
                                #print repairStack
                                #repairStack = filter(lambda x: not "<rps" + x[0] in repairRevokelist and not "<rpsdel" + x[0] in repairRevokelist\
                                #                     and not x[2]==i-1, repairStack) #note a bit tricky- removing 'imaginary' repairs, i.e. those hypothesized at that point, but subsequently revoked..
                                try:
                                    del repairStack[len(predictedTags)-1]
                                except KeyError:
                                    print "failed to delete"
                                    pass
                                #print repairStack
                                #clear the other elements efficiently
                                b = len(predictedTags)-1
                                while b >= delta and len(repairRevokelist)>0:
                                    remove = False
                                    for rr in range(0,len(repairRevokelist)):
                                        r = repairRevokelist[rr]
                                        ID = r[r.rfind(":"):]
                                        if ID == "": print "incomplete" + r; raw_input()
                                        if "<rms"+ID in predictedTags[b]: remove = True
                                        predictedTags[b] = predictedTags[b].replace("<rm"+ID,"").replace("<rms"+ID,"").replace("<rpn"+ID,"").replace("<rp"+ID,"").replace("<rpndel"+ID,"").replace("<i"+ID,"")
                                        if remove == True: del repairRevokelist[rr]; break 
                                    if remove == False: b=b-1
                                
                            if not "<i><e>" in predictedTags[-1]:
                                predictedTags[-1]="<i><e>"  #
                            #NB DON'T ALLOW FURTHER BACKTRACKING PAST THE FIRST WORD OF THE NONEDITWORDS
                            if len(nonEditPrefixWords.word_graph)>delta: #JUST POPS ONE OFF THE END
                                nonEditPrefixWords = nonEditPrefixWords.subgraph([0,len(nonEditPrefixWords.word_graph)-1],lm)
                                nonEditPrefixPOS = nonEditPrefixPOS.subgraph([0,len(nonEditPrefixPOS.word_graph)-1],pos_model)

                predictedTags.append(editTag) #add most recent
               
                #now get repair/reparandum/repairEnd tags
                goldRepairTag = ""
                predictedRepairTag = ""
                
                repairlist = re.findall('<rps\:[0-9]+>', tags[i], re.S) + \
                re.findall('<rpsdel\:[0-9]+>', tags[i], re.S) #could be embedded deletes, CLEARING THESE FOR NOW!
                #note- removing the shortest embedded ones
                if len(repairlist)>1:
                    b = i
                    while b >= delta and len(repairlist)>1:
                        remove = False
                        for rr in range(0,len(repairlist)):
                            r = repairlist[rr]
                            ID = r[r.rfind(":"):]
                            if ID == "": print "incomplete" + r; raw_input()
                            if "<rms"+ID in tags[b]: remove = True    
                            if remove == True: del repairlist[rr]; multipleRPS.append(list(words)); break 
                        if remove == False: b=b-1
                        else: break
                    assert(len(repairlist)<=1)
                for r in repairlist: #could reduce problem here
                    #if not "<i><e>" in goldTags[i]: goldTags[i]+=r; goldRepairTag = "<rps" #we don't consider these so won't hinder final score
                    #goldTags[i]+=r; 
                    goldRepairTag = "<rps"
                
                #reparandumlist = re.findall('<rms?\:[0-9]+>', words[i], re.S)
                #for r in reparandumlist:
                #    if not "<i><e>" in goldTags[i]: goldTags[i]+=r #we don't consider these so won't hinder..
                #reparandumlist = []
                
                #repairExtentlist = re.findall('<rp\:[0-9]+>', words[i], re.S) + re.findall('<rpndel\:[0-9]+>', words[i], re.S) \
                #+ re.findall('<rpnrep\:[0-9]+>', words[i], re.S) + re.findall('<rpnsub\:[0-9]+>', words[i], re.S)
                #for r in repairExtentlist:
                #    if not "<i><e>" in goldTags[i]: goldTags[i]+=r
                #repairExtentlist = []
                
                    
                if (not editTag == ""): #this should link to the repair file- this will not get embedded edit terms within reparanda yet
                    #these are not even considered (unless we incorporate the 'rejector' module in phase 2 :(
                    #we will simply output this to file
                    #self.increment_prediction_prefix_to_file(i, predictedTags,words,pos,delta,incrementalTagFile, reference)
                    editCount+=1; i+=1; continue #they will get incremented below otherwise
                    #print words[:i]
                    #print predictedTags
                    #print goldTags
                    #raw_input()
                #got this far we have repairable
                #also, gets here we've got a non-edit word, add to prefixes
                nonEditPrefixWords.append(words[i],lm) #auto calculates probs
                nonEditPrefixPOS.append(pos[(i+p)],pos_model)
                if len(nonEditPrefixWords.word_graph)<=lm.order or i < lm.order: #i.e. only up to <s> <s> first word, repairfile won't have incremented #i.e. only up to <s> <s> first word, repairfile won't have incremented
                    #self.increment_prediction_prefix_to_file(i, predictedTags,words,pos,delta,incrementalTagFile, reference)
                    
                    i+=1; editCount+=1; continue #only allow repairables..#now check the repairStack and check up to length 8, i.e. eight words beyond this point
                
                #got this far we have a repairable word, same as above method, can increment repairCount
                repairline = repairfilelines[repairCount].split()
                #if we predict a repair onset:
                if "rps" in repairline[2] or (repairGold==True and "rps" in repairline[1]): #could be a del
                    rp0confs = repairline[-1].split(",")
                    RP0confidence = float(filter(lambda x: "*" in x,rp0confs)[0].replace("*",""))
                    #RP0confidence = repairline[-1].split(",")[1].replace("*","") #gives the confidence of the repair prediction
                    #predictedRepairTag = "<rps"
                    for r in repairlist: #can add the (ONLY!) one in the repair list
                        if "rpsdel" in repairline[2]:
                            if "del" in r: predictedRepairTag+=r
                            else: predictedRepairTag+="<rpsdel"+r[4:] #false pos for dels
                        elif "rpsdel" in r: predictedRepairTag+="<rps"+r[7:]  #false neg for dels
                        else: predictedRepairTag+=r #just a normal non del
                    if predictedRepairTag == "":
                        if "rpsdel" in repairline[2]:
                            predictedRepairTag+="<rpsdel:FP" + str(FPindex) + ">" 
                        else: predictedRepairTag+="<rps:FP" + str(FPindex) + ">" 
                        repairlist = [predictedRepairTag]
                        FPindex+=1
                        
                #checks and balances
                if "<rps" in goldRepairTag:
                    #print repairline
                    assert("rps" in repairline[1]),str(repairline) + str(words) + str(i) + str(reference) #check it's made it through, won't work on MRC
                else:
                    assert("non" in repairline[1]),str(repairline) + str(words) + str(i) + str(reference)
                 
                #goldTags[i] = goldRepairTag
                #predictedTags[i]+=predictedRepairTag
                #WAIT TILL BELOW TO ADD THIS!
                
                #RPN detection- iterate through repair stack in order of rps position- i.e. key order! 1,2,3
                    #then iterate for each rps position through the levels of confidence as sorted
                    #-re-sort the hypotheses for each rps
                    #-if not rpn/c0's found at all, it will cancel it
                #iterating over stack items
                #only output the tags for the most confident hypothesis for that rps
                #if 6 words beyond i, all existing hyps with no RPN hyp get 0
                #print "starting stack it"
                for key,stack in sorted(repairStack.items()): #iterates through in order
                    #print "key!:"
                    #print key
                    topCombinedConfidence = -1 #sum the confidences of all repair/reparandum/repairEnd for this repair start
                    topHypothesis = None #for the labelling from this point
                    remove = False #if over 6, this key is removed
                    ID = stack[0][0] #ID always the same for all in this one
                    for rr in range(0,len(stack)): #just go through hypothesis for this rps
                        rnew = list(stack[rr]) #we have 6 categories here- rp, cancel, rpn, rpndel, c0, c
                        assert len(rnew) == 10, rnew #get the simple repair structure
                        oldIndex = rnew[3] #the previous rpn hypothesis or boundary if not yet found  
                        rnew[4] = i #increment end point to where we are
                        alreadyFinishedBool = False
                        currentRPNconfidence = rnew[7] #current confidence, can be 0 if cancelled, else 1/6 if rpn not found yet
                        repairExtentTag = "" #this can be structurally determined by previous tags
                        if rnew[-1] == "cancel" or rnew[-1] == 'c': #if c, it is 'safe', cannot be cancelled, though new rpn posisble
                            alreadyFinishedBool = True
                            if not rnew[-1] == "cancel":
                                repairExtentTag = rnew[-1] #only for 'c's
                            pass #remains the same
                        elif "c0" in rnew[-1]:
                            rnew[-1] = "c" #increments to c
                            repairExtentTag = rnew[-1]
                            alreadyFinishedBool = True #also stops it
                        elif "rpn" in rnew[-1]: #nb stability due to ,<i><e> can this get ammended? yes
                            if "<i><e>" in predictedTags[oldIndex]:
                                b = oldIndex
                                while b >= delta and "<i><e>" in predictedTags[b]:
                                    b=b-1
                                rnew[3] = b
                                rnew[-1] = "rpn"; repairExtentTag = "rpn"; alreadyFinishedBool = True #ensures end is incremented
                            else: rnew[-1] = "c0"; repairExtentTag = "c0"; alreadyFinishedBool = True #gone past the rpn, this is the only one worth recording, else `c'
                        elif "rp" in rnew[-1]: #or pre-cancel
                            #raw_input()
                            pass #do nothing, see if cancel happens         
                                        
                        #if none of these cases, it remains a rp (in progress) and not changed
                        repairData = repairEndData(lm,tuple(rnew),words,predictedTags,nonEditPrefixWords,nonEditPrefixPOS)
                        if repairData == None: #beyond 6 words
                            remove = True; break #remove all these hypotheses as beyond 6 at this point
                        
                        #now get the classification, only focus on rpn's vs non-rpns for now
                        repairExtentCount+=1
                        try:
                            repairExtentData = repairExtentfilelines[repairExtentCount].split()
                            dataRepairExtentTag = repairExtentData[2][repairExtentData[2].rfind(":")+1:]
                            rpnconfs = repairExtentData[-1].split(",")
                            RPNconfidence = float(filter(lambda x: "*" in x,rpnconfs)[0].replace("*",""))
                        except:
                            print "1936"
                            print "repairExtentData too far!" + str(words) + str(reference) + str(predictedTags) + "\n" \
                            + str(repairExtentCount)
                            print sys.exc_info()
                            raw_input()
                            
                        #either finished repair that gets a better hypothesis OR unfinished repair that gets one
                        if repairExtentTag == "" \
                                            or (repairExtentTag[0] == "c"\
                                                     and ((dataRepairExtentTag=="rpn" or dataRepairExtentTag=="c0")
                                                    and RPNconfidence>currentRPNconfidence)): #i.e. no auto classification, only for 'safe' repairs at c0 onwards
                            repairExtentTag = dataRepairExtentTag
                            if repairExtentTag == "cancel" and not "precancel" in rnew[-1]:
                                repairExtentTag = "precancel"
                            rnew[-1] = repairExtentTag #will put it in its current state
                            alreadyFinishedBool = False
                            
                        #given the current tags for all hyps and their confidence, get the best ones
                        if "rp" in rnew[-1]: #any repair label gets added
                            if rnew[-1] == "rp" and alreadyFinishedBool == True:
                                pass
                                #rnew[7] = (1.0/6.0) #shouldn't have changed, won't change its rnew[3]
                                #currentCombinedConfidence = rnew[5] * rnew[6] * rnew[7]
                                #if currentCombinedConfidence >= topCombinedConfidence:
                                #    topCombinedConfidence = currentCombinedConfidence
                                #    topHypothesis = rnew
                                #pass #not being generous for RPs, treating as cancelled
                            else: #we have an rpn or legit rp
                                if rnew[-1] == "rp":
                                    rnew[7] = (1.0/6.0)
                                    rnew[3] = i
                                    currentCombinedConfidence = (1.0 * rnew[5]) * (1.0 * rnew[6]) * (1.0 * rnew[7]) #i.e. max ent rather than 0, any positive hyp should be better than this
                                    if currentCombinedConfidence >= topCombinedConfidence:
                                        topCombinedConfidence = currentCombinedConfidence
                                        topHypothesis = rnew
                                        #WILL DO LABELLING BELOW
                                        #predictedTags[-1]+="<"+rnew[-1]+rnew[0] #only add if better confidence
                                #if "FP1771" in rnew[0]: raw_input("stop 1916" + str(predictedTags))
                                elif "rpn" in rnew[-1]: #we need to reintroduce this to the predicted tags if it's been deleted
                                    #only tag/replace if the combined confidence is better than previous
                                    if RPNconfidence > currentRPNconfidence:
                                        currentRPNconfidence = RPNconfidence
                                        rnew[7] = RPNconfidence #only change if better
                                        rnew[3] = i
                                    currentCombinedConfidence = (1.0 * rnew[5]) * (1.0 * rnew[6]) * (1.0 * rnew[7])
                                    if currentCombinedConfidence >= topCombinedConfidence:
                                        topCombinedConfidence = currentCombinedConfidence #this is now the most confident positive
                                        topHypothesis = rnew
                                        #WILL DO LABELLING BELOW
                                    #retintro should be complete
                                    rnew[-2] = False
                                    
                        elif "c0" in rnew[-1] and alreadyFinishedBool==False: #i.e. we have a c0 before an rpn
                            #only replace if better            
                            if RPNconfidence > currentRPNconfidence:
                                currentRPNconfidence = RPNconfidence
                                rnew[7] = RPNconfidence #only change if better
                                b = len(predictedTags)-2 #from one further back than above
                                while "<i><e>" in predictedTags[b]:
                                    b=b-1
                                rnew[3] = b #i.e. this is the rpn
                            currentCombinedConfidence = (1.0 * rnew[5]) * (1.0 * rnew[6]) * (1.0 * rnew[7]) 
                            if currentCombinedConfidence > topCombinedConfidence:
                                topCombinedConfidence = currentCombinedConfidence #this is now the most confident positive
                                topHypothesis = rnew
                                #WILL DO LABELLING BELOW
                            #retintro should be complete
                            rnew[-2] = False
                            
                        elif "precancel" in rnew[-1] and alreadyFinishedBool==False:
                            rnew[7] = 0
                            if topCombinedConfidence <= 0: #cancelled temporarily, no confidence
                                topHypothesis = rnew
                                topCombinedConfidence = 0
                            #DO TAGGING BELOW
                            #predictedTags[-1]+="<rp"+rnew[0] #speculating
                            #if "FP1771" in rnew[0]: raw_input("stop 1952" + str(predictedTags))
                        elif rnew[-1] == "cancel" and alreadyFinishedBool == False: #first time we have a cancel
                            #separating precancel and cancel ok here
                            rnew[7] = 0
                            if topCombinedConfidence <= 0: #cancelled temporarily, no confidence
                                topHypothesis = rnew
                                topCombinedConfidence = 0
                            rnew[-2] = True #i.e. cancelled
                        else:
                            myconf = (1.0 * rnew[5]) * (1.0 * rnew[6]) * (1.0 * rnew[7])
                            if myconf > topCombinedConfidence:
                                topCombinedConfidence = myconf
                                topHypothesis = rnew
                        stack[rr] = tuple(rnew) #replace the old one
                        
                        #referenceFile.write(reference+","+str(rnew[1]-delta)+","+str(rnew[2]-delta)+","+str(rnew[3]-delta))
                        #referenceFile.write(str(repairOnsetNgram)[1:-1].replace("'","").replace(" ","")+",") #the words
                        #referenceFile.write(str(rmwordngram)[1:-1].replace("'","").replace(" ","")+"\n")
                        if repairExtentReferencefilelines[repairExtentCount].split(",")[0:4] != [reference,str(rnew[1]-delta),str(rnew[2]-delta),str(rnew[4]-delta)]:
                            print "2028"
                            print predictedTags
                            print reference
                            print repairExtentReferencefilelines[repairExtentCount].split(",")[0:4]
                            print [reference,str(rnew[1]-delta),str(rnew[2]-delta),str(rnew[4]-delta)]
                            for r in stack:
                                print r
                            raw_input()
                    
                    repairStack[key] = stack #to make sure this is replaced
                    #TAGGING
                    #clear all previous tags
                    #CLEAR STACK- means things should have stayed where they were
                    clear = True
                    endofHyp = False
                    if remove == True: #if we've reached 6 words, cancel or stick
                        endofHyp = True
                        b = len(predictedTags)-1
                        clear = True
                        while clear == True and b >= delta:
                            if "<rpn" + ID in predictedTags[b] or "<rpndel" + ID in predictedTags[b]:
                                clear = False
                            b=b-1
                        try:
                            del repairStack[key]
                        except KeyError:
                            pass
                        
                    if clear == True: #otherwise do stuff
                        b = len(predictedTags)-1 #just iterate back
                        while b >= delta:
                            removeRepair = False
                            if "<rms"+ID in predictedTags[b]: removeRepair = True
                            predictedTags[b] = predictedTags[b].replace("<rps"+ID,"").replace("<rpsdel"+ID,"").\
                            replace("<rm"+ID,"").replace("<rms"+ID,"").replace("<rp"+ID,"").replace("<i"+ID,"").\
                            replace("<rpn"+ID,"")
                            if removeRepair == True: break
                            else: b=b-1
                    #RELABEL with best hyp tags
                    if topCombinedConfidence > 0 and endofHyp==False: #if there's at least one not cancelled, add the most confident hypothesis
                        reparandumEntered = None
                        if not topHypothesis[-1] == "rp": #i.e. rpn or c0
                            predictedTags[topHypothesis[3]]+="<rpn"+ID #only add if better confidence
                            if topHypothesis[2] == topHypothesis[3]:
                                predictedTags[topHypothesis[3]]+="<rps"+ID
                                if not "<i><e>" in predictedTags[topHypothesis[3]-1]: reparandumEntered = True
                                else: reparandumEntered = False
                            b = topHypothesis[3]-1 
                        else: #we have a currently still active <rp that is beyond rps
                            b = topHypothesis[3]
                        
                        
                        while b >= delta: #need to reintroduce all tags
                            if "<i><e>" in predictedTags[b] and (reparandumEntered==True or reparandumEntered==None):
                                b=b-1; continue
                            elif "<i><e>" in predictedTags[b] and reparandumEntered==False:
                                predictedTags[b]+="<i" + ID
                                b=b-1; continue
                            if b == topHypothesis[1]:
                                predictedTags[b]+= "<rms"+ID;
                                break #finished
                            elif b < topHypothesis[2]:
                                predictedTags[b]+= "<rm"+ID
                                reparandumEntered=True
                            elif b == topHypothesis[2]:
                                predictedTags[b]+= "<rps"+ID
                                reparandumEntered = False
                                #reparandumEntered=True
                            else:
                                predictedTags[b]+="<rp"+ID
                            b=b-1
                    
                
                if not "<rps" in predictedRepairTag: #only looking from predicted repair points
                    #self.increment_prediction_prefix_to_file(i, predictedTags,words,pos,delta,incrementalTagFile,reference) #for <i> and <e> should be no different
                    i+=1; editCount+=1;
                    repairCount+=1  #otherwise incremented below
                    continue
                
                #TODO improve these #we have a repair
                for r in repairlist: #needs to match to one in here, even if FP
                    #if "del" in r:
                    #    delextra = r[7:]; reparandumTag = reparandumTag.replace("del","")
                    #else: delextra = r[4:]
                    repairID = r[r.rfind(":"):]
                    break #should only be one
                editString = ""
                if "<i><e>" in predictedTags[i-1]:
                    editString+=words[i-1]
                    #predictedTags[i-1]+="<i"+repairID
                    j = i-1
                    while "<i><e>" in predictedTags[j] and j>=(delta):
                        editString = words[j]+" "+editString
                        predictedTags[j]+="<i"+repairID #we have a specific interregnum marker now
                        j=j-1
                    editString=editString[:-1]
                else: j = i-1
                if editString == "": editString = "NULL"
                else: editString = "EDIT" #try more coarse grained approach
                
                """
                #all candidates from j, skip over the non edits
                """
                reparandumLength = 1
                k = -1
                repairOnsetNgram = nonEditPrefixWords.word_graph[-lm.order:] #the uncleaned repair trigram
                #local boost of the trigram
                
                #start = j #for tracking backtrack extent
                #    repairOnsetPOSngram = nonEditPrefixPOS.word_graph[-pos_model.order:]
                    #local boost of the trigram
                    
                orig = False
                hypStack = []
                while j >= delta: #backwards search until we hit first word 
                    if not "<i><e>" in predictedTags[j]:#we don't allow reparanda with just edit term rms.. though do in fact include them as reparanda for evaluation when sandwiched
                        reparandumCount+=1 #increment here
                        reparandumline = reparandumfilelines[reparandumCount].split()
                        #cat = "NON"
                        reparandumTag = "NON" #original utt/or FP (not repair) for this one
                        #if orig == True: #non
                        #    #if ":NON" in reparandumline[2]: tag = "NON"
                        #    #else: orig = False; tag = "RM"
                        #    pass
                        rm0confs = reparandumline[-1].split(",")
                        RM0confidence = float(filter(lambda x: "*" in x,rm0confs)[0].replace("*",""))
                        #RM0confidence = reparandumline[-1].split(",")[1].replace("*","") #get the confidence of the reparandum start classifier
                        
                        if ":RMS" in reparandumline[2]:
                            orig = True
                            if ":RMSDEL" in reparandumline[2]: extradel = "" # "del"
                            else: extradel = ""
                            reparandumTag = "rms" + extradel
                        elif ":RM" in reparandumline[2]:
                            reparandumTag = "rm"
                        elif orig == False: #being generous here
                            reparandumTag = "rm"
          
                        #the hypothesised rm onset likelihood should be high
                        #rmwordngram = nonEditPrefixWords.word_graph[k-lm.order:k] #is this in fact correct
                        rpwordngram = nonEditPrefixWords.word_graph[k-lm.order:k-1] + [repairOnsetNgram[-1]]
                        if len(rpwordngram) < lm.order: #no more..
                            break
                        
                        if (not repairlist == []):
                            if "rms" in reparandumTag and len(repairlist)>1: #i.e. if we've got two or more
                                print("more than one RP0 hypothesised for word " + str(i) + str(words[i])) #we allow this here, but not above?    
                                raw_input()
                        
                        #todo, currently only taking the first one
                        if "rms" in reparandumTag and len(repairlist)>0: #popping it off (first one) adding to repair list
                            assert(len(repairlist)==1)
                            for r in repairlist: #needs to match to one in here, even if FP
                                #if "del" in r:
                                #    delextra = r[7:]; reparandumTag = reparandumTag.replace("del","")
                                #else: delextra = r[4:]
                                repairID = r[r.rfind(":"):]
                                break #should only be one
                            
                            repairEndCat = "" #just stipulating null one for now
                            #now we need some kind of control mechanism- take the first one?
                            #or take the most confident?, start simple, take the first one
                            #"""rnew[6] is RPNconfidence, filled in below"""
                            rnew = [repairID,j,i,i,i,RP0confidence,RM0confidence,0,False,repairEndCat]
                            
                            hypStack.append(tuple(rnew))  #rm0,rp0,rpN(so far, rpN can be extended, though always =rps for deletes)4th in tuple is whether in progress or its classification 
                            #now use that last one, won't be removed
                            #repairEndD = repairEndData(lm,repairStack[-1],words,predictedTags,nonEditPrefixWords,nonEditPrefixPOS)
                            ##refFile only when a positive classification of an RM0
                            
                            
                            #referenceFile.write(reference+","+str(j-delta)+","+str(i-delta)+","+str(i-delta)+",")
                            #referenceFile.write(str(repairOnsetNgram)[1:-1].replace("'","").replace(" ","")+",") #the words
                            #referenceFile.write(str(rmwordngram)[1:-1].replace("'","").replace(" ","")+"\n")
                            #clear the repair list
                            #OR ALLOW MULTIPLE HYPS.. up to us really, likely to rule out longer repairs
                
                            #repairlist = []
                        
                      

                        #   rmPOSngram = nonEditPrefixPOS.word_graph[k-pos_model.order:k]
                        #   rpPOSngram = nonEditPrefixPOS.word_graph[k-pos_model.order:k-1] + [repairOnsetPOSngram[-1]]
                            #print "rpPOSngram"
                            #print rpPOSngram
                            #referenceFile.write(str(repairOnsetPOSngram)[1:-1].replace("'","").replace(" ","")+",")
                            #referenceFile.write(str(rmPOSngram)[1:-1].replace("'","").replace(" ","")+"\n") #end of reference file stuff
                        
                        reparandumLength+=1 #we work with a non-edit context
                        k=-reparandumLength
                        if reparandumLength > 6: break #only up to lenth n
                    j=j-1
                
                #wait till decision has been made after 8/n- would be nice to be more incremental than this of course
                if not hypStack == []:
                    hypStack = hypStack[::-1] #reverse it first, so the latest ones last
                    sorting = True
                    while sorting == True:
                        #print "sorting"
                        sorting = False
                        for h in range(0,len(hypStack)):
                            if sorting == True: break
                            #check to see if any below it (shorter ones) have greater confidence
                            for hplus in range(h+1,len(hypStack)):
                                if hypStack[hplus][6] > hypStack[h][6]:
                                    hypStack[hplus],hypStack[h] = hypStack[h],hypStack[hplus]
                                    sorting = True
                                    break
                    hypcount = 0
                    bestConfidence = -1
                    ID = hypStack[0][0] # always the same
                    while hypcount < stackDepth and hypcount < len(hypStack):
                        #print "hypcount loop"
                        rnew = list(hypStack[hypcount])
                        hypcount+=1
                        
                        repairExtentCount+=1
                        repairExtentData = repairExtentfilelines[repairExtentCount].split()
                        repairEndCat = repairExtentData[2][repairExtentData[2].rfind(":")+1:]
                        rpnconfs = repairExtentData[-1].split(",")
                        RPNconfidence = float(filter(lambda x: "*" in x,rpnconfs)[0].replace("*",""))
                        #rnew = list(bestHyp)
                        if "rpn" in repairEndCat: #TODO add gold options
                            rnew[-1] = repairEndCat
                            rnew[7] = RPNconfidence #replaces 0
                            #DO tagging below
                        elif "cancel" in repairEndCat: #we have a cancelling action upon first word
                            #rnew[-1] = "cancel" #TODO, do we do an immediate cancel, or not.
                            rnew[-1] = "precancel" #simplest strategy, allow it to continue.. The whole point is to allow one more word
                            rnew[7] = 0
                            #should not be added if confidence is 0
                        else: #could still be an rp, c, c0- it hasn't decided to cancel..
                            rnew[-1] = "rp" #collapsing these for now- could look to change this
                            rnew[7] = 1.0/6.0
                        conf = (1.0 * rnew[5])  * (1.0 * rnew[6]) * (1.0 * rnew[7])
                        if (conf > bestConfidence):
                            bestConfidence = conf
                            bestHypothesis = rnew
                        #reference check
                        repairStack[i].append(tuple(rnew))
                        if repairExtentReferencefilelines[repairExtentCount].split(",")[0:4] != [reference,str(rnew[1]-delta),str(rnew[2]-delta),str(rnew[4]-delta)]:
                            print reference
                            print predictedTags
                            print "2189"
                            print repairExtentReferencefilelines[repairExtentCount].split(",")[0:4]
                            print [reference,str(rnew[1]-delta),str(rnew[2]-delta),str(rnew[4]-delta)]
                            for r in repairStack.keys():
                                print repairStack[r]
                            raw_input()
                            
                    if bestConfidence>0: ##only adding in most confident as actual tags here
                        predictedTags[i]+=predictedRepairTag #tag the repair (could combine with classification here?)
                        predictedTags[i]+="<" + bestHypothesis[-1]+ID
                        reparandumTag = "rms"  #todo do dels get different features?
                        rm0index = bestHypothesis[1]
                        predictedTags[rm0index]+="<" + reparandumTag + ID
                   
                        if "rms" in reparandumTag:
                            for t in range(rm0index+1,i): #up to current word, which is its RP
                                if "<i><e>" in predictedTags[t]: continue #CHOICE can add these if they're in the reparandum..
                                if not "<rm" + ID in predictedTags[t]:
                                    predictedTags[t]+="<rm" + ID
                    elif "<i><e>" in predictedTags[len(predictedTags)-2]:
                        inti = bestHypothesis[2]-1
                        while "<i><e>" in predictedTags[inti]:
                            predictedTags[inti].replace("<i"+ID,"")
                            inti=inti-1
                    
                    repairlist = [] #clear the repair list, so the below doesn't get called
                    #now get the repair end category from the trained model
                    
                #now clearing those with no reparandum onset found- greatly reduces problem
                
                #in theory shouldn't need this now
                if (not repairlist == []): #no start found, get rid of all hyps #TODO can widen this beam!
                    b = len(predictedTags)-1
                    while b >= delta:
                        remove = False                        
                        for rr in range(0,len(repairlist)):
                            r = repairlist[rr]
                            ID = r[r.rfind(":"):]
                            if ID == "": print "incomplete line 2240" + r; raw_input()
                            if "<rms"+ID in predictedTags[b]: remove = True
                            predictedTags[b] = predictedTags[b].replace("<rps"+ID,"").replace("<rpsdel"+ID,"").replace("<rp"+ID,"").replace("<rm"+ID,"").replace("<rms"+ID,"").replace("<i"+ID,"")
                            if remove == True: del repairlist[rr]; break 
                        if remove == False: b=b-1
                    #print "ptags1506"
                    #print predictedTags
                #"""
                #do the same for the reparanda
                #TODO
                
                #increment
                #self.increment_prediction_prefix_to_file(i, predictedTags,words,pos,delta,incrementalTagFile,reference)
                i+=1; editCount+=1 #overall counter increments
                repairCount+=1 #overall repair increments as we've got one

            #Just print the final outputs to tagfile and easy-read file
            self.increment_prediction_prefix_to_file((len(predictedTags)-delta)+1, predictedTags, words, pos, delta, tagFile,reference)
            final_string = reference + "," + easy_read_disf_format(words[delta:], predictedTags[delta:])+'\n'
            final_string+="POS,"+ easy_read_disf_format(pos[posdelta:], predictedTags[delta:])+"\n"
            easyReadTagFile.write(final_string)
	    if evaluation:
	       self.reset_utterance(lm,predictedTags,tags,["<i><e>","<rps","<rps:","<rpsdel:","<rm","<rms:","<rm:"],evaluation=evaluation)
            
            #can focus on the errors!
        processing_overhead = float(editCount+repairCount+reparandumCount+repairExtentCount)/float(editCount)
        print "processing overhead", processing_overhead
        #incrementalTagFile.write("PROCESSING_OVERHEAD: " + str(processing_overhead)) # NB THE ONLY THING NOT DERIVABLE FROM THE TAG FILES
        tagFile.close()
        #incrementalTagFile.close()
        easyReadTagFile.close()
        
        if evaluation == True:
            print self.accuracy_evaluation()
        return
