import math

from stir_classifier import stir_classifier

class repair_onset(stir_classifier):
    
    def __init__(self,lm,pos_model=None,evaluation=True,corpus=None,corpus_name=None,editfilelines=None,dataFile=None,
                 referenceFile=None,ranges=None,fileIndex=None,partial_words=False,editGold=False,previousExp=None):
        """ Takes results from the edit terms identification experiment (and does Zwarts et al. inc eval (delayed reset_utterance etc.)).
        Returns the classification task given to the repair onset classifier,
        Which includes all the initially classified false neg edited terms in the three word context.
        Therefore important to preserve indices in terms of line and word number from the REF file
        """
        window = 3 #TODO could get this as a variable
        #self.fileIndex = fileIndex
        #evaluation measures for all edit terms, and we can break this down into <e> and <i> and inc
        if previousExp == None: self.eval_init(lm,["<i>","<i><e>","<e>"])
        else:
            self.inherit_eval_values(lm, previousExp)
        editCount = fileIndex
        delta = lm.order-1
        posdelta = pos_model.order-1
        p = posdelta-delta #the difference from which we subtract for indexing
        
        a,b,c,d = corpus
        for reference,words,pos,tags in zip(a,b,c,d):
            if ranges and not reference.split(":")[0] in ranges:
                continue
    
            nonEditPrefixWords = delta * ["<s>"] #all the non-edit words so far these can be removed with "TWO" word hypothesis, though not added to
            nonEditPrefixPOS = posdelta * ["<s>"] 
            words = delta * ["<s>"] + list(words) #all the non-edit words so far these can be removed with "TWO" word hypothesis, though not added to
            tags = delta * [""] + list(tags)
            pos = posdelta * ["<s>"] + list(pos)
            #evaluated sequences
            predictedEditTags = delta * [""] #get these true negs for free! #we can evaluate tags so far, and ammend this, provides a way of removing entire repair annotations when a hypothesis is revoked
            evalTags = delta * [""]
            goldEditTags = delta * [""]  #this does not change incrementally
            
            #word loop through utterance/fileline
            target = editCount + len(words)- delta
            i = delta #word index = i-lm.order+1; prefix index = i+1
            wordprevious = 0 # for getting WML drop
            wordEntropyPrevious = lm.entropy_continuation_very_fast(nonEditPrefixWords,lm.order) #default


            posprevious = 0 #getting WML pos drop
            posEntropyPrevious = pos_model.entropy_continuation_very_fast(nonEditPrefixPOS,pos_model.order)
            #print target
            while editCount < target:

                editTag = ""
                goldTag = ""
                evalTag = "" #more fine grained, i..e <i> vs. <e>
                line = editfilelines[editCount] #data line from edit predictions
                #del editfilelines[0]
                ## for ppat, look ahead for 2-word repeats, see how we get on without otherwise
                """
                lookaheadWord = ""
                lookaheadPOS = ""
                if i<len(words)-1:
                    l = editCount + 1
                    o = i + 1
                    while o <len(words):
                        lookaheadline = editfilelines[l]
                        lookahead2line = lookaheadline
                        lookaheaddata = lookaheadline.split()
                        lookahead2data = lookaheaddata
                        if o <len(words)-1: lookahead2line = editfilelines[l+1]; lookahead2data = lookahead2line.split();
                       
                        #assert len(lookaheaddata) == 6 or len(lookaheaddata) == 7 or len(lookaheaddata) == 8 #line length with 3 categories
                        if ":ONE" in lookaheaddata[1] or ":TWO" in lookaheaddata[1]: #2s= predict, 1s = gold
                            assert "<e" in words[o] or "<i" in words[o], reference + str("one ahead") #check this corresponds!
                        if not (":ONE" in lookaheaddata[2] or ":ONE" in lookaheaddata[1])\
                        or (":TWO" in lookaheaddata[2] or ":TWO" in lookaheaddata[1]) or (":TWO" in lookaheaddata[2] or ":TWO" in lookaheaddata[1]): #2s= predict, 1s = gold
                            lookaheadWord = words[o]; lookaheadPOS = pos[o]; break
                        o+=1; l+=1
                """        
                    
                data = line.split()
                #assert len(data) == 6 or len(data) == 7 or len(data) == 8 #line length with 2-4 categories
                #[1] is gold
                if ":ONE" in data[1] or ":TWO" in data[1]:
                    assert "<e" in tags[i] or "<i" in tags[i], reference #check this corresponds!
                    if "<i" in tags[i]:
                        evalTag+="<i>"
                    elif "<e" in tags[i]:
                        evalTag+="<e>"
                    goldTag = "<i><e>" #one for now, can only get recall for interregna
                    #editResultsFile.write(data[1][data[1].rfind(":")+1:] + "\n") #just the results at the mo, excell not great at processing these
            
                goldEditTags.append(goldTag)
                evalTags.append(evalTag)
                
                #[2] is predicted, we can change the [2]s below to [1]s to give perfect results in testing
                if editGold==True:
                    editTag = goldTag
                elif (":ONE" in data[2]) or (":TWO" in data[2]): #can do both to increase recall
                    #if "<i" in words[i] or "<e" in words[i]:
                    editTag+="<i><e>" #in prediction we don't discriminate
                    if ":TWO" in data[2]:
                        if (not predictedEditTags[-1] == "<i><e>") and i>delta: #NOT detected incrementally, this is CORRECTING the hyp
                            if i > delta: predictedEditTags[-1] = "<i><e>" #if <e> and <i> are in there, fine for now
                            #predictedEvalTags[-1] = goldEditTags[-1]
                            #NB DON'T ALLOW FURTHER BACKTRACKING PAST THE FIRST WORD OF THE NONEDITWORDS
                            if len(nonEditPrefixWords)>delta:
                                nonEditPrefixWords = nonEditPrefixWords[:-1]
                                nonEditPrefixPOS = nonEditPrefixPOS[:-1]
                            #testStart = delta * ["<s>"]
                            #assert(nonEditPrefixWords[0:delta] == testStart),nonEditPrefixWords
                        
                predictedEditTags.append(editTag)
            
                #in reality reset_utterance won't change beyond 2 back
                #self.delayedAccuracy(lm,predictedEditTags,goldEditTags,6,["<i><e>"])
                #self.delayedAccuracy(lm,predictedEditTags,evalTags,6,["<i>","<e>"])
                
                repairTag = ""
                if "<rps" in tags[i]: #there could be shared rps for embedded repairs I [ like [t- + ]+ love ] it
                    if "<rpsdel" in tags[i]: repairTag = "rps" #rpsdel #indicates first repair point
                    else: repairTag = "rps"
                if not (repairTag == "" or editTag == ""): # de morgan- for  if (not editTag == "") and (not repairTag == ""):
                    repairTag = "" #cancels it
            
                #words[i] = words[i][words[i].rfind('>')+1:] #get rid of all the metadata on words, we have the edit tags
                #pos[(i+p)] = pos[(i+p)][pos[(i+p)].rfind('>')+1:] 
                if not editTag == "<i><e>":
                    nonEditPrefixWords.append(words[i])
                    nonEditPrefixPOS.append(pos[(i+p)])
                #print editTag
                #print repairTag
                i+=1 #word index (2 less than this actually)
                editCount+=1 #overall counter increments
                if i == lm.order:
                    assert repairTag == "", "repairTag =" + repairTag + " at beginnning of utt! " + reference + "\n" + str(words)
                #not considering the first (non-edit) word or beyond the last word (no <s> here, so don't need the length stipulation here..)
                if editTag == "<i><e>": #need at least one non-edit word for the next one to be an onset!
                    continue
                elif len(nonEditPrefixWords)<=lm.order: # we have one word..
                    wordprevious = lm.logprob_weighted_by_inverse_unigram_logprob(nonEditPrefixWords[-lm.order:])
                    wordEntropyPrevious = lm.entropy_continuation_very_fast(nonEditPrefixWords[-delta:],lm.order)
                    posprevious = pos_model.logprob_weighted_by_inverse_unigram_logprob(nonEditPrefixPOS[-pos_model.order:])
                    posEntropy = pos_model.entropy_continuation_very_fast(nonEditPrefixPOS[-posdelta:],pos_model.order)
                    continue
                
                #got this far we have a classifiable element
                if repairTag == "":
                    cat = "non"
                else: cat = repairTag #either "" or "<rps>"
                
                editString = ""
                j=i-2 #different to the experiments below as i is incremented before this..
                if predictedEditTags[j] == "<i><e>":
                    editString = words[j]+" "+editString
                    #j = i-2
                    while predictedEditTags[j] == "<i><e>" and j>=(delta):
                        editString = words[j]+" "+editString
                        j=j-1
                    editString=editString[:-1]
                    
                if editString == "": editString = "NULL"
                else: editString = "EDIT" #try more coarse grained approach, with more data can switch
                
                # we want prob WML and WML-drop?, no first words, no last words.. (does rely on good utterance segmentation..)
                wordngram = nonEditPrefixWords[-lm.order:]
                assert(not "" in wordngram),reference
                
                #print wordngram #probs
                wordlogprob = lm.logprob_weighted_by_sequence_length(wordngram)
                wordWML = lm.logprob_weighted_by_inverse_unigram_logprob(wordngram)
                #if partial_words == True: #TODO fix this, needs to be more principled
                #    if not "Partial" in corpusName and properPrefix(wordngram[delta-1],wordngram[delta]): wordWML = -3.0
                #    elif wordngram[-2][-1] == "-": wordWML = -3.0
                    #approximation to partial words at the mo, needs changing so we don't use the -
                
                
                worddrop = wordprevious - wordWML #or do we do this without the edit words i.e. previouswordngram = nonEditPrefixWords[-lm.order:-1]; previous= sum(lm.tokens_logprob(previouswordngram,lm.order))
                wordprevious = wordWML
                
                wordEntropy = lm.entropy_continuation_very_fast(wordngram[-delta:],lm.order)
                wordEntropyHike = wordEntropy - wordEntropyPrevious
                informationGain = math.log(-wordlogprob/wordEntropyPrevious,2) #the ratio of surprisal of this event to the inherent entropy, seems like a good measure
                #expectedGainSquared = (wordWML - wordEntropyPrevious) * (wordWML - wordEntropyPrevious)
                wordEntropyPrevious = wordEntropy
                
                #the amount of uncertainty from this word
                
                #we simply look back 3 words and look for identity, we are at least on the second word, however third and fourth words back might not be there
                w0w3=False
                w1w3=False
                w2w3=False
                if len(nonEditPrefixWords)<delta+4:
                    w0w3 = None
                else: w0w3 = nonEditPrefixWords[-4] == wordngram[-1]
                if len(nonEditPrefixWords)<delta+3:
                    w1w3 = None
                else: w1w3 = nonEditPrefixWords[-3] == wordngram[-1]
                w2w3 = nonEditPrefixWords[-2] == wordngram[-1] #always comparable
                
                extra = str(w0w3).upper() + "," + str(w1w3).upper() + "," + str(w2w3).upper() + "," + editString
                referenceFile.write(reference+","+str(i)+",")
                for word in wordngram: #triple of the words
                    referenceFile.write(word+",")
                reductions = []
                potentialWMLboost = [] #add this too? #currently pseudo binary- looks at itself and the three (+1) /four (+2) words back
                for b in range(len(nonEditPrefixWords)-1,len(nonEditPrefixWords)-1-(window+2),-1): #best local boost of entropy for the current word (i.e. is there a considerably more likely prefix locally?)
                    if b < (delta): break #can use <s> but not
                    test = nonEditPrefixWords[b-delta:b] + [wordngram[-1]] #test ngram
                    reductions.append(float(wordEntropy)-float(lm.entropy_continuation_very_fast(test[-delta:],lm.order)))
                    potentialWMLboost.append(float(lm.logprob_weighted_by_inverse_unigram_logprob(test))-float(wordWML))
                
                bestEntropyreduce = max(reductions)
                bestWMLboost = max(potentialWMLboost) #biggest difference
                dataFile.write(str(wordlogprob)+","+str(wordWML)+","+str(worddrop)+","+str(wordEntropy)+","+str(wordEntropyHike)+","\
                           +str(bestEntropyreduce)+","+str(bestWMLboost)+","+str(informationGain) + ","+str(len(nonEditPrefixWords)-delta)+","+extra+",") #prb = 1, wml = 2, drop = 3
                
                #now for POS, given we've done the words
                posngram = nonEditPrefixPOS[-pos_model.order:]
                
                poslogprob = pos_model.logprob_weighted_by_sequence_length(posngram)
                posWML = pos_model.logprob_weighted_by_inverse_unigram_logprob(posngram)
                
               
                posdrop = posprevious - posWML #or do we do this without the edit words i.e. previouswordngram = nonEditPrefixWords[-lm.order:-1]; previous= sum(lm.tokens_logprob(previouswordngram,lm.order))
                posprevious = posWML
                
                posEntropy = pos_model.entropy_continuation_very_fast(posngram[-posdelta:],pos_model.order)
                posEntropyHike = posEntropy - posEntropyPrevious
                posInformationGain = math.log(-poslogprob/posEntropyPrevious,2) #the ratio of surprisal of this event to the inherent entropy, seems like a good measure
                #expectedGainSquared = (wordWML - wordEntropyPrevious) * (wordWML - wordEntropyPrevious)
                posEntropyPrevious = posEntropy
                
                reductions = []
                potentialWMLboost = []
                for b in range(len(nonEditPrefixPOS)-1,len(nonEditPrefixPOS)-1-(window+2),-1): #best local boost of entropy for the current word (i.e. is there a considerably more likely prefix locally?)
                    if b < (posdelta): break #can use <s><s> but not <s> in trigram say..
                    test = nonEditPrefixPOS[b-posdelta:b] + [posngram[-1]]
                    reductions.append(float(posEntropy)-float(pos_model.entropy_continuation_very_fast(test[-posdelta:],pos_model.order)))
                    potentialWMLboost.append(float(pos_model.logprob_weighted_by_inverse_unigram_logprob(test))-float(posWML))
                bestPOSentropyreduce = max(reductions)
                bestPOSWMLboost = max(potentialWMLboost)
                
                
                #TODO scale up for all n's
                w0w3=False
                w1w3=False
                w2w3=False
                if len(nonEditPrefixPOS)<posdelta+4:
                    w0w3 = None
                else: w0w3 = nonEditPrefixPOS[-4] == posngram[-1]
                if len(nonEditPrefixPOS)<posdelta+3:
                    w1w3 = None
                else: w1w3 = nonEditPrefixPOS[-3] == posngram[-1]
                w2w3 = nonEditPrefixPOS[-2] == posngram[-1] #always comparable
                
                extra = str(w0w3).upper() + "," + str(w1w3).upper() + "," + str(w2w3).upper()
                for w in range(0,len(posngram)-1):
                    referenceFile.write(posngram[w]+",")
                referenceFile.write(posngram[-1]+"\n") #last line of referenceFile
                dataFile.write(str(poslogprob)+","+str(posWML)+","+str(posdrop)+","+str(posEntropy)+","+str(posEntropyHike)+","+\
                           str(bestPOSentropyreduce)+","+str(bestPOSWMLboost)+ ","+ str(posInformationGain)+","+ extra+",") #prb = 1, wml = 2, drop = 3
                #file.write(poscontext+",")
                
                #for word in wordngram:
                #    dataFile.write(word+",") #NOTE using the actual values themselves
                #for word in posngram:
                #    dataFile.write(word+",") #NOTE using the actual values themselves
                
                dataFile.write(cat+"\n")
                
                #if editCount == 100000 or editCount == 200000 or editCount == 300000 or editCount == 400000 or editCount == 500000:
                #    print str(float(editCount)/float(100000)) + "* 100K complete!"
            #overall reset_utterance end of utterance 
            self.reset_utterance(lm,predictedEditTags,evalTags,["<i>","<e>", "<i><e>"],evaluation=evaluation) #can turn eval off an on
            #end of while loop for words  
        #edit results
        self.fileIndex = editCount #nb for edit file
        if evaluation == True:
            print  self.accuracy_evaluation()
        return