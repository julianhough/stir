import re
import sys
import os
sys.path.append(os.path.join(os.getcwd(),".."))

from stir_classifier import stir_classifier
from language_model.ngram_language_model import NgramGraph

class reparandum_onset(stir_classifier):
    def __init__(self,lm,pos_model=None,evaluation=True,corpus=None,corpus_name=None,editfilelines=None,repairfilelines=None,
                 dataFile=None,referenceFile=None,
                 ranges=None,fileIndex=None,repairIndex=None,
                 partial_words=False,editGold=False,repairGold=False,previousExp=None):
        
        editCount = fileIndex #goes through all the examples, 1-indexed, skipping first line of file
        repairCount = repairIndex
       
        delta = lm.order-1
        posdelta = pos_model.order-1
        p = (posdelta - delta) #the difference, comes into word graphs!!
        
        window = 3 #how far we look back
        
        
        #evaluation measures for all edit terms, and we can break this down into <e> and <i> and inc
        if previousExp == None: self.eval_init(lm,["<i","<i><e>","<e>","<rps","<rps:","<rpsdel"])
        else:
            print "inheriting values"
            self.inherit_eval_values(lm,previousExp)
        end = False
        #reparandumStack = [] #will be pushed and popped
        multipleRPS = []
        #list of all the examples of shared RPS's- need justification of why we don't treat these independently
        #but instead as one big substitution class- is this particularly justified? Cuts the complexity down
        a,b,c,d = corpus
        for reference,words,pos,tags in zip(a,b,c,d):    
            if ranges and not reference.split(":")[0] in ranges:
                continue
        
            nonEditPrefixWords = NgramGraph(lm)
            nonEditPrefixPOS = NgramGraph(pos_model) ##MUST BE POS MODEL!
            
            words = delta * ["<s>"] + list(words) #all the non-edit words so far these can be removed with "TWO" word hypothesis, though not added to
            tags = delta * [""] + self.convert_to_old_stir_format(list(tags)) #TODO need to change this
            pos = posdelta * ["<s>"] + list(pos)
            
            #evaluated sequences
            
            predictedTags = delta * [""] #get these true negs for free! #we can evaluate tags so far, and ammend this, provides a way of removing entire repair annotations when a hypothesis is revoked
            #goldTags = delta * [""]  #this does not change incrementally, should now just be tags
            
            #word loop through utterance/fileline
            target = editCount + len(words)- delta
            i = delta #word index = i-lm.order+1; prefix index = i+1
            while editCount < target:

                editTag = ""
                goldTag = ""
                evalTag = "" #more fine grained, i..e <i> vs. <e>
                line = editfilelines[editCount] #data line from edit predictions
                data = line.split()
                #[1] is gold
                if ":ONE" in data[1] or ":TWO" in data[1]:
                    assert "<e" in tags[i] or "<i" in tags[i], tags #check this corresponds!
                    if "<i" in tags[i]:
                        evalTag+="<i>"
                    elif "<e" in tags[i]:
                        evalTag+="<e>"
                    goldTag = "<i><e>" #one for now, can only get recall for interregna
                    #goldTag = evalTag
                #goldTags.append(goldTag)
                #evalTags.append(evalTag)
                #[2] is predicted, we can change the [2]s below to [1]s to give perfect results in testing
                if editGold==True:
                    editTag = goldTag  
                elif  (":ONE" in data[2]) or (":TWO" in data[2]):
                    editTag+="<i><e>" #in prediction we don't discriminate
                    if (":TWO" in data[2]):
                        if not predictedTags[-1] == "<i><e>" and i>delta: #NOT detected incrementally, this is CORRECTING the hyp
                            if i > delta:
                                predictedTags[-1] = "<i><e>" #Gets rid of any RPS's TODO some cancelling operation needed really! 
                            #predictedEvalTags[-1] = goldTags[-1]
                            #NB DON'T ALLOW FURTHER BACKTRACKING PAST THE FIRST WORD OF THE NONEDITWORDS
                            if len(nonEditPrefixWords.word_graph)>delta: #JUST POPS ONE OFF THE END
                                nonEditPrefixWords = nonEditPrefixWords.subgraph([0,len(nonEditPrefixWords.word_graph)-1],lm)
                                nonEditPrefixPOS = nonEditPrefixPOS.subgraph([0,len(nonEditPrefixPOS.word_graph)-1],pos_model)
                    
                predictedTags.append(editTag)
               
                #now looking at repair flags
                goldRepairTag = ""
                predictedRepairTag = ""
                
                if "<rps" in tags[i]: #only permitting non edits?
                    goldRepairTag = "<rps" #currently not discriminating rps and rpsdel
                
                #if goldTag == "": goldTags[i] = goldRepairTag #TODO this gets checked separately below
                
                if (not editTag == ""): #this should link to the repair file
                    #these are not even considered :(
                   
                    i+=1;editCount+=1; continue #they will get incremented below otherwise
                
                #gets here we've got a non-edit word, add to prefixes
                nonEditPrefixWords.append(words[i],lm) #auto calculates probs
                nonEditPrefixPOS.append(pos[(i+p)],pos_model)
                
                if len(nonEditPrefixWords.word_graph)<=lm.order or i < lm.order: #i.e. only up to <s> <s> first word, repairfile won't have incremented
                    i+=1; editCount+=1; continue #only allow repairables..
                
                #got this far we have a repairable word, same as above method, can increment repairCount
                repairline = repairfilelines[repairCount].split()
                
                if "rps" in repairline[2] or (repairGold==True and "rps" in repairline[1]):
                    rpconfs = repairline[-1].split(",")
                    confidence = float(filter(lambda x: "*" in x,rpconfs)[0].replace("*",""))
                    #confidence = repairline[-1].split(",")[1].replace("*","") #gives the confidence of the repair prediction
                    predictedRepairTag = "<rps"
                
                #checks and balances, and populate repair stack
                repairlist = []
                if goldRepairTag == "<rps":
                    #print repairline
                    assert("rps" in repairline[1]),str(repairline) + str(words) + str(i) + str(reference)#check it's made it through, won't work on MRC
                    if predictedRepairTag == "<rps":
                        repairlist = re.findall('<rps\:[0-9]*>', tags[i], re.S)
                else:
                    assert("non" in repairline[1]),str(repairline) + str(words) + str(i) + str(reference)
                    if predictedRepairTag == "<rps":
                        repairlist = ["<rps:FP>"] #fps only ever have one
                
                predictedTags[i] = predictedRepairTag
                
                #NOTE adding pre-processing of removing the embedded rps that share the same word-rare. Need to think of good explanation for why we're leaving these
                #removal by backwards search
                if len(repairlist)>1:
                    b = i
                    while b >= delta and len(repairlist)>1:
                        remove = False
                        for rr in range(0,len(repairlist)):
                            r = repairlist[rr]
                            ID = r[r.rfind(":"):]
                            if ID == "": print "incomplete" + r; raw_input()
                            if "<rms"+ID in tags[b]: remove = True    
                            if remove == True: del repairlist[rr]; multipleRPS.append(list(words)); break 
                        if remove == False: b=b-1
                        else: break
                assert(len(repairlist)<=1)
                
    
                if not "<rps" in predictedRepairTag: #only looking from predicted repair points
                    i+=1; editCount+=1;
                    repairCount+=1  #otherwise incremented below
                    continue
                
                
                #TODO improve these
                editString = ""
                if predictedTags[i-1] == "<i><e>":
                    j = i-1
                    while predictedTags[j] == "<i><e>" and j>=(delta):
                        editString = words[j]+" "+editString
                        j=j-1
                    editString=editString[:-1]
                else: j = i-1
                if editString == "": editString = "NULL"
                else: editString = "EDIT" #try more coarse grained approach
                
                """
                #all candidates from j, skip over the non edits
                """
                reparandumLength = 1
                k = -1
                repairOnsetNgram = nonEditPrefixWords.word_graph[-lm.order:] #the uncleaned repair trigram/bigram
                #local boost of the trigram
                originalLocalWordWML = lm.logprob_weighted_by_inverse_unigram_logprob(repairOnsetNgram)
                originalLocalWordLogProb = lm.logprob_weighted_by_sequence_length(repairOnsetNgram)
                originalWordEntropy = lm.entropy_continuation_very_fast(repairOnsetNgram[-(delta):],lm.order) #mayonly be the last one for a bigram
                #overall boost of grammaticaility for utterance
                originalWordWML = nonEditPrefixWords.logprob_weighted_by_inverse_unigram_logprob()
                #print "original = " + str(nonEditPrefixWords)
                originalWordLogProb = nonEditPrefixWords.logprob_weighted_by_sequence_length()
                wordprevious = 0 #for WML drop
                start = j #for tracking backtrack extent

                repairOnsetPOSngram = nonEditPrefixPOS.word_graph[-pos_model.order:]
                #local boost of the trigram
                originalLocalPOSWML = pos_model.logprob_weighted_by_inverse_unigram_logprob(repairOnsetPOSngram)
                originalLocalPOSLogProb = pos_model.logprob_weighted_by_sequence_length(repairOnsetPOSngram)
                originalPOSEntropy = pos_model.entropy_continuation_very_fast(repairOnsetPOSngram[-posdelta:],pos_model.order)
                #overall boost for utterance
                originalPOSWML = nonEditPrefixPOS.logprob_weighted_by_inverse_unigram_logprob()
                originalPOSLogProb = nonEditPrefixPOS.logprob_weighted_by_sequence_length()
                POSprevious = 0
                while j >= delta: #backwards search until we hit first word 
                    if not predictedTags[j]=="<i><e>":
                        cat = "NON" #original utt/or FP (not repair) for this one
                        if "<rm" in tags[j]:
                            reparandumList = re.findall('<rms?\:[0-9]*>', tags[j], re.S)
                            #print reparandumList
                            for reparandum in reparandumList:
                                reparandumID = "<rps:" + reparandum[reparandum.rfind(":")+1:reparandum.rfind(">")] + ">"
                                reparandumIDdel = "<rpsdel:" + reparandum[reparandum.rfind(":")+1:reparandum.rfind(">")] + ">"
                                if reparandumID in repairlist or reparandumIDdel in repairlist: #only the ones found by our detector
                                    if reparandumIDdel in repairlist: extradel = "" #"DEL" could leave this
                                    else: extradel = ""
                                    if re.match('<rm\:[0-9]*>',reparandum):
                                        cat = "RM"+extradel
                                    elif re.match('<rms\:[0-9]*>',reparandum): #if there is an rms, pop it off
                                        cat= "RMS"+extradel
                                        #TODO DO WE LET THIS TAKE PRESIDENCE?
                                        repairlist = filter(lambda x: not x == reparandumID and not x == reparandumIDdel, repairlist) #popping it off
                                        # TODO these are rare! i.e. 3/13000
                                        if len(repairlist) > 0: #still some left
                                            #for now we don't have embedded deletes and in fact change this to RM
                                            #DO NOT PUNISH THESE IN THE BELOW METHODS for repair end etc..
                                            multipleRPS.append(list(words))
                                            cat = "RM" + extradel #i.e. we don't treat this as an RMS anymore, problem solved for below, slight hit in RMS terms
                                            print words
                                            print reference
                                            raw_input("more than one! should have been filtered out")
                                    #elif re.match('')
                                        #break #this one takes precedence, not ideal, may have to entertain idea of multiple classifications back
                        #the hypothesised rm onset likelihood should be high
                        rmwordngram = nonEditPrefixWords.word_graph[k-lm.order:k]
                        #the cleaned rpwordngram (i.e. excised of reparnadum)
                        rpwordngram = nonEditPrefixWords.word_graph[k-lm.order:k-1] + [repairOnsetNgram[-1]]
                        if len(rpwordngram) < lm.order: #no more..
                            break
                        #print "rpwordngram"
                        #print rpwordngram
                        
                        wordWML = lm.logprob_weighted_by_inverse_unigram_logprob(rpwordngram)
                        wordlogprob = lm.logprob_weighted_by_sequence_length(rpwordngram)
                        wordRMWML = lm.logprob_weighted_by_inverse_unigram_logprob(rmwordngram)
                        wordRMlogprob = lm.logprob_weighted_by_sequence_length(rmwordngram)
                        
                        wordRM0Entropy = lm.entropy_continuation_very_fast(rmwordngram[-delta:],lm.order)#uncleaned bigram/unigram around the rm0 boundary- should be high
                        wordEntropy = lm.entropy_continuation_very_fast(rpwordngram[-delta:],lm.order) #i.e. for cleaned, will both be the same in bigrams..
                        wordLocalEntropyReduce = wordEntropy - originalWordEntropy
                        
                        wordLocalWMLBoost = wordWML - originalLocalWordWML
                        wordLocalProbBoost = wordlogprob - originalLocalWordLogProb
                        #wordCleanTest = nonEditPrefixWords[:k-1] + [nonEditPrefixWords[-1]] #the last one
                        
                        wordCleanTest = nonEditPrefixWords.subgraph([0,k-1],lm)
                        wordCleanTest.extend(nonEditPrefixWords.subgraph([len(nonEditPrefixWords.word_graph)-1,len(nonEditPrefixWords.word_graph)],lm),lm)
                        
                        #print "word clean test"
                        #print wordCleanTest
                        #TODO this is where we need the graph methods, as
                        #we can quickly compute these that have already been computed
                        #wordWMLBoost = lm.logprob_weighted_by_inverse_unigram_logprob(wordCleanTest) - originalWordWML
                        #wordProbBoost = lm.logprob_weighted_by_sequence_length(wordCleanTest) - originalWordLogProb
                        wordWMLBoost = wordCleanTest.logprob_weighted_by_inverse_unigram_logprob() - originalWordWML
                        wordProbBoost = wordCleanTest.logprob_weighted_by_sequence_length() - originalWordLogProb 
                        
                        
                        if not j == start: worddrop = wordWMLBoost - wordprevious  #or do we do this without the edit words i.e. previouswordngram = nonEditPrefixWords[-lm.order:-1]; previous= sum(lm.tokens_logprob(previouswordngram,lm.order))
                        else: worddrop = 0
                        wordprevious = wordWMLBoost
                        
                        #wordKL = lm.KL_divergence_continuation_fast(rmwordngram[1:],rpwordngram[1:]) #predictibility of next word
                        #may have to leave this unfort..
                        #TODO speed up and put back in
                        #wordKL = 0.0
                        
                        referenceFile.write(reference+","+str(i-delta)+","+str(j-delta)+",")
                        if len(repairOnsetNgram) < 3: test = ["null"] + list(repairOnsetNgram)
                        else: test = list(repairOnsetNgram)
                        referenceFile.write(str(test)[1:-1].replace("'","").replace(" ","")+",") #the words, only bigram context?
                        referenceFile.write(str(test)[1:-1].replace("'","").replace(" ","")+",")
    
                        dataFile.write(str(confidence) + "," + str(wordlogprob)+","+str(wordWML)+","+str(wordRMlogprob)+","+str(wordRMWML)+\
                                   ","+str(wordLocalProbBoost)+","+str(wordLocalWMLBoost)+","+str(wordProbBoost)+","+str(wordWMLBoost)+","+str(worddrop)+","+\
                                str(wordEntropy) + "," + str(wordLocalEntropyReduce)+","+str(wordRM0Entropy)+",")
                    
                        #k = j
                        #other features
                        #getting embedded features- we want the fact that there is the rps of another
                        #repair onset here, we can't deal with more than one easily
                        #we can deal with chains, not deletes in the reparandum or (easily) with nests in repair
                        extra = ""
                        rps = 1
                        a = k
                        while rps < window: #only searches back in the local context of the window, could use more
                            if not "<i" in predictedTags[a] or "<e" in predictedTags[a]:
                                if "<rps" in predictedTags[a]: extra = "EMBEDDEDRP," + extra
                                else: extra = "FLUENT," + extra
                                rps+=1
                            a=a-1
                        #logical features of identity between rm1 and rp1
                        rm3rp3 = False #just one for now
                        if repairOnsetNgram[-1] == rmwordngram[-1]: rm3rp3 = True
                        extra = str(rm3rp3).upper() + "," + extra
                        
                        dataFile.write(extra)
                        dataFile.write(editString+","+str(reparandumLength)+","+ str(len(nonEditPrefixWords.word_graph)-delta)+ ",")
                        
                    
                        rmPOSngram = nonEditPrefixPOS.word_graph[(k+p)-pos_model.order:(k+p)]
                        rpPOSngram = nonEditPrefixPOS.word_graph[(k+p)-pos_model.order:(k+p)-1] + [repairOnsetPOSngram[-1]]
                        #print "rpPOSngram"
                        #print rpPOSngram
                        referenceFile.write(str(repairOnsetPOSngram)[1:-1].replace("'","").replace(" ","")+",")
                        referenceFile.write(str(rmPOSngram)[1:-1].replace("'","").replace(" ","")+"\n") #end of reference file stuff

                        POSlogprob = pos_model.logprob_weighted_by_sequence_length(rpPOSngram)
                        POSWML = pos_model.logprob_weighted_by_inverse_unigram_logprob(rpPOSngram)
                        POSRMlogprob = pos_model.logprob_weighted_by_sequence_length(rmPOSngram)
                        POSRMWML = pos_model.logprob_weighted_by_inverse_unigram_logprob(rmPOSngram)
                        
                        POSLocalWMLBoost = POSWML - originalLocalPOSWML
                        POSLocalProbBoost = POSlogprob - originalLocalPOSLogProb
                        
                        POSEntropy = pos_model.entropy_continuation_very_fast(rpPOSngram[-posdelta:],pos_model.order) #clean
                        POSLocalEntropyReduce = POSEntropy - originalPOSEntropy
                        POSRM0Entropy = pos_model.entropy_continuation_very_fast(rmPOSngram[-posdelta:],pos_model.order)#uncleaned bigram around the rm0 boundary- should be high
                        #POSCleanTest = nonEditPrefixPOS[:k-1] + [nonEditPrefixPOS[-1]]
                        POSCleanTest = nonEditPrefixPOS.subgraph([0,(k+p)-1],pos_model)
                        POSCleanTest.extend(nonEditPrefixPOS.subgraph([len(nonEditPrefixPOS.word_graph)-1,len(nonEditPrefixPOS.word_graph)],pos_model),pos_model)
                        
                        
                        #print "POSCleanTest"
                        #print POSCleanTest
                        #POSWMLBoost = pos_model.logprob_weighted_by_inverse_unigram_logprob(POSCleanTest) - originalPOSWML
                        #POSProbBoost = pos_model.logprob_weighted_by_sequence_length(POSCleanTest) - originalPOSLogProb
                       
                        POSWMLBoost = POSCleanTest.logprob_weighted_by_inverse_unigram_logprob() - originalPOSWML
                        POSProbBoost = POSCleanTest.logprob_weighted_by_sequence_length() - originalPOSLogProb
                        
                        if not j==start: POSdrop = POSWMLBoost - POSprevious #this should be positive if it's getting better or do we do this without the edit words i.e. previouswordngram = nonEditPrefixWords[-lm.order:-1]; previous= sum(lm.tokens_logprob(previouswordngram,lm.order))
                        else: POSdrop = 0
                        POSprevious = POSWMLBoost
                        POSKL = pos_model.KL_divergence_continuation_fast(rmPOSngram[-posdelta:],rpPOSngram[-posdelta:])
                        dataFile.write(str(POSlogprob)+","+str(POSWML)+","+str(POSRMlogprob)+","+str(POSRMWML)+","+str(POSKL)+","+\
                                   str(POSLocalProbBoost)+","+str(POSLocalWMLBoost)+","+str(POSProbBoost)+","+str(POSWMLBoost)+","+str(POSdrop)+","\
                                    +str(POSEntropy) + "," + str(POSLocalEntropyReduce) + ","+ str(POSRM0Entropy) + ",")
                        #logical features of identity between rm1 and rp1 for POS
                        extra = ""
                        rm3rp3 = False #just one for now, could add a partial word/prefix feature too
                        if repairOnsetPOSngram[-1] == rmPOSngram[-1]: rm3rp3 = True
                        extra+=str(rm3rp3).upper() + ","
                        dataFile.write(extra)
                        
                        #NB recording the word and POS values
                        #for word in repairOnsetNgram:
                        #    dataFile.write(word+",") #NOTE using the actual values themselves
                        #for word in rmwordngram:
                        #    dataFile.write(word+",") #NOTE using the actual values themselves
                        #for word in repairOnsetPOSngram:
                        #    dataFile.write(word+",") #NOTE using the actual values themselves
                        #for word in rmPOSngram:
                        #    dataFile.write(word+",") #NOTE using the actual values themselves
                        
                        
                        
                        dataFile.write(cat+"\n")
                        reparandumLength+=1 #we work with a non-edit context
                        if reparandumLength > 6: break #only up to lenth 8, experiment, if more there's more data for rejection, never bad
                        k=-reparandumLength
                    j=j-1
                
                #increment #DO WE NEED THIS OR NOT?
                #self.increment_prefix(lm, predictedTags, goldTags, 6, ["<i","<i><e>","<e>","<rps","<rps:","<rpsdel"])
                #print predictedTags
                #print goldTags
                #raw_input()
                i+=1 #word index (2 less than this actually)
                editCount+=1 #overall counter increments
                repairCount+=1 #overall repair increments as we've got one
                #if editCount > 100: #just for testing
                #    end = True
                #    break
                
                #if editCount == 100000 or editCount == 200000 or editCount == 300000 or editCount == 400000 or editCount == 500000:
                #    print str(float(editCount)/float(100000)) + "* 100K complete!" #TODO need a timing thing
            self.reset_utterance(lm, predictedTags, tags, ["<i","<i><e>","<e>","<rps","<rps:","<rpsdel"],evaluation=evaluation)
        #TODO we are missing some (i.e. edit terms which are in fact repairs, rare but they do exist)
        #if "ppat" in corpusName.lower(): fns+=24; relaxedfns+=24 #being mean here, also 4 badly formed (last index 939, so 940, only 916)
        #   #being mean here for switchboard
        #elif "partial" in corpusName.lower(): fns+=10; relaxedfns+=10 #doesn't make big difference 0.007 ish
        self.fileIndex = editCount
        self.repairIndex = repairCount
        if evaluation == True:
            print self.accuracy_evaluation()
        print "DEBUG number of double RPS"
        print len(multipleRPS)
        #for r in multipleRPS:
        #    raw_input(r)
        #    print "\n"
        #raw_input()
        return