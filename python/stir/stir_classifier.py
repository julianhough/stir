from collections import defaultdict
import re
import sys
import os
sys.path.append(os.path.join(os.getcwd(),".."))
    
class stir_classifier():
    
    def eval_init(self, lm, tags):
        """Initiliazing a simple evaluation to run on the fly for each component during training"""
        self.overall_accuracy_results = defaultdict(list) #just triples of tps,fps,fns for each tag
        self.relaxed_accuracy_results = defaultdict(list)
        for tag in tags:
            self.overall_accuracy_results[tag] = [0,0,0]
	self.relaxed_accuracy_results['<rps'] = [0,0,0]
        delta = lm.order - 1
        self.currentSequencePredicted = delta * [""] #nb dependent on lang smodel!
        self.currentSequenceGold = delta * [""]
        self.prefix_store = [] #simply a list of prefix tuples (gold,detected) for this utt
        self.detectedRepairs = []
        self.currentTranscript = "" #string var for keeping track of current transcript 
        return
        
    def inherit_eval_values(self,lm,other_exp):
        self.overall_accuracy_results = other_exp.overall_accuracy_results #just triples of tps,fps,fns for each one
        self.relaxed_accuracy_results = other_exp.relaxed_accuracy_results
        #reset
        delta = lm.order - 1
        self.currentSequencePredicted = delta * [""] #nb dependent on lang model!
        self.currentSequenceGold = delta * [""]
        self.prefix_store = []
        self.detectedRepairs = []
        self.currentTranscript = other_exp.currentTranscript
        return
    
    def p_r_f(self,tps,fps,fns):
        if tps == 0:
            return (0,0,0)
        p = float(tps)/float(tps+fps)
        r = float(tps)/float(tps+fns)
        f = float(2*(p*r))/float(p+r)
        return (p,r,f)
    
    def accuracy_evaluation(self,orclasses=None):
        """returns the p,r,f for overall tags"""
        result = "class,p,r,f\n"
        if not orclasses == None:
            ordict = defaultdict(list)
            for orclass in orclasses:
                ordict[orclass] = [0,0,0]
            
        for tag in self.overall_accuracy_results.keys():
            # print tag
            r = self.overall_accuracy_results[tag]
            # print r
            if not orclasses == None:
                for orclass in ordict.keys():
                    if tag in orclass.split("."):
                        ordict[orclass][0]+=r[0]
                        ordict[orclass][1]+=r[1]
                        ordict[orclass][2]+=r[2]
            myeval = self.p_r_f(r[0],r[1],r[2])
            result+=tag+","+str(myeval[0])+","+str(myeval[1])+","+str(myeval[2])+"\n"
        if not orclasses is None:
            for tag in ordict.keys():
                r = ordict[tag]
                myeval = self.p_r_f(r[0],r[1],r[2])
                self.overall_accuracy_results[tag] = r #stipulate it
                result+=tag+","+str(myeval[0])+","+str(myeval[1])+","+str(myeval[2])+"\n"
        #relaxedresults
        if not self.relaxed_accuracy_results.get("<rps") is None:
            r = self.relaxed_accuracy_results["<rps"]
            #rEval = self.p_r_f(r[0],r[1],r[2])
            self.relaxed_eval = self.p_r_f(r[0],r[1],r[2])
            rEval = self.relaxed_eval
            print "per-utterance <rps = " + str(rEval[0]) + "," + str(rEval[1]) + "," + str(rEval[2])
        print result
    
    def reset_utterance(self, lm, listPredict, listGold, tags, evaluation=False, speaker=None):
        """Reset the prefix store, and optional accuracy totaller called at end of an utterance"""
        assert len(listPredict) == len(listGold)
        delta = lm.order -1
        if evaluation == True: # only p_r_f if specified
            error = False
            relaxedGold = 0
            relaxedHyp = 0
            #at the moment leaving out rpn types as source of error
            for i in range(delta,len(listGold)):
                for tag in tags:
                    if tag in listGold[i]:
                        if tag == "<rps": relaxedGold+=1
                        if tag in listPredict[i]:
                            if tag == "<rps": relaxedHyp+=1
                            self.overall_accuracy_results[tag][0]+=1 #TP
                        else:
                            self.overall_accuracy_results[tag][2]+=1 #FN
                            #if not "rpn" in tag: error = True 
                    elif tag in listPredict[i]:
                        if tag == "<rps": relaxedHyp+=1
                        self.overall_accuracy_results[tag][1]+=1 #FP
                        if not "rpn" in tag: error = True
            if not self.relaxed_accuracy_results.get("<rps") is None:
                self.relaxed_accuracy_results["<rps"][0]+=min(relaxedHyp,relaxedGold)
                self.relaxed_accuracy_results["<rps"][1]+=max(0,relaxedHyp-relaxedGold)
                self.relaxed_accuracy_results["<rps"][2]+=max(0,relaxedGold-relaxedHyp)
        #reset these
        self.detectedRepairs = []
        self.prefix_store = []
        self.currentSequencePredicted = delta * [""] 
        self.currentSequenceGold = delta * [""]
        return
        
    def increment_prediction_prefix_to_file(self, i, predictedTags, words, pos, delta, tagFile, reference):
        """Writes the current predictions for this prefix to file"""
        self.currentSequencePredicted = predictedTags
        tags = []
        for i, word, p_tag in zip(range(0, len(words)), words, predictedTags):
            tag = ""
            for tag_type in ["rms","rm","i","rps","rp","rpnsub","rpndel","rpsdel","rpnrep", "rpn"]:
                markup = re.findall("<{}\:[0-9]*>".format(tag_type), p_tag, re.S) + \
                re.findall("<{}\:FP[0-9]*>".format(tag_type), p_tag, re.S)
                if markup:
                    if tag_type == "i" and not "<e>" in p_tag:
                        tag+="<e/>"
                    for m in markup:
                        ID = m[m.find(":")+1:-1]
                        if False:
                            if tag_type == "rpn" and ("rpndel" not in tags[-1]): # no classification yet
                                repair_words = [word]
                                reparandum_words = []
                                for j in range(i-1,-1,-1):
                                    if "<rp:" + ID in predictedTags[j] or \
                                            "<rps:" + ID in predictedTags[j]:
                                        repair_words.insert(0, words[j])
                                    if "<rm:" + ID in predictedTags[j]:
                                        reparandum_words.insert(0, words[j])
                                    if "<rms:" + ID in predictedTags[j]:
                                        reparandum_words.insert(0, words[j])
                                        break
                                if repair_words == reparandum_words:
                                    tag_type = "rpnrep"
                                else:
                                    tag_type = "rpnsub"
                        if tag_type == "rpsdel":
                            tag+='<{} id="{}"/>'.format("rps",ID)+'<{} id="{}"/>'.format("rpndel",ID)
                        else:
                            tag+='<{} id="{}"/>'.format(tag_type,ID)
            
            if "<e>" in p_tag:
                tag+="<e/>"
            tags.append(tag)
        
        
        #tagFile.write("Time: "+str((i-delta)+1)+"\n")
        started = False
        for p in range(delta,len(tags)):
            tag = tags[p]
            if tag == "":
                tag = "<f/>"
            ref = ""
            if started == False:
                ref = reference
                started = True
            tagFile.write(ref + "\t" + str((p-delta)+1)+"\t"+words[p]+ "\t" + pos[p] + "\t" + tag+"\n")
            
        
    def increment_prefix(self, lm, listPredict, listGold):
        """ provides reset_utterance for a given strings of predictions, that must be of the same length as the gold standard list
            only configured for the given class we're interested in. d= distance back from current prefix"""
        assert len(listPredict) == len(listGold)
        #print listPredict
        #print listGold

        #add the new gold and current predicted
        #print listGold[-1]
        repairlist = re.findall(r"<rps\:[0-9]*>",listGold[-1]) + re.findall(r"<rpsdel\:[0-9]*>",listGold[-1])
        #print listGold
        #print repairlist
        assert(len(repairlist))<=1,repairlist
        currentSequenceGold = list(self.currentSequenceGold) #YES, fine
        currentSequenceGold.append("")
        if "<rps" in listGold[-1]:
            goldTag = "<rps"
            if "<rpsdel" in listGold[-1]: goldTag+="del"
            repairID = repairlist[0][repairlist[0].rfind(":"):]
            currentSequenceGold[-1]+=goldTag+repairID 
            #print "gold"
            #print repairID
            for i in range(len(listGold)-2,-1,-1):
                #print listGold[i]
                if "<rm" + repairID in listGold[i]:
                    if not "<rm" in currentSequenceGold[i]: currentSequenceGold[i]+="<rm" + repairID
                elif "<rms" + repairID in listGold[i]:
                    if not "<rm" in currentSequenceGold[i]: currentSequenceGold[i]+="<rm" + repairID; break
                #raw_input()
        elif "<i><e>" in listGold[-1]:
            currentSequenceGold[-1]+="<i><e>"
        elif "<rp:" in listGold[-1]:
            currentSequenceGold[-1]+=listGold[-1]
        elif "<rpn" in listGold[-1]:
            currentSequenceGold[-1]+=listGold[-1]
        #This should be the predicted tags up to length of currentSequenceGold
        #the Gold tags are never revoked but the below could be
        #could do clever backwards search with diff. or- just copy from the original predicted list
        #
        #currentSequencePredicted = list(self.currentSequencePredicted)
        #currentSequencePredicted.append("")
        currentSequencePredicted = [] #reset each time, bit expensive
        for i in range(0,len(listPredict)):
            currentSequencePredicted.append("")
            if "<rps" in listPredict[i]:
                currentSequencePredicted[-1]+=listPredict[i]
                if "<rps" in listGold[i]:
                    goldList = re.findall(r"<rps\:[0-9]*>",listGold[i]) + re.findall(r"<rpsdel\:[0-9]*>",listGold[i])
                    repairID = goldList[0][goldList[0].rfind(":"):]
                    if not repairID in self.detectedRepairs:
                        for j in range(i,1,-1):
                            if "<rms" + repairID in listPredict[j] and "<rms" + repairID in listGold[j]:
                                self.detectedRepairs.append(repairID)
                                #self.time_to_detection["<rps"].append(len(listPredict)-i) #so will give 1 over the difference
                                #self.time_to_detection["<rms"].append(len(listPredict)-j) #so 10 tags and detected at 9 (i.e. 8) 
                                break
            elif "<rm" in listPredict[i]: 
                currentSequencePredicted[i]+=listPredict[i]
            elif "<i><e>" in listPredict[i]:
                currentSequencePredicted[i]+="<i><e>"
            elif "<rp:" in listPredict[i]:
                currentSequencePredicted[i]+=listPredict[i]
            elif "<rpn" in listPredict[i]:
                currentSequencePredicted[i]+=listPredict[i]
        #self.editOverhead(list(currentSequencePredicted),list(currentSequenceGold)) #see how they're different first
        self.currentSequencePredicted = currentSequencePredicted
        self.prefix_store.append((list(currentSequenceGold),list(currentSequencePredicted)))
        self.currentSequenceGold = currentSequenceGold
        return
    
    def convert_to_old_stir_format(self, sequence):
        tags = []
        for i in range(0,len(sequence)):
            tag = ""
            if "<r" in sequence[i] or "<i" in sequence[i]:
                tag = sequence[i].replace('/>','>').replace(' id=',':').replace('"','')
            if tag == "" and "<e/>" in sequence[i]:
                tag = "<e/>"
            tags.append(tag)
        return tags
    
    def convertToRepairTags(self,sequence,lm_order):
        """Convert any sequence to uniform tag standard which other detectors can ensemble with.
        In theory this should simply be a change in the ID number, though even that is not needed."""
        tags = []
        tagID = -(lm_order-1) #starts back from prefix
        idmap = dict() #map from the ID given in the corpus to the new simpler ID here
        #print sequence
        for i in range(0,len(sequence)):
            tag = sequence[i]
            final_tag = ""
            if tag == "": #fluent word
                final_tag = "<f/>"
            elif "<i:" in tag: #interregnum
                final_tag = "<e/>" #NB need to establish interregnum after this
            elif "<e>" in tag:  # normal edit term
                final_tag = "<e/>" #NB ok to leave this for an interrengum?
            elif "<rp" in tag.lower(): #This is a repair onset, repair mid or repair end, or combo of these
                
                if "<rps" in tag.lower():
                    #print sequence
                    #Get ID: 
                    repairs = re.findall('<rps\:f?p?[0-9]+>', tag.lower(), re.S) +\
                    re.findall('<rpsdel\:f?p?[0-9]+>', tag.lower(), re.S)
                    assert(len(repairs)<2),sequence
                    #print "repairs",repairs
                    repairOnset = repairs[0][repairs[0].find(":")+1:-1]
                    idmap[repairOnset] = tagID
                    final_tag+='<rps id="{}"/>'.format(tagID) 
                    
                    j = i-1
                    reparandumEntered = False
                    while j>=0:
                        #print repairOnset
                        #print sequence[j].lower()
                        reparandumlist = re.findall('<rms?\:f?p?[0-9]+>', sequence[j].lower(), re.S)
                        interregnumlist = re.findall('<i\:f?p?[0-9]+>', sequence[j].lower(), re.S)
                        #print "<rms:{}>".format(repairOnset)
                        #raw_input()
                        if "<rms:{}>".format(repairOnset) in reparandumlist:
                            tags[j] = tags[j].replace("<f/>","")
                            tags[j]+='<rms id="{}"/>'.format(tagID)
                            break #found the reparandum onset
                        elif "<rm:{}>".format(repairOnset) in reparandumlist:
                            tags[j] = tags[j].replace("<f/>","")
                            tags[j]+='<rm id="{}"/>'.format(tagID)
                            reparandumEntered = True
                        if "<i:{}>".format(repairOnset) in interregnumlist and reparandumEntered==False:
                            #ensuring no dodgy interregna that overlap with rm's
                            tags[j]+='<i id="{}"/>'.format(tagID) 
                        j-=1
                if "<rpn" in tag.lower():
                    repairExtentList = re.findall('<rpn\:f?p?[0-9]+>', tag.lower(), re.S) + \
                    re.findall('<rpnsub\:f?p?[0-9]+>', tag.lower(), re.S) + re.findall('<rpnrep\:f?p?[0-9]+>', tag.lower(), re.S)  
                    repairExtentListDelete = re.findall('<rpndel\:f?p?[0-9]+>', tag.lower(), re.S) \
                    ## re.findall('<rpnrep\:[0-9]+>', tag, re.S) + re.findall('<rpnsub\:[0-9]+>', tag, re.S)
                    for repairEnd in repairExtentList:
                        repairEndID = repairEnd[repairEnd.find(":")+1:-1]
                        if idmap.get(repairEndID) == None:
                            print "NO REPAIR start detected",repairEndID
                        else:
                            final_tag+='<rpn id="{}"/>'.format(idmap[repairEndID])
                    for repairEnd in repairExtentListDelete:
                        repairEndID = repairEnd[repairEnd.find(":")+1:-1]
                        if idmap.get(repairEndID) == None:
                            print "NO REPAIR start detected",repairEndID
                        else:
                            final_tag+='<rpnDel id="{}"/>'.format(idmap[repairEndID])
                if "<rp" in tag.lower():
                    repairMidList = re.findall('<rp\:f?p?[0-9]+>', tag.lower(), re.S) 
                    for repairMid in repairMidList:
                        repairMidID = repairMid[repairMid.find(":")+1:-1]
                        if idmap.get(repairMidID) == None:
                            print "NO REPAIR start detected",repairMidID
                        else:
                            final_tag+='<rp id="{}"/>'.format(idmap[repairMidID])
            elif "<rm" in tag.lower():
                final_tag = "<f/>" #NB only fluent if repair ID has been not been passed yet, will be changed.
            else:
                final_tag = "<f/>"
                print tag
            tagID+=1
            assert(not final_tag == ""),tags
            tags.append(final_tag)
        return tags
    
    def printPrefixes(self,lm,words,pos,reference,speaker):
        prefix_list = ""
        delta = lm.order - 1
        assert(len(words)-delta==len(self.prefix_store))
        print reference,speaker
        for p in range(0,len(self.prefix_store)):
            prefix = self.prefix_store[p]
            #print prefix
            prefix_list+="\t".join(["BOS",reference,speaker])+"\n"
            gold,predicted = self.convertToRepairTags(prefix[0],lm.order),self.convertToRepairTags(prefix[1],lm.order)
            assert len(gold)==len(predicted)
            #assert len(gold)==len(words)
            current = ""
            for i in range(delta,len(gold)):
                #print words[i],pos[i],gold[i],predicted[i]
                myword,mypos,mygold,mypredicted = words[i][words[i].rfind('>')+1:],pos[i][pos[i].rfind('>')+1:],gold[i],predicted[i]
                #mypredicted = self.convertToRepairTag(mypredicted)
                #mygold = self.convertToRepairTag(mygold)
                current+="\t".join([myword,mypos,mygold,mypredicted])+"\n"
                
            prefix_list+=current
            if p == len(self.prefix_store)-1:
                prefix_list+="\t".join(["BOS",reference,speaker])+"\n"+current+"EOS\tO\tO\n" #TODO artifact that we also compute incrementality at length+1
            prefix_list+="\n"
        return prefix_list
    
    def convertDelete(self,words):
        """ad hoc method to turn rps rpndels into just one rpsdel.
        TODO hack for now, may get rid of multiple embedded deletes.
        note we also check for interregna
        NB for use before!! <s> <s> is added before"""
        bad = 0
        for i in range(0,len(words)):
            if "<i:" in words[i]: #TODO this is because Ngram corpus is not giving us these- worse than this it's not giving all interreg
                interreg = re.findall("<i\:[0-9]*>", words[i], re.S)
                ID = interreg[0][2:]
                #print ID
                j = i; found = False
                if "<r" in words[i]:
                    #print "Interregnum in wrong place",words
                    bad+=1
                while j > -1: #-1 not delta!!!
                    if "<rms" + ID in words[j]: found = True; break
                    j=j-1
                if found == False: words[i] = words[i].replace("<i" + ID,"<e>") #get rid of false pos interregna
            elif "<e>" in words[i] and not "<i" in words[i]: #check for missed INTERREG
                if i < (len(words)-1) and "<rps" in words[i+1]:
                    ID = re.findall("<rps\:[0-9]*>", words[i+1], re.S)[0][4:]
                    j = i; found = False
                    while j > -1: #not delta 
                        if "<e>" in words[j] and not ("<i"+ID) in words[j]:
                            words[j] = "<i"+ID + words[j]
                            #print ID
                            words[j] = words[j].replace("<e>","")
                        if "<rms" + ID in words[j]: found = True; break
                        j=j-1
            elif "<rps" in words[i]:
                repairs = re.findall("<rps\:[0-9]*>", words[i], re.S)
                if len(repairs)>1:
                    print "embedded repair"
                    dels = re.findall('<rpndel\:[0-9]*>', words[i], re.S)
                    #assert len(dels) == 1,str(words)
                    if len(dels)>1: print "embeddeddelete!!" + str(words)
                #ID = dels[0][8:-1]
                #before = "<rps:" + str(ID)+">"
                #assert before in words[i],str(words)+str(before)
                #after = "<rpsdel:" + str(ID)+">"
                #words[i] = words[i].replace(before,after) #in fact don't get rid of the rpndel's as we can then distinguish between the onsets in general and the classification
                #j = i; found = False
                #while j > 1:
                #    if "<rms:" + str(ID) in words[j]: found = True; break        
                #    j=j-1
                #if found == False: words[i] = words[i].replace("<i" + ID,"<e>") #get rid of false pos interregna
        #return bad #TODO remove, just for test
        #if bad>0: return False
        return words

if __name__ == '__main__':
    
    test_seq = ["","" ]
    for x in "<rms:44474>i <rm:44474>wanted <rm:44474>a <rm:44474>truck <rm:44474>that <rm:44474><rms:42954>i \
    <rps:44474><rps:42954><rpnrep:42954>i <rp:44474>wanted <rp:44474>something <rp:44474>that <rpnsub:44474>i \
    could throw people in the back too".split():
        if "<" in x:
            test_seq.append(x[:x.rfind(">")+1])
        else:
            test_seq.append("")
    print test_seq
    s = stir_classifier()
    print s.convertToRepairTags(test_seq, 3)
