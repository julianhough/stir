""" STIR (Strongly Incremental Repair detection, Hough and Purver EMNLP 2014)
Training and Testing on corpora offline.
Fairly cumbersome way of saving files and using those results.
Newer more elegant code in progress as of 13.04.2015
"""

import argparse
import subprocess
import os
import time
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/..")

from corpus.load import lm_corpus_splits, get_tag_data_from_corpus_file
from corpus.create_folds import swbd_folds_disfluency_corpus
from language_model.ngram_language_model import KneserNeySmoothingModel
from edit_term import edit_term
from repair_onset import repair_onset
from reparandum_onset import reparandum_onset
from repair_end import repair_end
from final_classifier import final_classifier
from util import create_final_evaluation_file_from_stir_output

THIS_DIR = os.path.dirname(os.path.realpath(__file__))
SWBD_DIR = THIS_DIR + '/../data/switchboard'


def process_arguments():
    """ Process arguments from command line.
    """
    parser = argparse.ArgumentParser(description="This script trains \
        STIR classifiers and saves them to disk ,and tests them.\
        By changing the detection mode (-mode) argument from default\
        train_test to test one can just test its performance on the default\
        Switchboard heldout and test data.")
    parser.add_argument('-mode', '--detection_mode', type=str,
                        help='Can be train_test (trains the models and tests) \
                        or just test',
                        default='test')
    parser.add_argument('-lm', type=str,
                        help='write the language model to OUTPUT \
                        (must be a file) instead of stdout',
                        default=None)
    parser.add_argument('-o', '--order', type=int,
                        help='the order of the generated model. \
                        It defaults to 3 (trigrams).',
                        default=3)
    parser.add_argument('-d', '--discount', type=float,
                        help='the discount for Kneyser-Ney smoothing. \
                        0.7-0.9 generally good.',
                        default=0.9)
    parser.add_argument('-i', '--input', type=str,
                        help='training corpus for the language model. \
                        These are REF.text files with words then POS tags \
                        on alternate lines.',
                        default=os.sep.join([SWBD_DIR,
                                             'lm_corpora',
                                             'swbd_train_clean.text']))
    parser.add_argument('-i2', '--secondcorpus',
                        help='second additional training corpus for the lm.',
                        default=None)
    parser.add_argument('-ho', '--heldout', type=float,
                        help='the percentage of heldout dialogues from the \
                        main training corpus to derive unknown word counts.',
                        default=10.0)
    parser.add_argument('-e', '--editinput', type=str,
                        help='edit term lm training corpus',
                        default=os.sep.join([SWBD_DIR,
                                             'lm_corpora',
                                             'swbd_train_edit.text']))
    parser.add_argument('-eh', '--editheldout', type=float,
                        help='the percentage of heldout dialogues from the \
                        main training edit term corpus to derive \
                        unknown word counts.',
                        default=10.0)
    parser.add_argument('-dtc', '--training_corpus', type=str,
                        help='disfluency detection training corpus, \
                        default the swbd training set wit partial words',
                        default=os.sep.join([
                            SWBD_DIR,
                            'disfluency_detection',
                            'swbd_disf_train_1_partial_data.csv']))
    parser.add_argument('-dhc', '--heldout_corpus', type=str,
                        help='disfluency detection heldout corpus, \
                        default just the swbd heldout \
                        set with partial words',
                        default=os.sep.join([
                            SWBD_DIR,
                            'disfluency_detection',
                            'swbd_disf_heldout_partial_data.csv']))
    parser.add_argument('-dtestc', '--test_corpora', type=str,
                        help='list of disfluency detection test corpora, \
                        default the swbd test set with partial words',
                        nargs="*",
                        default=[os.sep.join([
                            SWBD_DIR,
                            'disfluency_detection',
                            'swbd_disf_test_partial_data.csv'])])
    parser.add_argument('-s', '--smoothing', type=str,
                        help='applies the specified smoothing method.',
                        default='kneser-ney')
    parser.add_argument('-c', '--cutoff', type=int,
                        help='Ngrams with count equal to or smaller \
                        than CUTOFF are not recorded',
                        default=0)
    parser.add_argument('-pos', '--pos_tagged', type=bool,
                        help='Whether pos tagged or not',
                        default=True)
    parser.add_argument('-part', '--partial_words', type=bool,
                        help='Whether it processes partial words (true) \
                        or removes them (false)',
                        default=True)
    parser.add_argument('-xfold', '--cross_fold_lm', type=bool,
                        help='Whether the lm is trained using x-fold methods.',
                        default=False)
    parser.add_argument('-exp', '--experiment_directory', type=str,
                        help='The location of the experiment directory',
                        default=os.sep.join([THIS_DIR, '..', 'experiments']))
    parser.add_argument('-ec', '--edit_term_classifier', type=str,
                        help='The location of a pre-trained edit term\
                         classifier to load',
                        default=None)
    parser.add_argument('-m', '--saved_model', type=str,
                        help='The location of a pre-trained model with \
                        all 4 classifiers in sub-directories \
                        e, rps, rms and rpn. Default is the one \
                        saved model released with STIR using \
                        POS tags and partial words',
                        default=None
                        )
    parser.add_argument('-rpsc', '--repair_onset_classifier', type=str,
                        help='The location of a pre-trained \
                        repair onset classifier',
                        default=None)
    parser.add_argument('-rmsc', '--reparandum_onset_classifier', type=str,
                        help='The location of a pre-trained \
                        reparandum onset classifier',
                        default=None)
    parser.add_argument('-rpnc', '--repair_end_classifier', type=str,
                        help='The location of a pre-trained \
                        repair end classifier',
                        default=None)
    parser.add_argument('-eon', '--ed_on', type=bool,
                        help='Whether to keep the \
                        edit term classifier on or not',
                        default=True)
    parser.add_argument('-rpson', '--rps_on', type=bool,
                        help='Whether to keep the \
                        repair onset classifier on or not',
                        default=True)
    parser.add_argument('-rmson', '--rms_on', type=bool,
                        help='Whether to keep the \
                        reparandum onset classifier on or not',
                        default=True)
    parser.add_argument('-rpnon', '--rpn_on', type=bool,
                        help='Whether to keep the \
                        repair end classifier on or not',
                        default=True)
    parser.add_argument('-v', '--verbose',
                        help='When passed this option the script tries \
                        to estimate the time it will take to \
                        complete training',
                        default=True)
    args = parser.parse_args()

    if not args.partial_words:
        print "no partial words, adjusting corpora"
        partial_string = ''
        args.training_corpus = os.sep.join([
            SWBD_DIR,
            'disfluency_detection',
            'swbd_disf_train_1' + partial_string + '_data.csv'])
        args.heldout_corpus = os.sep.join([
            SWBD_DIR,
            'disfluency_detection',
            'swbd_disf_heldout' + partial_string + '_data.csv'])
        args.test_corpora = [os.sep.join([
            SWBD_DIR,
            'disfluency_detection',
            'swbd_disf_test' + partial_string + '_data.csv'])]
    if args.detection_mode == "test":
        if args.cross_fold_lm:
            print "switching to non-cross fold method for test"
            args.cross_fold_lm = False

        if not args.saved_model:
            print "No saved model, using default with partial words and pos"
            args.saved_model = os.sep.join([THIS_DIR, '..', 'saved_models',
                                            'words_pos_partial'])
        if not args.edit_term_classifier:
            print "adding edit term classifier from saved model"
            args.edit_term_classifier = (os.sep.join([
                                            args.saved_model,
                                            "classifiers",
                                            "e",
                                            "e_3",
                                            "e_3.model"]),
                                         os.sep.join([
                                             args.saved_model,
                                             "classifiers",
                                             "e",
                                             "e_3",
                                             "data.csv"])
                                         )
        if not args.repair_onset_classifier:
            print "adding repair start classifier from saved model"
            args.repair_onset_classifier = (os.sep.join([
                                               args.saved_model,
                                               "classifiers",
                                               "rps",
                                               "rps_3_8",
                                               "rps_3_8.model"]),
                                            os.sep.join([
                                                args.saved_model,
                                                "classifiers",
                                                "rps",
                                                "rps_3_8",
                                                "data.csv"]))
        if not args.reparandum_onset_classifier:
            print "adding reparandum start classifier from saved model"
            args.reparandum_onset_classifier = (os.sep.join([
                                                    args.saved_model,
                                                    "classifiers",
                                                    "rms",
                                                    "rms_3_8_4",
                                                    "rms_3_8_4.model"]),
                                                os.sep.join([
                                                    args.saved_model,
                                                    "classifiers",
                                                    "rms",
                                                    "rms_3_8_4",
                                                    "data.csv"]))
        if not args.repair_end_classifier:
            print "adding repair end classifier from saved model"
            args.repair_end_classifier = (os.sep.join([
                                                args.saved_model,
                                                "classifiers",
                                                "rpn",
                                                "rpn_3_8_4_2_d3",
                                                "rpn_3_8_4_2_d3.model"]),
                                          os.sep.join([
                                                args.saved_model,
                                                "classifiers",
                                                "rpn",
                                                "rpn_3_8_4_2_d3",
                                                "data.csv"]))
    if args.detection_mode == "train_test":
        if not args.cross_fold_lm:
            print "switching to cross fold method for training"
            args.cross_fold_lm = True

    return args


def train_test(args):
    """Main method for training STIR classifiers and then applying
    the trained models on unseen data.
    Takes params from the args as in the process_arguments() method.
    """
    big_tick = time.clock()
    # set the ignore flags for the POS features
    ed_ignore = "None"
    rps_ignore = "None"
    rms_ignore = "None"
    rpn_ignore = "None"
    if not args.pos_tagged:
        ed_ignore = "9,10,11"
        rps_ignore = "14,15,16,17,18,19,20,21,22,23,24"
        rms_ignore = "20,21,22,23,24,25,26,27,28,29,30,31,32,33"
        rpn_ignore = "6,7,11,13,12,15,18,19,22,23"

    if args.partial_words:
        partial_string = "_partial"
    else:
        partial_string = ""

    # The name of the internal switchboard corpus we always train on
    training_corpus_name = args.training_corpus[
        args.training_corpus.rfind(os.sep)+1:args.training_corpus.rfind(".")]

    # Make directories
    if args.pos_tagged:
        posstring = "_POS"
    else:
        posstring = ""
    experiment_directory = args.experiment_directory + os.sep + \
        str(args.order) + "_" + str(3) +\
        "_words" + posstring + "_" + training_corpus_name
    try:
	print "Making", str(experiment_directory)
        os.mkdir(experiment_directory)
        print "made experiment directory", str(experiment_directory)
    except OSError:
        print "Problem creating Directory ", str(experiment_directory),
        "-- may already exist"

    results_directory = args.experiment_directory + os.sep + "results"
    try:
	print "Making", results_directory
        os.mkdir(results_directory)
        print "made results directory", results_directory
    except OSError:
        print "Problem creating Directory ", str(results_directory),
        "-- may already exist"

    # exp_time_date = str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
    # TODO need to mark the results file

    # training info summary
    print "smoothing = ", str(args.smoothing)
    print "pos-tagged = ", str(args.pos_tagged)
    print "partial words =", str(args.partial_words)
    print "detection mode = ", str(args.detection_mode)
    print "cross_fold_lm = ", str(args.cross_fold_lm)

    # open all the files and create strings from them if needs be
    if args.secondcorpus:
        print "training lm on second corpus ", str(args.secondcorpus)
        args.secondcorpus, args.pos_secondcorpus, _, _ = \
            lm_corpus_splits(args.secondcorpus, split=0.0, pos_tagged=True)
    else:
        args.pos_secondcorpus = None

    if args.editinput:
        print "training edit term file on",
        args.editinput, "with", args.editheldout, "% heldout"
        args.editinput, _, args.editheldout, _ = \
            lm_corpus_splits(args.editinput,
                             split=args.editheldout, pos_tagged=False)
    else:
        args.editheldout = None

    # divide the lm corpus into folds if folds is true
    if args.cross_fold_lm:
        f = open(args.input)
        args.config, args.folds = swbd_folds_disfluency_corpus(f,
                                                               num_files=496,
                                                               num_folds=10)
        f.close()
    else:
        # only one config, i.e. one training and one test (the heldout file)
        args.config = [()]

    print "training main lm on main file", str(args.input),
    "with", args.heldout, "% heldout"
    args.input, args.pos_corpus, args.heldout, args.pos_heldout_corpus = \
        lm_corpus_splits(args.input, split=args.heldout, pos_tagged=True)

    # Train the main language model now. Used in both x-fold
    # and fold methods for testing
    lm = KneserNeySmoothingModel(order=args.order,
                                 discount=args.discount,
                                 partial_words=args.partial_words,
                                 train_corpus=args.input,
                                 heldout_corpus=args.heldout,
                                 second_corpus=args.secondcorpus)
    POS_lm = KneserNeySmoothingModel(order=3,
                                     discount=0.9,
                                     partial_words=args.partial_words,
                                     train_corpus=args.pos_corpus,
                                     heldout_corpus=args.pos_heldout_corpus,
                                     second_corpus=args.secondcorpus)
    # can try 3-gram and 2-gram and different smoothing
    assert lm.train_length == POS_lm.train_length,\
        "WORD AND POS LENGTH MISMATCH!" +\
        "{} vs {}".format(lm.train_length, POS_lm.train_length)
    if args.editinput:
        print "Training edit term Language Model..."
        edit_lm = KneserNeySmoothingModel(train_corpus=args.editinput,
                                          heldout_corpus=args.editheldout,
                                          order=2,
                                          discount=0.9)  # non-partial word?

    # get the names of the corpora
    training_corpus_name = args.training_corpus[
        args.training_corpus.rfind(os.sep)+1:
        args.training_corpus.rfind('.csv')]
    heldout_corpus_name = args.heldout_corpus[
        args.heldout_corpus.rfind(os.sep)+1:
        args.heldout_corpus.rfind('.csv')]
    test_corpora_names = [x[x.rfind(os.sep)+1:x.rfind('.csv')]
                          for x in args.test_corpora]

    # get the corpora in 4-tuple form (IDs,seq,pos_seq,targets)
    print "loading corpora..."
    if "train" in args.detection_mode:
        args.training_corpus = get_tag_data_from_corpus_file(
            args.training_corpus)
    args.heldout_corpus = get_tag_data_from_corpus_file(
        args.heldout_corpus)
    args.test_corpora = [get_tag_data_from_corpus_file(x)
                         for x in args.test_corpora]

    # Test on the normal heldout and test data, as well as external
    # test corpora arguments
    corpus_name_collection = []
    corpus_collection = []
    if "train" in args.detection_mode:
        corpus_name_collection.append(training_corpus_name)
        corpus_collection.append(args.training_corpus)
    corpus_name_collection.extend([heldout_corpus_name] + test_corpora_names)
    corpus_collection.extend([args.heldout_corpus] + args.test_corpora)
    corpora = zip(corpus_name_collection, corpus_collection)

    print "Applying on corpora:"
    for c_name, _ in corpora:
        print c_name

    # cost file ranges for each classifier
    start_edit_cost = 3
    no_edit_cost = 3
    start_rp0_cost = 8
    no_rp0_cost = 8
    start_rm0_cost = 4
    no_rm0_cost = 4
    start_rpn_cost = 2
    no_rpn_cost = 2  # TODO more to do on this. 2 works best in practice
    stackDepth = 3

    # data prefixes
    edit_term_data = "e_data_"
    repair_onset_data = "rps_data_"
    reparandum_onset_data = "rms_data_"
    repair_extent_data = "rpn_data_"

    # BEGIN STIR EXPERIMENT
    # (1) EDIT TERMS %%%%%%%%%%%%%
    editcommand = ""
    editcommandfiles = ""
    edittrain = ""
    # iterate over the corpora
    for i in range(0, len(corpora)):
        corpusName = corpora[i][0]
        corpus = corpora[i][1]
	print corpusName

        data = experiment_directory + os.sep + edit_term_data + \
            corpusName + ".csv"  # over whichever test, inc. training
	print data
        if i == 0 and args.detection_mode == "train_test":
            edittrain = data
        else:
            editcommandfiles += " " + data  # concatenating test filenames
        if i >= 0 and not args.ed_on:
            continue  # stops the ed_on file creation
        dataFile = open(data, "w")
        referenceFile = dataFile.name[0: dataFile.name.rfind(".")] + \
            "REFERENCE.csv"
        referenceFile = open(referenceFile, "w")
        # setting headers
        referenceheader = ",".join(["W1", "W2", "W3",
                                    "POSW1", "POSW2", "POSW3"]) + "\n"
        header = "editprob,editWML,editdrop,edit1wordprob,edit2wordprob,"
        header += "prob,WML,drop,"
        header += "POSprob,POSWML,POSdrop,"
        header += "cat\n"
        dataFile.write(header)
        referenceFile.write(referenceheader)
        ranges = None
        tick = time.clock()
        for config in args.config:
            # only one iteration for non-cross_fold_lm style
            if args.cross_fold_lm and i == 0:
                # need to train each time unfort-
                # this only needs to be done in training
                myinput = ""
                mypos_corpus = ""
                myheldout_corpus = args.folds[config[1]][1]
                mypos_heldout_corpus = args.folds[config[1]][2]
                ranges = args.folds[config[2]][0]
                # get test
                for r in config[0]:
                    myinput += args.folds[r][1]
                    mypos_corpus += args.folds[r][2]
                print "training on "
                print ranges
                print len(ranges)
                sublm = KneserNeySmoothingModel(
                    order=args.order,
                    discount=args.discount,
                    partial_words=args.partial_words,
                    train_corpus=myinput,
                    heldout_corpus=myheldout_corpus,
                    second_corpus=args.secondcorpus)
                subposlm = KneserNeySmoothingModel(
                    order=args.order,
                    discount=args.discount,
                    partial_words=args.partial_words,
                    train_corpus=mypos_corpus,
                    heldout_corpus=mypos_heldout_corpus,
                    second_corpus=args.pos_secondcorpus)
            else:
                # if not in training mode,
                # just use the normal set up, else is non-cross_fold_lm
                sublm = lm
                ranges = None
                subposlm = POS_lm
            # corpusFile.seek(0) #where we got to last time,
            # else 0 for non-cross_fold_lm. Actually always go to 0 for now
            print "edit term experiment\ncorpus File : " + corpusName
            ex = edit_term(sublm,
                           pos_model=subposlm,
                           edit_model=edit_lm,
                           corpus=corpus,
                           corpus_name=corpusName,
                           dataFile=dataFile,
                           referenceFile=referenceFile,
                           ranges=ranges,
                           partial_words=args.partial_words)
            print "time for fold"
            tock = time.clock()
            print tock - tick
            tick = tock
            if i > 0 and args.detection_mode == 'train_test':
                # only one fold for test/heldout files
                break
        dataFile.close()
        referenceFile.close()

    for e in range(start_edit_cost, no_edit_cost+1):
        # runs over the same train/test data files,
        # but with the different costs
        editcost = args.experiment_directory + os.sep + \
            "costs" + os.sep + "Edit" + str(e) + ".csv"
        edittermdirectory = experiment_directory + os.sep + \
            "e_predictions_" + str(e)
        if args.ed_on:
            try:
                os.mkdir(edittermdirectory)
            except OSError:
                print "already made folder", edittermdirectory
        edittermpredictiondirectory = edittermdirectory + os.sep + \
            "e_predictions_" + str(e)
        edittermclassifier = 'None'
        if args.edit_term_classifier:  # Loading saved data for WEKA model
            edittermclassifier = args.edit_term_classifier[0]
            edittrain = args.edit_term_classifier[1]
        editcommand = "java -Xmx2g -jar {}/../STIRcomponent.jar ".\
            format(THIS_DIR) + \
            edittrain + " " + editcost + " " + \
            edittermpredictiondirectory + " " + \
            ed_ignore + editcommandfiles + " " + edittermclassifier
        print "calling " + editcommand
        if args.ed_on:
            subprocess.call(editcommand, shell=True)

        # (2) REPAIR ONSET %%%%%%%%%%%%%%%%%%%%%%
        repaironsetcommand = ""
        repaironsetcommandfiles = ""
        repaironsettrain = ""
        for i in range(0, len(corpora)):
            corpusName = corpora[i][0]
            corpus = corpora[i][1]
            data = edittermdirectory + os.sep + repair_onset_data + \
                corpusName + ".csv"  # over whichever test, inc. training
            if i == 0 and args.detection_mode == "train_test":
                repaironsettrain = data
            else:
                repaironsetcommandfiles += " " + data
            if i >= 0 and not args.rps_on:
                continue
            dataFile = open(data, "w")
            referenceFile = dataFile.name[0:dataFile.name.rfind(".")] + \
                "REFERENCE.csv"
            referenceFile = open(referenceFile, "w")
            editFile = edittermdirectory + os.sep + "e_predictions_" + \
                str(e) + "_" + corpusName + ".text"
            editFile = open(editFile)
            editGold = False
            if not editGold:
                editFileName = editFile.name[editFile.name.rfind(os.sep)+1:
                                             editFile.name.rfind('.text')]
            else:
                editFileName = "GOLD"
            print "repair onset experiment\ncorpus File : " + \
                corpusName + "\neditFile :" + editFileName
            editfilelines = editFile.readlines()
            # get to header of the results
            while not editfilelines[0][0:5] == "inst#":
                del editfilelines[0]
            del editfilelines[0]  # get rid of header
            # set headers
            referenceheader = "ref,index,"
            referencePOSheader = ""
            header = ""
            extra = ""
            posextra = ""
            # define window back from current word
            window = 3
            for n in range(0, window+1):
                if n > 0:
                    referenceheader += "W" + str(n) + ","
                    referencePOSheader += "POSW" + str(n) + ","
                if n < window:
                    extra += "W" + str(n) + "=W" + str(window) + ","
                    posextra += "POSW" + str(n) + "=POSW" + str(window)+","
            header += "prob,WML,drop,wordEntropy,wordEntropyHike,"
            header += "wordBestEntropyReduce,wordBestWMLBoost,"
            header += "wordInformationGain,length," + extra + "edit,"
            header += "POSprob,POSWML,POSdrop,POSEntropy,POSEntropyHike,"
            header += "POSBestEntropyReduce,POSBestWMLBoost,"
            header += "POSInformationGain," + posextra
            referenceheader += referencePOSheader
            header += "cat"
            dataFile.write(header + "\n")
            referenceFile.write(referenceheader + "\n")
            ranges = None
            tick = time.clock()
            previousExp = None
            fileIndex = 0
            for config in args.config: #only one iteration for non-cross_fold_lm style
                if args.cross_fold_lm and i == 0: #need to train each time unfort- this only needs to be done in training
                    myinput = ""
                    mypos_corpus = ""
                    myheldout_corpus = args.folds[config[1]][1]
                    mypos_heldout_corpus = args.folds[config[1]][2]
                    ranges =  args.folds[config[2]][0]
                    #get test
                    for r in config[0]:
                        myinput+=args.folds[r][1]
                        mypos_corpus+=args.folds[r][2]
                    print "training on "
                    print ranges
                    print len(ranges)
                    sublm = KneserNeySmoothingModel(order=args.order,discount=args.discount,partial_words=args.partial_words,
                                                train_corpus=myinput, heldout_corpus=myheldout_corpus,
                                                second_corpus=args.secondcorpus)
                    subposlm = KneserNeySmoothingModel(order=args.order,discount=args.discount,partial_words=args.partial_words,
                                                   train_corpus=mypos_corpus, heldout_corpus=mypos_heldout_corpus,
                                                   second_corpus=args.pos_secondcorpus)
                else: 
                    sublm = lm; subposlm = POS_lm; ranges = None #if not in training mode, just use the normal set up, else is non-cross_fold_lm
            
                ex = repair_onset(sublm,pos_model=subposlm,evaluation=True,corpus=corpus,corpus_name=corpusName,
                                    editfilelines=editfilelines,
                                    dataFile=dataFile, referenceFile=referenceFile,
                                    ranges=ranges,fileIndex=fileIndex,partial_words=args.partial_words,editGold=editGold,previousExp=previousExp)
                previousExp = ex
                fileIndex = ex.fileIndex
                print "time for fold"
                tock = time.clock()
                print tock - tick
                tick = tock
                if i > 0 and args.detection_mode == 'train_test':
                    # only one fold for test/heldout files
                    break
            dataFile.close()
            referenceFile.close()
         
        for rp in range(start_rp0_cost,no_rp0_cost+1):
            repaironsetcost = args.experiment_directory + os.sep + "costs"+ os.sep +"RepairOnset" + str(rp) + ".csv"
            repaironsetdirectory = edittermdirectory + os.sep + "rps_predictions_" + str(rp)
            if args.rps_on:
                try:
                    os.mkdir(repaironsetdirectory) #TODO remove when needed
                except OSError:
                    print "already created", repaironsetdirectory
            repaironsetpredictiondirectory = repaironsetdirectory + os.sep + "rps_predictions_"  + str(rp) #train/test prediction file
            repaironsetclassifier = 'None'
            if args.repair_onset_classifier: #Loading saved data for WEKA model
                repaironsetclassifier = args.repair_onset_classifier[0]
                repaironsettrain = args.repair_onset_classifier[1]
            repaironsetcommand = "java -Xmx2g -jar {}/../STIRcomponent.jar ".\
            format(THIS_DIR) + \
             repaironsettrain + " " + repaironsetcost + " " + \
                                repaironsetpredictiondirectory + " " + rps_ignore + repaironsetcommandfiles + " " + repaironsetclassifier
            print "calling " + repaironsetcommand
            if args.rps_on:
                subprocess.call(repaironsetcommand,shell=True) #TODO remove when needed
            
            #(3) REPARANDUM ONSET %%%%%%%%%%%%%%%%%%%%%%
            reparandumonsetcommand = ""
            reparandumonsetcommandfiles = ""
            reparandumonsettrain = ""
            for i in range(0,len(corpora)):
                corpusName = corpora[i][0]
                corpus = corpora[i][1]
                data = repaironsetdirectory + os.sep + reparandum_onset_data + corpusName+ ".csv" 
                if i == 0 and args.detection_mode == "train_test":
                    reparandumonsettrain = data
                else:
                    reparandumonsetcommandfiles += " " + data
                if i >= 0 and not args.rms_on:
                    continue
                dataFile = open(data,"w")
                referenceFile = dataFile.name[0:dataFile.name.rfind(".")]+"REFERENCE.csv"
                referenceFile = open(referenceFile,"w")
                editFile = edittermdirectory + os.sep + "e_predictions_"  + str(e) + "_" + corpusName + ".text"
                editFile = open(editFile)
                repairFile = repaironsetdirectory + os.sep + "rps_predictions_"  + str(rp) + "_" + corpusName + ".text"
                repairFile = open(repairFile)
                editGold = False
                repairGold = False
                if not editGold:
                    editFileName = editFile.name[editFile.name.rfind(os.sep)+1:editFile.name.rfind('.text')]
                else:
                    editFileName = "GOLD"
                if not repairGold:
                    repairFileName = repairFile.name[repairFile.name.rfind(os.sep)+1:repairFile.name.rfind('.text')]
                else: repairFileName = "GOLD"
                print "reparandum onset experiment\ncorpus File : " + corpusName + "\neditFile :" +editFileName + \
                "\nrepairFile :" + repairFileName
                editfilelines = editFile.readlines()
                repairfilelines = repairFile.readlines()
                #get to header of the results        
                while not editfilelines[0][0:5]== "inst#":
                    del editfilelines[0]
                del editfilelines[0] #get rid of header
                while not repairfilelines[0][0:5]== "inst#":
                    del repairfilelines[0]
                del repairfilelines[0] #get rid of header    
                #setting headers

                referenceheader = "ref,rpsPos,rmsPos," #for the marker of the repair in question and the potential starting point
                header = ""
                extra = ""
                posextra = ""
                referenceposheader = ""
                #TODO change this as we will always look 3 words back for repeat features
                window = 3 #always look this far back from current prefix, can try to make this go further back
                targets = ["rps_on","rms"]
                for target in targets:
                    for n in range(1,window+1):
                        referenceheader+=target+"W"+str(n)+","
                        referenceposheader+=target+"POSW"+str(n)+","
                        if n>1 and n<window+1 and target == "rps_on": #not looking at w1 at the mo, though possible for 4-chained repairs
                            extra+="rmsW"+str(n)+"=<rps_on>(embed)," #should not be positive for when it's its own rps0
                            #posextra+="rmPOSW"+str(n)+"=POSW"+str(n)+","
                extra = "rmsW"+str(window)+"=rpsW"+str(window)+"," + extra + "edit_rps,distanceback,lengthIntoUtt,"#logical extras, ident first
                #TODO add in another edit term presence feature for before w3 in reparandum boundary? #KL({(origN)-rms|..}-{..(origN)-rps_on|..}) taking this out for now
                posextra = "POSrmsW"+str(window)+"=POSrpsW"+str(window) + ","
                header+= "rp1confidence,logprob{..origN-rps1},WML{..origN-rps1},logprob{..origN-rm1},WML{..origN-rm1},LocalLogProbBoost,LocalWMLboost,LogProbBoost,WMLboost,WMLdrop,WordEntropy,WordEntropyReduce,WordRMPEntropy,"
                header+= extra #shouldn't have one at w1
                header+="POSlogprob{..origN-rps1},POSWML{..origN-rps1},POSlogprob{..origN-rm1}},POSWML{..origN-rm1},POSKL({(origN)-rms|..}-{..(origN)-rps_on|..}),POSLocalLogProbBoost,POSLocalWMLboost,POSLogProbBoost,POSWMLboost,POSWMLdrop,POSEntropy,POSEntropyReduce,POSRM0Entropy,"
                header+=posextra
                referenceheader+=referenceposheader
                #header+= "word1,word2,word3,"
                #header+= "RMword1,RMword2,RMword3,"
                #header+= "POS1,POS2,POS3,"
                #header+= "RMPOS1,RMPOS2,RMPOS3,"
                header+= "cat"  #cancel (for false pos's) #rm (within repair (keep going back)) #rm0 (beginning of repair, stop) #o (original utt)
                #print header
                    
                dataFile.write(header +"\n")
                referenceFile.write(referenceheader + "\n")
                ranges = None
                tick = time.clock()
                previousExp = None
                fileIndex = 0
                repairIndex = 0
                for config in args.config: #only one iteration for non-cross_fold_lm style
                    if args.cross_fold_lm and i == 0: #need to train each time unfort- this only needs to be done in training
                        myinput = ""
                        mypos_corpus = ""
                        myheldout_corpus = args.folds[config[1]][1]
                        mypos_heldout_corpus = args.folds[config[1]][2]
                        ranges =  args.folds[config[2]][0]
                        #get test
                        for r in config[0]:
                            myinput+=args.folds[r][1]
                            mypos_corpus+=args.folds[r][2]
                        print "training on "
                        print ranges
                        print len(ranges)
                        #limit = None
                        #if config[2] < len(args.folds)-1: limit = args.folds[config[2]+1][0][0] + ".utt1" #i.e. onset of next one
                        sublm = KneserNeySmoothingModel(order=args.order,discount=args.discount,partial_words=args.partial_words,
                                                train_corpus=myinput, heldout_corpus=myheldout_corpus,
                                                second_corpus=args.secondcorpus)
                        subposlm = KneserNeySmoothingModel(order=args.order,discount=args.discount,partial_words=args.partial_words,
                                                   train_corpus=mypos_corpus, heldout_corpus=mypos_heldout_corpus,
                                                   second_corpus=args.pos_secondcorpus)
                    else: 
                        sublm = lm; subposlm = POS_lm; ranges = None #if not in training mode, just use the normal set up, else is non-cross_fold_lm
                    
                    ex = reparandum_onset(sublm,pos_model=subposlm,evaluation=True,corpus=corpus,corpus_name=corpusName,
                                        editfilelines=editfilelines,repairfilelines=repairfilelines,
                                        dataFile=dataFile,referenceFile=referenceFile,
                                        ranges=ranges,fileIndex=fileIndex,repairIndex=repairIndex,
                                        partial_words=args.partial_words,editGold=editGold,repairGold=repairGold,previousExp=previousExp)
                    previousExp = ex
                    fileIndex = ex.fileIndex #getting where the edit file gets up to
                    repairIndex  = ex.repairIndex #where the repair file gets up to
                    print "time for fold"
                    tock = time.clock()
                    print tock - tick
                    tick = tock
                    if i > 0 and args.detection_mode == 'train_test':
                        # only one fold for test/heldout files
                        break
                dataFile.close()
                referenceFile.close()
                
                
            for rm in range(start_rm0_cost,no_rm0_cost+1):
                reparandumonsetcost = args.experiment_directory + os.sep + "costs" +  os.sep + "ReparandumOnset" + str(rm) + ".csv"
                reparandumonsetdirectory = repaironsetdirectory + os.sep + "rms_predictions_" + str(rm)
                reparandumonsetclassifier = 'None'
                if args.reparandum_onset_classifier: #Loading saved data for WEKA model
                    reparandumonsetclassifier = args.reparandum_onset_classifier[0]
                    reparandumonsettrain = args.reparandum_onset_classifier[1]
                if args.rms_on:
                    try:
                        os.mkdir(reparandumonsetdirectory)
                    except OSError:
                        print "already made dir", reparandumonsetdirectory
                reparandumonsetpredictiondirectory = reparandumonsetdirectory + os.sep +"rms_predictions_"  + str(rm) #train/test prediction files
                #TODO change back!:
                #args.reparandum_onset_classifier = reparandumonsetpredictiondirectory + "_SW1FullREF.model"
                reparandumonsetcommand = "java -Xmx2g -jar {}/../STIRcomponent.jar ".\
                format(THIS_DIR) + \
                 reparandumonsettrain + " " +  reparandumonsetcost +\
                 " " + reparandumonsetpredictiondirectory + " " + rms_ignore + reparandumonsetcommandfiles + " " + reparandumonsetclassifier
                print "calling",reparandumonsetcommand
                if args.rms_on: subprocess.call(reparandumonsetcommand,shell=True)
                
                #(4) REPAIR EXTENT %%%%%%%%%%%%%%%%%%%%%%%%%
                repairextentcommand = ""
                repairextentcommandfiles = ""
                repairextenttrain = ""
                for i in range(0,len(corpora)):
                    corpusName = corpora[i][0]
                    corpus = corpora[i][1]
                    data = reparandumonsetdirectory + os.sep + repair_extent_data + corpusName+ ".csv" 
                    if i == 0 and args.detection_mode == "train_test":
                        repairextenttrain = data
                    else:
                        repairextentcommandfiles += " " + data
                    if i >= 0 and not args.rpn_on:
                        continue
                    dataFile = open(data,"w")
                    referenceFile = dataFile.name[0:dataFile.name.rfind(".")]+"REFERENCE.csv"
                    referenceFile = open(referenceFile,"w")
                    editFile = edittermdirectory + os.sep +"e_predictions_"  + str(e) + "_" + corpusName + ".text"
                    editFile = open(editFile)
                    repairFile = repaironsetdirectory + os.sep + "rps_predictions_"  + str(rp) + "_" + corpusName + ".text"
                    repairFile = open(repairFile)
                    reparandumFile = reparandumonsetdirectory + os.sep + "rms_predictions_"  + str(rm) + "_" + corpusName + ".text"
                    reparandumFile = open(reparandumFile)
                    editGold = False
                    repairGold = False
                    reparandumGold = False
                    if not editGold:
                        editFileName = editFile.name[editFile.name.rfind(os.sep)+1:editFile.name.rfind('.text')]
                    else:
                        editFileName = "GOLD"
                    if not repairGold:
                        repairFileName = repairFile.name[repairFile.name.rfind(os.sep)+1:repairFile.name.rfind('.text')]
                    else:
                        repairFileName = "GOLD"
                    if not reparandumGold:
                        reparandumFileName = reparandumFile.name[reparandumFile.name.rfind(os.sep)+1:reparandumFile.name.rfind('.text')]
                    else: reparandumFileName = "GOLD"
                    print "repair extent experiment\ncorpus File : " + corpusName + "\neditFile :" +editFileName + \
                    "\nrepairFile :" + repairFileName + "\nreparandumonsetFile :" + reparandumFileName
                    editfilelines = editFile.readlines()
                    repairfilelines = repairFile.readlines()
                    reparandumfilelines = reparandumFile.readlines()
                    #get to header of the results        
                    while not editfilelines[0][0:5]== "inst#":
                        del editfilelines[0]
                    del editfilelines[0] #get rid of header
                    while not repairfilelines[0][0:5]== "inst#":
                        del repairfilelines[0]
                    del repairfilelines[0] #get rid of header
                    while not reparandumfilelines[0][0:5]== "inst#":
                        del reparandumfilelines[0]
                    del reparandumfilelines[0] #get rid of header
                    
                    #might want tag files here to see whats going on if we don't have good rpn tags
                    tagFileName = reparandumFile.name[0:reparandumFile.name.rfind(".text")]+"TAGS.text"
                    tagFile = open(tagFileName, "w")
                    
                    #setting headers
                    referenceheader = "ref,rpsPos,rmsPos," #for the marker of the repair in question and the potential starting point
                    header = ""
                    extra = ""
                    posextra = ""
                    referenceposheader = ""
                    #TODO change this as we will always look 3 words back for repeat features
                    window = 3 #always look this far back from current prefix, can try to make this go further back
                    referenceheader = "ref,rpsPos,rmsPos,rpnPos" #for the marker of the repair in question and the potential starting point
                    header = "RP0confidence,RM0confidence,reparandumLength,repairLength,embedded," #basic features
                    header+="KLposRM0RP0,KLposRMNRPN,RM0RP0,RMNRPN,wordIdent,RM0RP0POS,RMNRPNPOS,POSident," #similarity features (last four binary)
                    header+="reparandumRepairDiff,POSreparandumRepairDiff," #subsitutability evaluation
                    header+="wordLocalWMLBoost,wordLocalProbBoost,POSLocalWMLBoost,POSLocalProbBoost," #boost features
                    header+="wordInfoGainReduce,wordRM0Entropy,POSInfoGainReduce,POSRM0Entropy," #entropy features
                    #header+= "RPSword1,RPSword2,RPSword3,"
                    #header+= "RMword1,RMword2,RMword3,"
                    #header+= "RPNword1,RPNword2,RPNword3,"
                    #header+= "POS1,POS2,POS3,"
                    #header+= "RMPOS1,RMPOS2,RMPOS3,"
                    #header+= "RPNPOS1,RPNPOS2,RPNPOS3,"
                    header+="cat"
   
                    dataFile.write(header +"\n")
                    referenceFile.write(referenceheader + "\n")
                    ranges = None
                    tick = time.clock()
                    previousExp = None
                    fileIndex = 0
                    repairIndex = 0
                    reparandumIndex = 0
                    for config in args.config: #only one iteration for non-cross_fold_lm style
                        if args.cross_fold_lm and i == 0: #need to train each time unfort- this only needs to be done in training
                            myinput = ""
                            mypos_corpus = ""
                            myheldout_corpus = args.folds[config[1]][1]
                            mypos_heldout_corpus = args.folds[config[1]][2]
                            ranges =  args.folds[config[2]][0]
                            #get test
                            for r in config[0]:
                                myinput+=args.folds[r][1]
                                mypos_corpus+=args.folds[r][2]
                            print "training on "
                            print ranges
                            print len(ranges)
                            #limit = None
                            #if config[2] < len(args.folds)-1: limit = args.folds[config[2]+1][0][0] + ".utt1" #i.e. onset of next one
                            sublm = KneserNeySmoothingModel(order=args.order,discount=args.discount,partial_words=args.partial_words,
                                                train_corpus=myinput, heldout_corpus=myheldout_corpus,
                                                second_corpus=args.secondcorpus)
                            subposlm = KneserNeySmoothingModel(order=args.order,discount=args.discount,partial_words=args.partial_words,
                                                   train_corpus=mypos_corpus, heldout_corpus=mypos_heldout_corpus,
                                                   second_corpus=args.pos_secondcorpus)
                        else:
                            sublm = lm
                            subposlm = POS_lm
                            ranges = None  # if not in training mode, just use the normal set up, else is non-cross_fold_lm
                        
                        ex = repair_end(sublm,pos_model=subposlm,evaluation=True,corpus=corpus,corpus_name=corpusName,
                                            editfilelines=editfilelines,repairfilelines=repairfilelines,reparandumfilelines=reparandumfilelines,
                                            dataFile=dataFile,referenceFile=referenceFile,
                                            ranges=ranges,fileIndex=fileIndex,repairIndex=repairIndex,reparandumIndex=reparandumIndex,
                                            partial_words=args.partial_words,editGold=editGold,repairGold=repairGold,reparandumGold=reparandumGold,
                                            previousExp=previousExp,stackDepth=stackDepth)
                        previousExp = ex
                        fileIndex = ex.fileIndex #getting where the edit file gets up to
                        repairIndex  = ex.repairIndex #where the repair file gets up to
                        reparandumIndex = ex.reparandumIndex #where the reparandum file gets up to
                        print "time for fold"
                        tock = time.clock()
                        print tock - tick
                        tick = tock
                        if i > 0 and args.detection_mode == 'train_test':
                            # only one fold for test/heldout files
                            break
                    tagFile.close()
                    dataFile.close()
                    referenceFile.close()

                for rpn in range(start_rpn_cost, no_rpn_cost+1):
                    repairextentcost = args.experiment_directory + os.sep +"costs"+ os.sep +"RepairExtent" + str(rpn) + ".csv"
                    repairextentdirectory = reparandumonsetdirectory + os.sep + "rpn_predictions_" + str(rpn) + "_stackD" + str(stackDepth)
                    if args.rpn_on:
                        try:
                            os.mkdir(repairextentdirectory)  #TODO the file names are too long so shortening here
                        except OSError:
                            print "already created dir", repairextentdirectory
                    repairextentpredictiondirectory = repairextentdirectory + os.sep +"_"  + str(rpn) + "_stackD" + str(stackDepth) #train/test prediction files
                    repairendclassifier = 'None'
                    if args.repair_end_classifier: #Loading saved data for WEKA model
                        repairendclassifier = args.repair_end_classifier[0]
                        repairextenttrain = args.repair_end_classifier[1]
                    repairextentcommand = "java -Xmx2g -jar {}/../STIRcomponent.jar ".\
                      format(THIS_DIR) + \
                      repairextenttrain + " " +\
                      repairextentcost + " " + repairextentpredictiondirectory + " " + rpn_ignore +\
                       repairextentcommandfiles + " " + repairendclassifier
                    print "calling",repairextentcommand
                    if args.rpn_on:
                        subprocess.call(repairextentcommand,shell=True)

                    # (5) FINAL OUTPUT TO RESULTS DIR
                    # (DETECTION + CLASSIFICATION) %%%%%%%%%%%%%%%%%%%%%%%%%
                    posstring = ""
                    if args.pos_tagged:
                        posstring = "pos"
                    setting_result_dir = results_directory + os.sep + \
                        "stack_d" + str(stackDepth) + "_lm" + str(lm.order) + \
                        posstring + partial_string + "_" + \
                        training_corpus_name + "_" + \
                        str(e)+"_"+str(rp)+"_"+str(rm)+"_"+str(rpn)
                    try:
                        os.mkdir(setting_result_dir)
                    except OSError:
                        print "problem creating ", setting_result_dir
                    for i in range(0, len(corpora)):
                        # TODO filenames too long!
                        corpusName = corpora[i][0]
                        corpus = corpora[i][1]
                        if i == 0 and args.detection_mode == "train_test":
                            continue  # TODO skip switchboard training data
                        repairextentfile = repairextentdirectory + \
                            os.sep + "_" + str(rpn) + \
                            "_stackD" + str(stackDepth) + \
                            "_" + corpusName + ".text"
                        reparandumonsetfile = reparandumonsetdirectory + \
                            os.sep + "rms_predictions_" + str(rm) + \
                            "_" + corpusName + ".text"
                        editFile = edittermdirectory + \
                            os.sep + "e_predictions_" + str(e) + \
                            "_" + corpusName + ".text"
                        repaironsetfile = repaironsetdirectory + os.sep + \
                            "rps_predictions_" + str(rp) + "_" + \
                            corpusName + ".text"
                        tag_file = setting_result_dir + \
                            os.sep + corpusName + "_TAGS.text"
                        inc_tag_file = setting_result_dir + os.sep +\
                            corpusName + "_inc_TAGS.text"
                        easy_read_tag_file = setting_result_dir + os.sep +\
                            corpusName + "_easy_read_TAGS.text"
                        final_classifier(lm,
                                         pos_model=POS_lm,
                                         evaluation=True,
                                         corpus=corpus,
                                         corpus_name=corpusName,
                                         editFile=editFile,
                                         repairFile=repaironsetfile,
                                         reparandumFile=reparandumonsetfile,
                                         repairExtentFile=repairextentfile,
                                         tagFile=tag_file,
                                         incrementalTagFile=inc_tag_file,
                                         easyReadTagFile=easy_read_tag_file,
                                         partial_words=args.partial_words,
                                         stackDepth=stackDepth)
                        eval_file_path = tag_file.replace("_TAGS.",
                                                          "_output_final.")
                        create_final_evaluation_file_from_stir_output(
                                                  tag_file,
                                                  eval_file_path,
                                                  gold_file_path=None,
                                                  timings=False,
                                                  eval_word=True,
                                                  eval_interval=False)

    print time.clock() - big_tick, "seconds for experiment"

if __name__ == '__main__':
    args = process_arguments()
    train_test(args)

