from stir_classifier import stir_classifier

class edit_term(stir_classifier):
    
    #def __init__(self,lm,classifier=None):
    #    self.lm = lm
    #    self.classifier = classifier
    #    self.currentUtt = []
        
    #def get_features_from_current_word(self):
        
    def __init__(self,lm,pos_model=None,edit_model=None,corpus=None,corpus_name=None,dataFile=None,referenceFile=None,ranges=None,partial_words=False):
        """Language model features for detecting editing terms (produce features for interregnum/edit term classifier)"""
        delta = lm.order-1
        posdelta = pos_model.order-1
        p = posdelta-delta  #difference between i and pos index!
        
        a,b,c,d = corpus
        for reference,words,pos,tags in zip(a,b,c,d):
            if ranges and not reference.split(":")[0] in ranges:
                continue
            #self.fileIndex+=1
            editBool = False
            wordprevious = 0 # for getting clean WML drop
            editprevious = 0 # for edit term lm WML drop
            worddrop = 0
            editdrop = 0
            posdrop = 0
    
            posprevious = 0 #getting WML pos drop
            words = delta * ["<s>"] + list(words) #all the non-edit words so far these can be removed with "TWO" word hypothesis, though not added to
            tags = delta * [""] + list(tags)
            pos = posdelta * ["<s>"] + list(pos)
            
            for i in range(lm.order-1,len(words)): #we need to get both words and POS to do this properly..
                
                if partial_words == False and words[i][-1] == "-": #need to add 'partial' penalty for editPoints below..
                    continue
                tag = "NONE" #only the last one is not
                if "<e" in tags[i] or "<i" in tags[i] or "<r" in tags[i]:  #case!
                    if "<i" in tags[i] or "<e" in tags[i]: #not distinguishing between them for now
                        tag = "ONE" #THIS IS THE EQUIVALENT OF ONE
                        if editBool == True:
                            tag = "TWO"
                        editBool = True
                    else:
                        #if editBool == False: tag = "NONETWO";
                        editBool = False
                    #words[i] = words[i][words[i].rfind('>')+1:] #delete all tags
                    #pos[(i+p)] = pos[(i+p)][pos[(i+p)].rfind('>')+1:]
                else:
                    #THIS IS THE EXTRA ONE
                    #if editBool == True:
                    #    tag = "YESNO" #still in the two word window
                    #if editBool == False: tag = "NONETWO";
                    editBool = False #i.e. two fluent words, can cancel!
                    
                #clean probs
                if i >= 2:
                    wordngram = words[i-2:i+1] #let's always do a trigram back, where possible
                else:
                    wordngram = words[i-1:i+1] #for bigrams at start
                if wordngram == ["<s>","<s>","<s>"]: print reference
                #print "words = " + str(wordngram)
                wordlogprob = sum(lm.tokens_logprob(wordngram,lm.order))
                wordWML = lm.logprob_weighted_by_inverse_unigram_logprob(wordngram)
                
                """ #ppat/looking ahead for more accuracy
                lookaheadngram = wordngram
                lookaheadlogprob = wordlogprob
                lookaheadWML = wordWML
                
                if i < len(words)-1: #i.e. there is a word to look ahead to
                    lookaheadngram = words[i-lm.order+2:i+2]
                    lookaheadlogprob = sum(lm.tokens_logprob(lookaheadngram,lm.order))
                    lookaheadWML = lm.logprob_weighted_by_inverse_unigram_logprob(lookaheadngram)
                """
                #if partial_words == True: #TODO fix this
                #    if not "Partial" in corpusName and properPrefix(wordngram[delta-1],wordngram[delta]): wordWML = -3.0
                #    elif wordngram[delta-1][-1] == "-": wordWML = -3.0
                if i > delta: worddrop = wordprevious - wordWML #or do we do this without the edit words i.e. previouswordngram = nonEditPrefixWords[-lm.order:-1]; previous= sum(lm.tokens_logprob(previouswordngram,lm.order))
                wordprevious = wordWML #clean score                
                #edit term LM
                editngram = words[i-edit_model.order+1:i+1] #gives two word prob
                editlogprob = sum(edit_model.tokens_logprob(editngram,edit_model.order))
                test1 = editngram #these good for midsentence ones...
                test2 = editngram
                if i > delta: test1 = list(["<s>"]+[editngram[-1]])
                if i > lm.order: test2 = list(["<s>"]+editngram)
                edit1wordlogprob = sum(edit_model.tokens_logprob(test1,edit_model.order))
                edit2wordlogprob = sum(edit_model.tokens_logprob(test2,edit_model.order))
                editWML = edit_model.logprob_weighted_by_inverse_unigram_logprob(editngram)
                #if partial_words == True and properPrefix(editngram[edit_model.order-2],editngram[edit_model.order-1]): editWML = -3.0 # partial words
                if i >= lm.order: editdrop = editprevious - editWML
                editprevious = editWML
                
                cat = tag
                
                #just for reference purposes in case we have a bigram
                if len(wordngram) < 3:
                    wordngram = ["null"] + wordngram
                for word in wordngram:
                    referenceFile.write(word+",")
                dataFile.write(str(editlogprob)+","+str(editWML)+","+str(editdrop)+","+\
                           str(edit1wordlogprob)+","+str(edit2wordlogprob)+","+\
                            str(wordlogprob)+","+str(wordWML)+","+str(worddrop)+",")
                #print str(editlogprob)+","+str(editWML)+","+str(editdrop)+","+str(wordlogprob)+","+str(wordWML)+","+str(worddrop)+","+extra+","
                #now for POS, given we've done the words
                if (i+p) >= 2: #TODO need to change to posi and need posdelta too?, or do a pos diff..
                    posngram = pos[(i+p)-2:(i+p)+1] #let's always do a trigram back, where possible
                else:
                    posngram = pos[(i+p)-1:(i+p)+1] #for bigrams at start
                #print posngram
                #raw_input()
                poslogprob = sum(pos_model.tokens_logprob(posngram,lm.order))
                posWML = pos_model.logprob_weighted_by_inverse_unigram_logprob(posngram)
                if (i+p) >= pos_model.order: posdrop = posprevious - posWML #or do we do this without the edit words i.e. previouswordngram = nonEditPrefixWords[-lm.order:-1]; previous= sum(lm.tokens_logprob(previouswordngram,lm.order))
                posprevious = posWML              
               
                if len(posngram) < 3:
                    posngram = ["<s>"] + posngram
                for w in range(0,len(posngram)-1):
                    referenceFile.write(posngram[w]+",")
                referenceFile.write(posngram[-1]+"\n") #last line of referenceFile
                dataFile.write(str(poslogprob)+","+str(posWML)+","+str(posdrop)+",")
                #for word in wordngram:
                #    #if not word in lm.unigrams: #might be fairer?
                #    #    word = "<unk>"
                #    dataFile.write(word+",") #NOTE using the actual values themselves
                #for word in posngram:
                #    #if not word in pos_model.unigrams: #might be fairer?
                #    #    word = "<unk>"
                #    dataFile.write(word+",") #NOTE using the actual values themselves
                dataFile.write(cat+"\n")
        return
        #end of file loop