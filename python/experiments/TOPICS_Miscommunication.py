import os
import sys
THIS_DIR = os.path.dirname(os.path.realpath(__file__))
sys.path.append(THIS_DIR + "/../")

from corpus.disfluency_corpus_creator import DisfluencyCorpusCreator
from corpus.tree_pos_map_writer import POSMapWriter
from stir.train_test_offline import process_arguments
from stir.train_test_offline import train_test

PRIVATE_USE = False  # NB should be false in public repo

CREATE_CORPORA = False
TRAIN = False
TEST = True

SWBD_DIR = THIS_DIR + '/../data/switchboard'

PARTIAL_WORDS = True  # whether you want to include partial words

# (OPTIONAL) If no STIR formatted data (and no POS map), create it
if CREATE_CORPORA:
    # folder where the SWDA style files for disfluency detection
    # You need to clone https://bitbucket.org/julianhough/stir_data
    # in the sister directory to this repository
    source_dir = os.sep.join([THIS_DIR,
                              '..',
                              '..',
                              'stir_data',
                              'raw_data',
                              'bnc_spoken'])
    target_dir = os.sep.join(['data',
                              'bnc_spoken'])
    # folder where you want the created corpus for detection to go
    corpusName = "BNC-CH"  # what you want to call the corpus
    try:
        os.mkdir(source_dir+os.sep+"posmap")
    except OSError:
        print "couldn't make", source_dir+os.sep+"posmap"
    posMapWriter = POSMapWriter(
        source_dir,
        metadata_path=None,
        file_path=source_dir + os.sep + "posmap" + os.sep + "POSMap00.csv.txt")
    n = DisfluencyCorpusCreator(source_dir,
                                metadata_path=None,
                                mapdir_path=source_dir+os.sep+'posmap',
                                partial=PARTIAL_WORDS,
                                p1_annotation_files=None,
                                p3_annotation_files=None,
                                p1_annotation_files_relaxed=[
                                    source_dir + os.sep +
                                    "annotations" + os.sep +
                                    "BNCMarcusRepairsRelaxed.csv"])
    n.makeCorpus(target_folder=target_dir,
                 corpus=corpusName,
                 range_folder=None,
                 mode="repair")
    n.errorlog.close()

for pos_tagged in [True, False]:  # run with POS tags and without POS tags
    args = process_arguments()
    args.pos_tagged = pos_tagged  # overrides defaults/command line
    pos_string = ""
    if args.pos_tagged:
        pos_string = "_pos"
    partial_string = ""
    if PARTIAL_WORDS:
        args.partial_words = True
        partial_string = "_partial"

    args.training_corpus = os.sep.join([SWBD_DIR,
                                        "disfluency_detection",
                                        "swbd_disf_train_1{}_data.csv".
                                        format(partial_string)])
    args.heldout_corpus = os.sep.join([SWBD_DIR,
                                       "disfluency_detection",
                                       "swbd_disf_heldout{}_data.csv".
                                       format(partial_string)])
    args.test_corpora = [
        os.sep.join([
            SWBD_DIR,
            "disfluency_detection",
            "swbd_disf_test{}_data.csv".format(partial_string)]),
        THIS_DIR + '/../data/bnc_spoken/BNC-CH{}_data.csv'.format(
            partial_string)
                         ]
    if PRIVATE_USE:
        args.test_corpora.append(
            THIS_DIR +
            '/../data/pcc/PCC_test{}_data.csv'.format(partial_string))
    # args.secondcorpus = "data" + os.sep + "bnc" + os.sep + "bncFold3REF.text"
    # args.secondcorpus = "data" + os.sep + "switchboard" + \
    #     os.sep + "SWDisfHeldoutCleanREF.text" #To balance the size

    args.cross_fold_lm = False
    mode = ""
    if TRAIN:
        mode += "train_"
        args.cross_fold_lm = True  # always do cross fold LM training
    if TEST:
        mode += "test"
    args.detection_mode = mode

    # Saved classifiers, currently need to be specified a tuple of <classifier
    # training_data> due to WEKA's model input requirements
    if args.detection_mode == "test":
        SAVED_DIR = THIS_DIR + "/../../../stir_data/saved_models/" + \
            "words{0}{1}/classifiers".format(pos_string, partial_string)
        args.edit_term_classifier = (os.sep.join([SAVED_DIR,
                                                  "e",
                                                  "e_3",
                                                  "e_3.model"]),
                                     os.sep.join([SAVED_DIR,
                                                  "e",
                                                  "e_3",
                                                  "data.csv"]))
        args.repair_onset_classifier = (os.sep.join([SAVED_DIR,
                                                     "rps",
                                                     "rps_3_8",
                                                     "rps_3_8.model"]),
                                        os.sep.join([SAVED_DIR,
                                                     "rps",
                                                     "rps_3_8",
                                                     "data.csv"]))
        args.reparandum_onset_classifier = (os.sep.join([SAVED_DIR,
                                                         "rms",
                                                         "rms_3_8_4",
                                                         "rms_3_8_4.model"]),
                                            os.sep.join([SAVED_DIR,
                                                         "rms",
                                                         "rms_3_8_4",
                                                         "data.csv"]))
        args.repair_end_classifier = (os.sep.join([SAVED_DIR,
                                                   "rpn",
                                                   "rpn_3_8_4_2_d3",
                                                   "rpn_3_8_4_2_d3.model"]),
                                      os.sep.join([SAVED_DIR,
                                                   "rpn",
                                                   "rpn_3_8_4_2_d3",
                                                   "data.csv"]))

        # args.input = args.heldout # for fast test of output only
        args.ed_on = True  # For testing turn various components on and off
        args.rps_on = True
        args.rms_on = True
        args.rpn_on = True
    else:
        args.edit_term_classifier = None
        args.repair_onset_classifier = None
        args.reparandum_onset_classifier = None
        args.repair_end_classifier = None

    train_test(args)

# To see the evaluation results see experiments/analysis/TOPICS.ipynb
