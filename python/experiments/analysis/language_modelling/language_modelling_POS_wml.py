
# coding: utf-8

# In[1]:

import os
import sys
sys.path.append(os.path.join(os.getcwd(),".."))
from language_model.util import safe_open, tag_separator, Timer
from language_model.ngram_language_model import KneserNeySmoothingModel
import numpy as np
import scipy as sp
import argparse
import re


# In[2]:

#opens the corpora
train_corpus = safe_open("../data/bnc_spoken/bnc_spokenAllREF.text",'r')
resultsfile = open("PurverEtAl2016Clarifications.text","w")
#heldout_corpus = safe_open(heldout_corpus,'r')


# In[3]:

print>>resultsfile, "Results for Purver et al 2016\n\n"


# In[4]:

#train the KN smoothed LM
lm = KneserNeySmoothingModel(order=3,discount=0.7,partial_words=True,
                                 train_corpus=train_corpus, heldout_corpus=None,
                                 second_corpus=None)


# In[5]:

#TESTING#####################################################
#Matt's request re POS information content
content = "^(N|V|A|PRO).*"
function = "^(D|CJ|PRP|TO|ORD|CRD).*"
noun = "^(N|PRO).*"
verb = "^V.*" 
common_noun = "^NN.*"
non_aux_verb = "^VV.*"

def corpus_experiment(lm_function):
    pos_content = []
    pos_function = []
    pos_noun = []
    pos_verb = []
    pos_common_noun = []
    pos_non_aux_verb = []
    count = 100
    train_corpus.seek(0)
    for line in train_corpus:
        if not "POS," in line:
            words = ["<s>","<s>"] + line.split()
            continue
        else:
            pos =  ["<s>","<s>"] + line.split()

            for i in range(2,len(pos)):
                test = pos[i-2:i+1]
                ngram = words[i-2:i+1]

                #Value of inteterest for trigram final with matching POS values 
                if re.match(noun,test[-1]):
                    v_pos = lm_function(ngram)
                    pos_noun.append(v_pos)
                if re.match(verb,test[-1]):
                    v_pos = lm_function(ngram)
                    pos_verb.append(v_pos)
                if re.match(function,test[-1]):
                    v_pos = lm_function(ngram)
                    pos_function.append(v_pos)
                if re.match(content,test[-1]):
                    v_pos = lm_function(ngram)
                    pos_content.append(v_pos)
                if re.match(common_noun,test[-1]):
                    v_pos = lm_function(ngram)
                    pos_common_noun.append(v_pos)
                if re.match(non_aux_verb,test[-1]):
                    v_pos = lm_function(ngram)
                    pos_non_aux_verb.append(v_pos)
        count-=1
        #if count == 0: break


    print>>resultsfile,"NUMBER (tokens) OF CONTENT words: " + str(len(pos_content))
    print>>resultsfile,"NUMBER (tokens) OF FUNCTION words: " + str(len(pos_function))
    print>>resultsfile,"NUMBER (tokens) OF NOUN words: " + str(len(pos_noun))
    print>>resultsfile,"NUMBER (tokens) OF COMMON NOUN words: " + str(len(pos_common_noun))
    print>>resultsfile,"NUMBER (tokens) OF VERB words: " + str(len(pos_verb))
    print>>resultsfile,"NUMBER (tokens) OF NON AUX VERB words: " + str(len(pos_non_aux_verb))

    #get mean values
    pos_mean_noun = np.average(pos_noun)
    pos_mean_verb = np.average(pos_verb)
    pos_mean_function = np.average(pos_function)
    pos_mean_content = np.average(pos_content)
    pos_mean_common_noun = np.average(pos_common_noun)
    pos_mean_non_aux_verb = np.average(pos_non_aux_verb)


    print>>resultsfile,"\n%%%%%%%%%%%%%%%"
    print>>resultsfile,"CONTENT mean: " + str(pos_mean_content)
    print>>resultsfile,"FUNCTION mean: " + str(pos_mean_function)
    print>>resultsfile,""
    print>>resultsfile,"NOUN mean: " + str(pos_mean_noun)
    print>>resultsfile,"COMMON NOUN mean: " + str(pos_mean_common_noun)
    print>>resultsfile,"VERB mean: " + str(pos_mean_verb)
    print>>resultsfile,"NON AUX VERB mean: " + str(pos_mean_non_aux_verb)


# In[6]:

#Get values for the function of interest
#(1) Information content (surprisal)
print>>resultsfile,"%%%%%%%%%%%%%%%%%Information content (neg log prob)\n"
lm_function = lambda x : lm.surprisal(x,3)
corpus_experiment(lm_function)


# In[ ]:

#(2) WML (syntactic probability)
print>>resultsfile,"%%%%%%%%%%%%%%%%%WML (syntactic probability)\n"
lm_function = lambda x : lm.logprob_weighted_by_inverse_unigram_logprob(x)
corpus_experiment(lm_function)


# In[ ]:

#(3) Entropy of continuation (takes a long time)
print>>resultsfile,"%%%%%%%%%%%%%%%%%Entropy of continuation\n"
lm_function = lambda x : lm.entropy_continuation_fast(x[1:],3)
corpus_experiment(lm_function)


# In[ ]:

resultsfile.close()


# In[ ]:



