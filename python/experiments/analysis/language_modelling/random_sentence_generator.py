#Method for KneyserNey model generation
import random

def generate(lm, WMLThreshold, logprobThresholdLower, logprobThresholdUpper, length):
    """generates utterances incrementally by trying to maintain grammaticality through WML, though simultaneously trying to maximise semantic entropy?"""
    #are we argMaxing each time? say WMLThreshold =< -1.3, lots of ungrammatical stuff
    delta = lm.order -1
    string = ['<s>'] * delta
    mystring = ""
    outlist = []
    repair = 0
    while string[-1]!= "<\s>" and len(string) < length + delta:
        context = string[-delta:]
        unigrams = list(lm.unigrams.keys())
        random.shuffle(unigrams)
        gram = False
        found = False
        for unigram in unigrams:
            #if unigram in outlist:
            #    continue
            candidateNgram = context + [unigram]
            if lm.logprob_weighted_by_inverse_unigram_logprob(candidateNgram) > WMLThreshold:
                testTokens = string + [unigram]
                gram = True
            else:
                continue
            #testString = mystring + candidate + " "
            test = lm.logprob_weighted_by_sequence_length(testTokens)
            if test > logprobThresholdLower and test < logprobThresholdUpper: #needs to be surpising/interesting enough
                string.append(unigram)
                mystring+=unigram+" "
                print mystring
                found = True
                outlist=[]
                repair = 0 #no. not quite, needs recursive graph search
                break
            else:
                outlist.append(unigram)
        if found == False:
            print "not found REPAIR"
            print string
            repair+=1
            string = string[:-repair] #goes back one at a time to get the repair
            print string
            print test
        if gram == False:
            print "no syntax pass either!"
            #print string
    return string