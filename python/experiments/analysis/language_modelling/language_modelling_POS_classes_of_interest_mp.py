# coding: utf-8

# In[3]:

import multiprocessing
import os
import sys
import time

sys.path.append(os.path.join(os.getcwd(), ".."))
from language_model.util import safe_open, tag_separator, Timer
from language_model.ngram_language_model import KneserNeySmoothingModel
import numpy as np
import scipy as sp
import argparse
import re

# In[15]:

# opens the corpora
train_corpus = safe_open("../data/bnc_spoken/bnc_spokenAllREF.text", 'r')
resultsfile = open("mp.txt", "w")
# heldout_corpus = safe_open(heldout_corpus,'r')


# In[5]:

print>> resultsfile, "Results for Purver et al 2016\n\n"

# In[6]:

# train the KN smoothed LM
start = time.time()
lm = KneserNeySmoothingModel(order=3, discount=0.7, partial_words=True,
                             train_corpus=train_corpus, heldout_corpus=None,
                             second_corpus=None)
print "trained in %s secs" % (time.time() - start)
print>> resultsfile, "trained in %s secs" % (time.time() - start)

# In[12]:

# TESTING#####################################################
# Purver et al. 2016 paper on clarification requests
tests = {}
tests['oldcontent'] = "^(N|V|A|PRO).*"
tests['oldfunction'] = "^(D|CJ|PRP|TO|ORD|CRD).*"
tests['content'] = "^(N|V|AJ|AV|PN).*"  # not sure about AVQ, PNQ
tests['function'] = "^(D|AT|CJ|PR|TO|ORD|CRD).*"
tests['determiner'] = "^(D|AT).*"
tests['number'] = "^(ORD|CRD).*"
tests['noun'] = "^(N|PRO).*"
tests['verb'] = "^V.*"
tests['common_noun'] = "^NN.*"
tests['non_aux_verb'] = "^VV.*"
tests['aux_verb'] = "^V[^V].*"
for t in tests:
    print t, tests[t]


def mp_surprisal(x):
    """for multiprocessing without lambdas"""
    return lm.surprisal(x, 3)


def mp_entropy_now_fast(x):
    """for multiprocessing without lambdas"""
    return lm.entropy_continuation_very_fast(x[0:-1], 3)


def mp_entropy_next_fast(x):
    """for multiprocessing without lambdas"""
    return lm.entropy_continuation_very_fast(x[1:], 3)


def mp_entropy_now(x):
    """for multiprocessing without lambdas"""
    return lm.entropy_continuation(x[0:-1], 3)


def mp_entropy_next(x):
    """for multiprocessing without lambdas"""
    return lm.entropy_continuation(x[1:], 3)


# set up a CPU pool if we're on a multi-CPU machine - but don't use more than 20
numcpu = min(20, multiprocessing.cpu_count())
print("Setting up CPU pool: %s" % numcpu)


def corpus_experiment(lm_function):
    start = time.time()
    test_res = {}
    for t in tests:
        test_res[t] = []

    count = 0
    pos_ngrams = []
    lex_ngrams = []
    train_corpus.seek(0)
    for line in train_corpus:
        # print "line", line # TODO fix - add space in e.g. NP0POS for tag of "mary's"
        if not line.startswith("POS,"):
            line = line.lstrip(",")
            words = ["<s>", "<s>"] + line.split()
            continue
        else:
            line = line.lstrip("POS,")
            pos = ["<s>", "<s>"] + line.split()
            for i in range(2, len(pos)):
                pos_ngrams.append(pos[i - 2:i + 1])
                lex_ngrams.append(words[i - 2:i + 1])

        count += 1
        if count % 500 == 0:
            print count, "c in %s secs" % (time.time() - start)
        if count > 5000:
            break

    # calculate the critical values - functions to use *must* be defined before creating Pool
    pool = multiprocessing.Pool(processes=numcpu)
    ares = pool.map_async(lm_function, lex_ngrams)
    pool.close()
    original = -1
    last = original
    while True:
        if ares.ready(): break
        remaining = ares._number_left
        if original < 0:
            original = remaining
        if remaining != last:
            print "Waiting for", remaining, "of", original, "tasks to complete, %s secs so far ..." % (time.time() - start)
            last = remaining
        time.sleep(0.5)
    res = ares.get()
    print "done1 in %s secs %s %s" % (time.time() - start, len(lex_ngrams), len(res))

    # now collate vs matching POS values
    for (i, r) in enumerate(res):
        for t in tests:
            if re.match(tests[t], pos_ngrams[i][-1]):
                # print "matched", t, lex_ngrams[i], pos_ngrams[i]
                test_res[t].append(r)

    print "done2 in %s secs" % (time.time() - start)
    print>> resultsfile, "done in %s secs" % (time.time() - start)
    for t in tests:
        print>> resultsfile, "NUMBER (tokens) OF", t.upper(), "words: " + str(len(test_res[t]))

    print>> resultsfile, ""
    # get mean values & variances
    for t in tests:
        res = [np.average(test_res[t]), np.std(test_res[t])]
        print>> resultsfile, "%s mean: %s" % (t.upper(), res)
    print>> resultsfile, ""
    resultsfile.flush()

    return test_res


# In[8]:

# Get values for the function of interest
# (1) Information content (surprisal)
print "information content"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Information content (neg log prob)\n"
# lm_function = lambda x: lm.surprisal(x, 3)
corpus_experiment(mp_surprisal)

# In[9]:

# #(2) WML (syntactic probability)
# print "WML"
# print>>resultsfile,"%%%%%%%%%%%%%%%%%WML (syntactic probability)\n"
# lm_function = lambda x : lm.logprob_weighted_by_inverse_unigram_logprob(x)
# corpus_experiment(lm_function)


# In[16]:

# (3) Entropy of continuation now - sped up
print "entropy now sped up"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Entropy of continuation now (sped up approximation)\n"
# lm_function = lambda x: lm.entropy_continuation_very_fast(x[0:-1], 3)
res_now = corpus_experiment(mp_entropy_now_fast)

# (3) Entropy of continuation next - sped up
print "entropy next sped up"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Entropy of continuation next (sped up approximation)\n"
# lm_function = lambda x: lm.entropy_continuation_very_fast(x[1:], 3)
res_next = corpus_experiment(mp_entropy_next_fast)

print>> resultsfile, ""
# get mean values & variances
print "entropy change now>next"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Entropy of continuation change (sped up approximation)\n"
for t in tests:
    res = np.average(res_next[t]) - np.average(res_now[t])
    print>> resultsfile, "%s mean: %s" % (t.upper(), res)
print>> resultsfile, ""
resultsfile.flush()

# In[13]:

# (3) Entropy of continuation now (takes a long time)
print "entropy now slow"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Entropy of continuation now (very slow)\n"
# lm_function = lambda x: lm.entropy_continuation(x[0:-1], 3)
res_now = corpus_experiment(mp_entropy_now)

# (3) Entropy of continuation next (takes a long time)
print "entropy next slow"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Entropy of continuation next (very slow)\n"
# lm_function = lambda x: lm.entropy_continuation(x[1:], 3)
res_next = corpus_experiment(mp_entropy_next)

print>> resultsfile, ""
# get mean values & variances
print "entropy change now>next slow"
print>> resultsfile, "%%%%%%%%%%%%%%%%%Entropy of continuation change (very slow)\n"
for t in tests:
    res = np.average(res_next[t]) - np.average(res_now[t])
    print>> resultsfile, "%s mean: %s" % (t.upper(), res)
print>> resultsfile, ""
resultsfile.flush()

# In[17]:

resultsfile.close()


# In[ ]:
