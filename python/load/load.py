import csv

def add_word_continuation_tags(tags):
    """Returns list with continuation tags for each word:
    <cc/> continues current dialogue act and the next word will also continue
    <ct/> continues current dialogue act and is the last word of it
    <tc/> starts this dialogue act tag and the next word continues it
    <tt/> starts and ends dialogue act (single word dialogue act)
    """
    tags = list(tags)
    for i in range(0, len(tags)):
        if i == 0:
            tags[i] = tags[i] + "<t"
        else:
            tags[i] = tags[i] + "<c"
        if i == len(tags)-1:
            tags[i] = tags[i] + "t/>"
        else:
            tags[i] = tags[i] + "c/>"
    return tags


def sort_into_dialogue_speakers(IDs, mappings, utts, pos_tags=None,
                                labels=None,
                                add_uttseg=False,
                                add_dialogue_acts=False):
    # for each utterance, given its ID get its conversation number
    # and dialogue participant in the format
    # needed for MSALign files
    # return a list of tuples of (dialogueID+speaker [eg. sw4004A],
    # dialogue_act
    # ((ID1,utterance)),(ID2,utterance))
    dialogue_speakers = []
    currentA = ""
    currentB = ""
    A_utts = []
    B_utts = []
    A_mappings = []
    B_mappings = []
    A_pos = []
    B_pos = []
    A_labels = []
    B_labels = []

    for ID, _, utt, pos, tags in zip(IDs, mappings, utts, pos_tags, labels):
        # if "3756" in ID:
        # print ID, mapping, utt
        split = ID.split(":")
        dialogue = split[0]
        speaker = split[1]
        uttID = split[2]
        mapping = [uttID] * len(utt)
        if add_dialogue_acts:
            dialogue_act = split[3]
        current_speaker = "".join([dialogue, speaker])
        if "A" in current_speaker:
            if current_speaker != currentA and not currentA == "":
                dialogue_speakers.append((currentA, A_mappings, A_utts, A_pos,
                                          A_labels))
                A_utts = []
                A_mappings = []
                A_pos = []
                A_labels = []
            currentA = current_speaker
            A_utts.extend(list(utt))
            A_mappings.extend(list(mapping))
            A_pos.extend(list(pos))
            if add_uttseg:
                tags = add_word_continuation_tags(tags)
            if add_dialogue_acts:
                tags = [this_tag + '<diact type="{0}"/>'
                        .format(dialogue_act) for this_tag in tags]
            A_labels.extend(tags)
        elif "B" in current_speaker:
            if current_speaker != currentB and not currentB == "":
                dialogue_speakers.append((currentB, B_mappings, B_utts, B_pos,
                                          B_labels))
                B_utts = []
                B_mappings = []
                B_pos = []
                B_labels = []
            currentB = current_speaker
            B_utts.extend(list(utt))
            B_mappings.extend(list(mapping))
            B_pos.extend(list(pos))
            if add_uttseg:
                tags = add_word_continuation_tags(tags)
            if add_dialogue_acts:
                tags = [this_tag + '<diact type="{0}"/>'
                        .format(dialogue_act) for this_tag in tags]
            B_labels.extend(tags)

    if not (currentA, A_mappings, A_utts, A_pos, A_labels) in\
            dialogue_speakers[-2:]:
        # if current_speaker != currentA and not currentA == "":
        dialogue_speakers.append((currentA, A_mappings, A_utts, A_pos,
                                  A_labels))  # concatenate them all together
    if not (currentB, B_mappings, B_utts, B_pos, B_labels) in \
            dialogue_speakers[-2:]:
        # if current_speaker != currentB and not currentB == "":
        dialogue_speakers.append((currentB, B_mappings, B_utts, B_pos,
                                  B_labels))  # concatenate them all together
    return dialogue_speakers


def load_data_from_disfluency_corpus_file(f, fake_timing=True):
    """Loads from file into five lists of lists of strings of equal length:
    one for utterance iDs (IDs))
    one for word timings of the targets (start,stop)
    one for words (seq), 
    one for pos (pos_seq) 
    one for tags (targets).
     
    NB this does not convert them into one-hot arrays, just outputs lists of string tags."""
     
    f = open(f)
    print "loading data", f.name
    count_seq = 0
    IDs = []
    seq = []
    pos_seq = []
    targets = []
    timings = []
    currentTimings = []
    current_fake_time = 0 # marks the current fake time for the dialogue (i.e. end of word)
    current_dialogue = ""
    
    reader=csv.reader(f,delimiter='\t')
    counter = 0
    utt_reference = ""
    currentWords = []
    currentPOS = []
    currentTags = []
    current_fake_time = 0
    
    #corpus = "" # can write to file
    for ref, mapping, word, postag, disftag in reader: #mixture of POS and Words
        counter+=1
        timing = (current_fake_time, current_fake_time + 1)
        if not ref == "":
            if count_seq>0: #do not reset the first time
                #convert to the inc tags
                #corpus+=utt_reference #write data to a file for checking
                #convert to vectors
                seq.append(tuple(currentWords))
                pos_seq.append(tuple(currentPOS))
                targets.append(tuple(currentTags))
                IDs.append(utt_reference)
                timings.append(tuple(currentTimings))
                #reset the words
                currentWords = []
                currentPOS = []
                currentTags = []
                currentTimings = []
            #set the utterance reference
            count_seq+=1
            utt_reference = ref
            if not utt_reference.split(":")[0] == current_dialogue:
                current_dialogue = utt_reference.split(":")[0]
                current_fake_time = 0 #TODO fake for now- reset the current beginning of word time
        currentWords.append((word, timing[0], timing[1]))
        currentPOS.append(postag)
        currentTags.append(disftag)
        currentTimings.append(timing)
        current_fake_time+=1
    #flush
    if not currentWords == []:
        seq.append(tuple(currentWords))
        pos_seq.append(tuple(currentPOS))
        targets.append(tuple(currentTags))
        IDs.append(utt_reference)
        timings.append(tuple(currentTimings))

    assert len(seq) == len(targets) == len(pos_seq)
    print "loaded " + str(len(seq)) + " sequences"
    f.close()
    return (IDs, timings, seq, pos_seq, targets)

