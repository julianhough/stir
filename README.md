# STIR (STrongly Incremental Repair detection)

For disfluency detection, don’t shake, just STIR…

```
()   ()      ()    /
  ()      ()  ()  /
   ______________/___
   \            /   /
    \^^^^^^^^^^/^^^/
     \     ___/   /
      \   (   )  /
       \  (___) /
        \ /    /
         \    /
          \  /
           \/
           ||			
           ||
           ||
           ||
           ||
           /\
          /;;\
     ==============
```



## About ##


STIR is an incrementally (word-by-word, left-to-right) functioning detector of self-repairs and edit terms in people’s speech.

STIR’s output is xml-style tags for each disfluent word, symbolising each part of any repair or edit term detected. The tags are:

`<e/>` - an edit term word, not necessarily inside a repair structure

`<rms id=“N”/>` - reparandum start word for repair with ID number N

`<rm id=“N”/>` - mid-reparandum word for repair N

`<i id=“N”/>` - interregnum word for repair N

`<rps id=“N”/>` - repair onset word for repair N (where N is normally the 0-indexed position in the sequence)

`<rp id=“N”/>` - mid-repair word for repair N

`<rpn id=“N”/>` - repair end word for substitution or repetition repair N

`<rpndel id=“N”/>` - repair end word for a delete repair N

Every repair detected or in the gold standard will have at least the `rms`, `rps` and `rpn`/`rpndel` tags, but the others may not be present.

Some example output on Switchboard utterances is as below, where `<f/>` is the default tag for a fluent word:

```
	4617:A:15:h		1	uh          UH	        <e/>
    				2	i	        PRP	        <f/>
    				3	dont	    VBPRB	    <f/>
    				4	know	    VB	        <f/>
    				
	4617:A:16:sd	1	the         DT          <rms id="1"/>
    				2	the	        DT	        <rps id="1"/><rpn id="1"/>
    				3	things	    NNS	        <f/>
    				4	they	    PRP	        <f/>
    				5	asked	    VBD         <f/>
    				6	to	        TO	        <f/>
    				7	talk	    VB	        <f/>
    				8	about	    IN	        <f/>
    				9	were	    VBD	        <f/>
    				10	whether	    IN	        <rms id="12"/>
    				11	the	        DT	        <rm id="12"/>
    				12	uh	        UH	        <i id="12"/><e/>
    				13	whether	    IN	        <rps id="12"/>
    				14	the	        DT	        <rpn id="12"/>
    				15	judge	    NN	        <f/>
    				16	should	    MD	        <f/>
    				17	be	        VB	        <f/>
    				18	the	        DT	        <f/>
    				19	one	        NN	        <f/>
    				20	that	    WDT	        <f/>
    				21	does	    VBZ	        <f/>
    				22	the	        DT	        <f/>
    				23	uh	        UH	        <e/>
				    24	sentencing	NN	        <f/>
```


## Basic use ##


Requirements: please check the README's in the subfolders `/python/` and `/java/` for the specific requirements.

STIR has two modes, offline and online. For the offline mode, where the code lives in the `/python/` directory, you can train a model on the standard switchboard training data and provide output in the above format on the standard Switchboard test and heldout data by running `python python/stir/train_test.py` from this directory. You can modify the arguments explained by running the script with the `-help` argument.

We envisage the most common useage is to tag a new corpus with the disfluency tags using the saved model which uses words and POS tags, and can deal with partial words, saved in `python/saved_models/words_pos_partial`. The corpus must be in the same format as those in the subdirectories found in `python/data`. For example, tagging the file `python/data/bnc_spoken/BNC-CH_partial_data.csv`, run the below:

`python python/stir/train_test_offline.py -dtestc python/data/bnc_spoken/BNC-CH_partial_data.csv`

Once finished, the resulting tagged file will be saved in several formats to give you some flexibility in how you want to use the results. With the saved model supplied here, these will be in the below locations where `{MODEL_NAME}` is `stack_d3_lm3pos_partial_swbd_disf_train_1_partial_data_3_8_4_2`:

```
python/experiments/results/{MODEL_NAME}/BNC-CH_partial_data_TAGS.text
python/experiments/results/{MODEL_NAME}/BNC-CH_partial_data_easy_read.text
python/experiments/results/{MODEL_NAME}/BNC-CH_partial_data_output_final.text
```

Only the file ending `output_final.text` can be evaluated using the evaluation methods in `python/evalution/disfluency_evaluation.py`. See the `python/evaluation` module for useage instructions. 

The online version is still in development (November 2017).


## Citation ##


In general, cite the following if you use this software:

```
@InProceedings{HoughPurver14EMNLP,
  author    = {Hough, Julian  and  Purver, Matthew},
  title     = {Strongly Incremental Repair Detection},
  booktitle = {Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP)},
  month     = {October},
  year      = {2014},
  address   = {Doha, Qatar},
  publisher = {Association for Computational Linguistics},
  pages     = {78--89},
  url       = {http://www.aclweb.org/anthology/D14-1009}
}
```

If you want to run the experiments described in the below paper, please cite the below, and run the experiments as described in `python/README.md`:

```
@article{PurverEtAl18Topics,
  author = "Matthew Purver and Julian Hough and Christine Howes",
  journal = "Topics in Cognitive Science",
  title = "{C}omputational models of miscommunication phenomena",
  volume = "submitted, under review",
  year = "2018"
}

```


## Acknowledgements ##

The Python language modelling code is largely based on:

[https://github.com/jhlau/acceptability_prediction]

for which one should cite:

```
@inproceedings{clark2013statistical,
  title={Statistical representation of grammaticality judgements: the limits of n-gram models},
  author={Clark, Alexander and Giorgolo, Gianluca and Lappin, Shalom},
  booktitle={Proceedings of the Fourth Annual Workshop on Cognitive Modeling and Computational Linguistics (CMCL)},
  pages={28--36},
  year={2013}
}
```
